package com.gcloud.compute.handler.vm.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.util.StringUtils;
import com.gcloud.compute.service.vm.base.IVmBaseNodeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.ErrorCodeUtil;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.senior.InstanceFsFreezeMsg;
import com.gcloud.header.compute.msg.node.vm.senior.InstanceFsFreezeReplyMsg;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Handler
public class InstanceFsFreezeHandler extends AsyncMessageHandler<InstanceFsFreezeMsg>{
	@Autowired
    private IVmBaseNodeService vmBaseNodeService;
	
	@Autowired
    private MessageBus bus;
	
	@Override
	public void handle(InstanceFsFreezeMsg msg) throws GCloudException {
		InstanceFsFreezeReplyMsg replyMsg = msg.deriveMsg(InstanceFsFreezeReplyMsg.class);
        replyMsg.setSuccess(false);
        replyMsg.setServiceId(MessageUtil.controllerServiceId());
        try{
        	boolean result = vmBaseNodeService.fsFreeze(msg.getInstanceId(), msg.getFsfreezeType());
            replyMsg.setSuccess(result);
        }catch (Exception ex){
            log.error("::冻结/解冻虚拟机文件系统失败", ex);
            replyMsg.setErrorCode(ErrorCodeUtil.getErrorCode(ex, "::冻结/解冻虚拟机文件系统失败"));
        }
        if(!replyMsg.getSuccess() && StringUtils.isBlank(replyMsg.getErrorCode())) {
        	replyMsg.setErrorCode("::冻结/解冻虚拟机文件系统失败");
        }
        bus.send(replyMsg);
	}

}
