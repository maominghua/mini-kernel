package com.gcloud.controller.utils;

import com.gcloud.common.model.PageParams;
import com.gcloud.framework.db.PageResult;

import java.util.ArrayList;
import java.util.List;

public class ApiUtil {

    public static <T> PageResult<T> toPage(PageResult<?> page, List<T> datas){

        PageResult<T> pageResult = new PageResult<>();
        pageResult.setList(datas);
        pageResult.setPageNo(page.getPageNo());
        pageResult.setPageSize(page.getPageSize());
        pageResult.setTotalCount(page.getTotalCount());

        return pageResult;
    }

//    public static <T> PageResult<T> emptyPage(T clazz, PageParams pageParams){
//
//        PageResult<T> pageResult = new PageResult<>();
//        List<T> datas = new ArrayList<>();
//        pageResult.setList(datas);
//        pageResult.setPageNo(pageParams.getPageNumber());
//        pageResult.setPageSize(pageParams.getPageSize());
//        pageResult.setTotalCount(0);
//
//        return pageResult;
//
//    }

    public static PageResult emptyPage(PageParams pageParams){

        PageResult pageResult = new PageResult<>();
        List datas = new ArrayList<>();
        pageResult.setList(datas);
        pageResult.setPageNo(pageParams.getPageNumber());
        pageResult.setPageSize(pageParams.getPageSize());
        pageResult.setTotalCount(0);

        return pageResult;

    }
}
