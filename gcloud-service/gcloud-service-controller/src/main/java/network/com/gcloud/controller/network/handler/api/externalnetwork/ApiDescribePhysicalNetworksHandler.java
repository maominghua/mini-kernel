package com.gcloud.controller.network.handler.api.externalnetwork;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.prop.NetworkProp;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.enums.NetworkType;
import com.gcloud.header.network.model.PhysicalNetworksItemType;
import com.gcloud.header.network.msg.api.DescribePhysicalNetworksMsg;
import com.gcloud.header.network.msg.api.DescribePhysicalNetworksReplyMsg;

@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="DescribePhysicalNetworks")
public class ApiDescribePhysicalNetworksHandler extends MessageHandler<DescribePhysicalNetworksMsg, DescribePhysicalNetworksReplyMsg>{
	@Autowired
	private NetworkProp prop;
	
	@Override
	public DescribePhysicalNetworksReplyMsg handle(DescribePhysicalNetworksMsg msg) throws GCloudException {
		List<String> phynets = new ArrayList<String>();
		String networkType = msg.getNetworkType();
		if(StringUtils.isBlank(msg.getNetworkType())) {
			phynets = prop.getVlanPhysnets();
			phynets.addAll(prop.getFlatPhysnets());
		}else if(networkType.equals(NetworkType.VLAN.value())) {
			phynets = prop.getVlanPhysnets();
		}else if(networkType.equals(NetworkType.FLAT.value())) {
			phynets = prop.getFlatPhysnets();
		}else {
			throw new GCloudException("0160401::不支持该网络类型");
		}
		List<PhysicalNetworksItemType> response = new ArrayList<PhysicalNetworksItemType>();
		for(String phynet:phynets) {
			PhysicalNetworksItemType item = new PhysicalNetworksItemType();
			item.setPhyDev(phynet);
			response.add(item);
		}
		DescribePhysicalNetworksReplyMsg replyMsg = new DescribePhysicalNetworksReplyMsg();
		replyMsg.init(response);
		return replyMsg;
	}

}
