package com.gcloud.controller.network.dao;

import com.gcloud.controller.network.entity.QosFipPolicyBinding;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Created by yaowj on 2018/10/30.p_id
 */
@Repository
public class QosFipPolicyBindingDao extends JdbcBaseDaoImpl<QosFipPolicyBinding, Long> {

    public int deleteByFipId(String fipId){

        String sql = "delete from gc_qos_fip_policy_bindings where fip_id = ?";
        Object[] values = {fipId};
        return this.jdbcTemplate.update(sql, values);
    }

    public int deleteByPolicyId(String policyId){

        String sql = "delete from gc_qos_fip_policy_bindings where policy_id = ?";
        Object[] values = {policyId};
        return this.jdbcTemplate.update(sql, values);
    }

}
