package com.gcloud.controller.network.model;

import java.util.List;

public class CreateSubnetParams {

    private String networkId;
    private String cidrBlock;
    private String zoneId;
    private String regionId;
    private String subnetName;
    private String gatewayIp;
    private List<String> dnsNameServers;

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String getCidrBlock() {
        return cidrBlock;
    }

    public void setCidrBlock(String cidrBlock) {
        this.cidrBlock = cidrBlock;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getSubnetName() {
        return subnetName;
    }

    public void setSubnetName(String subnetName) {
        this.subnetName = subnetName;
    }

	public List<String> getDnsNameServers() {
		return dnsNameServers;
	}

	public void setDnsNameServers(List<String> dnsNameServers) {
		this.dnsNameServers = dnsNameServers;
	}

	public String getGatewayIp() {
		return gatewayIp;
	}

	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}
}
