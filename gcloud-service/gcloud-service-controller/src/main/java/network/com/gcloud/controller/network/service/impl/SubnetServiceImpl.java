package com.gcloud.controller.network.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.NetworkDao;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.CreateSubnetParams;
import com.gcloud.controller.network.model.DescribeSubnetParams;
import com.gcloud.controller.network.provider.ISubnetProvider;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.model.DescribeSubnetModel;
import com.gcloud.header.network.msg.api.ModifySubnetAttributeMsg;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class SubnetServiceImpl implements ISubnetService {
    @Autowired
    private SubnetDao subnetDao;
    @Autowired
    private NetworkDao networkDao;
    @Autowired
    private PortDao portDao;

    @Autowired
    private IPortService portService;

    @Override
    public String createSubnet(CreateSubnetParams params, CurrentUser currentUser) {
        Network network = networkDao.getById(params.getNetworkId());
        if (network == null) {
            throw new GCloudException("0030402::找不到网络");
        }
        String subnetId = StringUtils.genUuid();
        this.getProviderOrDefault().createSubnet(network, subnetId, params, currentUser);
        return subnetId;
    }

    @Override
    public void deleteSubnet(String subnetId) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030402::找不到交换机");
        }
        subnetDao.deleteById(subnetId);
        //删除dhcp的poort

        List<Port> ports = portDao.subnetPorts(subnetId);
        if(ports != null && ports.size() > 0){
            for(Port port : ports){
                if(!DeviceOwner.ROUTER.getValue().equals(port.getDeviceOwner()) && !DeviceOwner.DHCP.getValue().equals(port.getDeviceOwner())){
                    throw new GCloudException("0030403::有关联的端口，不能删除");
                }
            }

            for(Port port : ports){
                portService.cleanPortData(port.getId());
            }
        }

        this.checkAndGetProvider(subnet.getProvider()).deleteSubnet(subnet.getProviderRefId());
        
    }

    @Override
    public void modifyAttribute(String subnetId, String subnetName) {
        Subnet subnet = subnetDao.getById(subnetId);
        if (null == subnet) {
            throw new GCloudException("0030203::找不到交换机");
        }
        //如果要修改dhcp，需要清楚数据库dhcp的port

        List<String> updateField = new ArrayList<String>();
        updateField.add(subnet.updateName(subnetName));
        updateField.add(subnet.updateUpdatedAt(new Date()));
        subnetDao.update(subnet, updateField);

        CacheContainer.getInstance().put(CacheType.SUBNET_NAME, subnetId, subnetName);
        
        this.checkAndGetProvider(subnet.getProvider()).modifyAttribute(subnet.getProviderRefId(), subnetName, null, null);
        
    }
    
    @Override
    public void modifyAttribute(ModifySubnetAttributeMsg params) {
        Subnet subnet = subnetDao.getById(params.getSubnetId());
        if (null == subnet) {
            throw new GCloudException("0170203::找不到该子网");
        }
        List<String> updateField = new ArrayList<String>();
        updateField.add(subnet.updateName(params.getSubnetName()));
        updateField.add(subnet.updateUpdatedAt(new Date()));
        //dns
        if (params.getDnsNameServers() != null) {
        	String dnsServers = "";
			for (String host : params.getDnsNameServers()) {
				dnsServers += host + ";";
			}
			if(StringUtils.isNotBlank(dnsServers)) {
				updateField.add(subnet.updateDnsServers(dnsServers.substring(0, dnsServers.length() - 1)));
			}
		}
        if(com.gcloud.common.util.StringUtils.isNotBlank(params.getGatewayIp())) {
        	updateField.add(subnet.updateGatewayIp(params.getGatewayIp()));
        }
        subnetDao.update(subnet, updateField);
        
        CacheContainer.getInstance().put(CacheType.SUBNET_NAME, params.getSubnetId(), params.getSubnetName());
        
        this.checkAndGetProvider(subnet.getProvider()).modifyAttribute(subnet.getProviderRefId(), params.getSubnetName(),params.getDnsNameServers(), params.getGatewayIp());
    }

    private ISubnetProvider getProviderOrDefault() {
        ISubnetProvider provider = ResourceProviders.getDefault(ResourceType.SUBNET);
        return provider;
    }

    private ISubnetProvider checkAndGetProvider(Integer providerType) {
        ISubnetProvider provider = ResourceProviders.checkAndGet(ResourceType.SUBNET, providerType);
        return provider;
    }

	@Override
	public PageResult<DescribeSubnetModel> describeSubnet(DescribeSubnetParams params, CurrentUser currentUser) {
		//TODO tenantName, userName, vrouterId没有赋值
		PageResult<DescribeSubnetModel> response = subnetDao.describeSubnet(params, DescribeSubnetModel.class, currentUser);
		return response;
	}
}
