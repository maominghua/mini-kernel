package com.gcloud.controller.network.handler.api.network.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateVpcMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiCreateVpcMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiCreateVpcReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="CreateVpc", versions = {ApiVersion.Standard})
public class StandardApiCreateVpcHandler extends MessageHandler<StandardApiCreateVpcMsg, StandardApiCreateVpcReplyMsg>{

	@Autowired
	IVpcService service;
	
	@Override
	public StandardApiCreateVpcReplyMsg handle(StandardApiCreateVpcMsg msg) throws GCloudException {
		StandardApiCreateVpcReplyMsg reply = new StandardApiCreateVpcReplyMsg();
		String vpcId = service.createVpc(toParams(msg));
		reply.setVpcId(vpcId);
		return reply;
	}
	
	public CreateVpcMsg toParams(StandardApiCreateVpcMsg msg) {
		CreateVpcMsg params = BeanUtil.copyProperties(msg, CreateVpcMsg.class);
		return params;
	}

}
