package com.gcloud.controller.network.util;

public class QosUtil {

    public static Integer burst(Integer bandwidth){

        if(bandwidth == null || bandwidth <= 0){
            return bandwidth;
        }
        return ((Double)(bandwidth * 8 * 0.8)).intValue();
    }

}
