package com.gcloud.controller.network.service;

/**
 * Created by yaowj on 2018/10/30.
 */
public interface IQosFipPolicyBindingService {
    void bind(String fipId, String policyId);

}
