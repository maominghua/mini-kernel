package com.gcloud.controller.network.handler.api.router;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DetailVRouterParams;
import com.gcloud.controller.network.service.IRouterService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailVRouter;
import com.gcloud.header.network.msg.api.ApiDetailVRouterMsg;
import com.gcloud.header.network.msg.api.ApiDetailVRouterReplyMsg;

@ApiHandler(module=Module.ECS,subModule=SubModule.VROUTER,action="DetailVRouter")
public class ApiDetailVRouterHandler extends MessageHandler<ApiDetailVRouterMsg, ApiDetailVRouterReplyMsg>{

	@Autowired
	IRouterService vRouterService;
	
	@Override
	public ApiDetailVRouterReplyMsg handle(ApiDetailVRouterMsg msg) throws GCloudException {
		DetailVRouterParams params = BeanUtil.copyProperties(msg, DetailVRouterParams.class);
		DetailVRouter response = vRouterService.detailVRouter(params, msg.getCurrentUser());
		
		ApiDetailVRouterReplyMsg reply = new ApiDetailVRouterReplyMsg();
		reply.setDetailVRouter(response);
		return reply;
	}

}
