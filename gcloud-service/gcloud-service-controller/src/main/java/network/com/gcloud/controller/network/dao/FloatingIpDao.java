package com.gcloud.controller.network.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.FloatingIp;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.model.EipAddressSetType;
import com.gcloud.header.network.msg.api.DescribeEipAddressesMsg;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class FloatingIpDao extends JdbcBaseDaoImpl<FloatingIp, String>{
	public PageResult<EipAddressSetType> getByPage(DescribeEipAddressesMsg param) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(param.getCurrentUser(), "f.");
		
		List<Object> values = new ArrayList<Object>();

		StringBuffer sb = new StringBuffer();
		sb.append("select f.id, f.status, f.floating_ip_address, f.create_time, f.floating_network_id, f.fixed_port_id, bw.bandwidth, ");
		
		//instanceName和externalNetworkName
		sb.append(" gp.name as instanceName, gn.name as externalNetworkName");
		
		sb.append(" from gc_floating_ips f ");
		sb.append(" left join(");
		sb.append(" select b.fip_id, convert(ifnull(min(r.max_kbps), 0) / 1024, signed) bandwidth");
		sb.append(" from gc_qos_fip_policy_bindings b, gc_qos_bandwidth_limit_rules r where b.policy_id = r.qos_policy_id group by b.fip_id");
		sb.append(" ) bw");
		sb.append(" on f.id = bw.fip_id");
		
		//补充instanceName和externalNetworkName
		sb.append(" left join gc_ports as gp on f.fixed_port_id = gp.id");
		sb.append(" left join gc_networks as gn on f.floating_network_id = gn.id");

		sb.append(" where 1 = 1");

		if(StringUtils.isNotBlank(param.getAllocationId())) {
			sb.append(" AND f.id = ?");
			values.add(param.getAllocationId());
		}
		if(StringUtils.isNotBlank(param.getEipAddress())) {
			sb.append(" AND f.floating_ip_address = ?");
			values.add(param.getEipAddress());
		}
		if(StringUtils.isNotBlank(param.getStatus())) {
			sb.append(" AND f.status = ?");
			values.add(param.getStatus());
		}
		
		sb.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());

		sb.append(" ORDER BY f.create_time DESC");
		return this.findBySql(sb.toString(), values, param.getPageNumber(), param.getPageSize(), EipAddressSetType.class);
	}
	
	public int getAllNum(CurrentUser currentUser) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "f.");
		
		List<Object> values = new ArrayList<Object>();
		StringBuffer countsql = new StringBuffer();
		countsql.append("select count(f.id) from gc_floating_ips f where 1=1 ");
		countsql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		return this.countBySql(countsql.toString(), values);
	}
}
