package com.gcloud.controller.network.model;

import com.gcloud.common.model.PageParams;

public class DescribeSubnetParams extends PageParams{
	private String networkId;

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
}
