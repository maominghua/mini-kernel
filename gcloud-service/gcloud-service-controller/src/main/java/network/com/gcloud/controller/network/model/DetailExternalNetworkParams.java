package com.gcloud.controller.network.model;

public class DetailExternalNetworkParams {
	private String networkId;

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
}
