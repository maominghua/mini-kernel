package com.gcloud.controller.network.handler.api.network;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.ApiDetailVpcMsg;
import com.gcloud.header.network.msg.api.ApiDetailVpcReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="DetailVpc")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcId")
public class ApiDetailVpcHandler extends MessageHandler<ApiDetailVpcMsg, ApiDetailVpcReplyMsg>{
	@Autowired
	IVpcService service;
	
	@Override
	public ApiDetailVpcReplyMsg handle(ApiDetailVpcMsg msg) throws GCloudException {
		ApiDetailVpcReplyMsg reply  = new ApiDetailVpcReplyMsg();
		reply.setDetailVpc(service.detail(msg.getVpcId()));
		return reply;
	}

}
