package com.gcloud.controller.network.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

/**
 * Created by yaowj on 2018/10/24.
 */
@Table(name = "gc_ipallocations", jdbc = "controllerJdbcTemplate")
public class Ipallocation {

    @ID
    private Long id;
    private String portId;
    private String ipAddress;
    private String subnetId;
    private String networkId;

    public static final String ID = "id";
    public static final String PORT_ID = "portId";
    public static final String IP_ADDRESS = "ipAddress";
    public static final String SUBNET_ID = "subnetId";
    public static final String NETWORK_ID = "networkId";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getSubnetId() {
        return subnetId;
    }

    public void setSubnetId(String subnetId) {
        this.subnetId = subnetId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    public String updateId(Long id) {
        this.setId(id);
        return ID;
    }

    public String updatePortId(String portId) {
        this.setPortId(portId);
        return PORT_ID;
    }

    public String updateIpAddress(String ipAddress) {
        this.setIpAddress(ipAddress);
        return IP_ADDRESS;
    }

    public String updateSubnetId(String subnetId) {
        this.setSubnetId(subnetId);
        return SUBNET_ID;
    }

    public String updateNetworkId(String networkId) {
        this.setNetworkId(networkId);
        return NETWORK_ID;
    }
}
