package com.gcloud.controller.network.entity;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

@Table(name = "gc_qos_fip_policy_bindings", jdbc = "controllerJdbcTemplate")
public class QosFipPolicyBinding {

    @ID
    private Long id;
    private String policyId;
    private String fipId;

    public static final String ID = "id";
    public static final String POLICY_ID = "policyId";
    public static final String FIP_ID = "fipId";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getFipId() {
        return fipId;
    }

    public void setFipId(String fipId) {
        this.fipId = fipId;
    }

    public String updateId(Long id) {
		this.setId(id);
		return ID;
	}

	public String updatePolicyId(String policyId) {
		this.setPolicyId(policyId);
		return POLICY_ID;
	}

	public String updateFipId(String fipId) {
		this.setFipId(fipId);
		return FIP_ID;
	}
}
