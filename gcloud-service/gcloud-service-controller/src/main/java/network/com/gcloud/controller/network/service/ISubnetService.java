package com.gcloud.controller.network.service;

import com.gcloud.controller.network.model.CreateSubnetParams;
import com.gcloud.controller.network.model.DescribeSubnetParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.network.model.DescribeSubnetModel;
import com.gcloud.header.network.msg.api.ModifySubnetAttributeMsg;

public interface ISubnetService {
    String createSubnet(CreateSubnetParams params, CurrentUser currentUser);

    void deleteSubnet(String subnetId);

    void modifyAttribute(String subnetId, String subnetName);

    void modifyAttribute(ModifySubnetAttributeMsg msg);
    
    PageResult<DescribeSubnetModel> describeSubnet(DescribeSubnetParams params, CurrentUser currentUser);
}
