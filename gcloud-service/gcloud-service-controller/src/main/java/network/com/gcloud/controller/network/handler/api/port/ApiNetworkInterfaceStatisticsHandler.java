package com.gcloud.controller.network.handler.api.port;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsMsg;
import com.gcloud.header.network.msg.api.ApiNetworkInterfaceStatisticsReplyMsg;

@ApiHandler(module = Module.ECS,action = "NetworkInterfaceStatistics")
@GcLog(taskExpect = "网卡统计")
public class ApiNetworkInterfaceStatisticsHandler extends MessageHandler<ApiNetworkInterfaceStatisticsMsg, ApiNetworkInterfaceStatisticsReplyMsg>{
	@Autowired
	private IPortService portService;
	
	@Override
	public ApiNetworkInterfaceStatisticsReplyMsg handle(ApiNetworkInterfaceStatisticsMsg msg) throws GCloudException {
		return portService.statistic(msg.getCurrentUser());
	}

}
