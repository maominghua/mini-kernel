package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.SecurityDirection;

import java.util.Arrays;

public enum NeutronSecurityDirection {
    EGRESS("egress", SecurityDirection.EGRESS),
    INGRESS("ingress", SecurityDirection.INGRESS);

    NeutronSecurityDirection(String neutronSecurityDirection, SecurityDirection securityDirection) {
        this.neutronSecurityDirection = neutronSecurityDirection;
        this.securityDirection = securityDirection;
    }

    private String neutronSecurityDirection;
    private SecurityDirection securityDirection;

    public String getNeutronSecurityDirection() {
        return neutronSecurityDirection;
    }

    public SecurityDirection getSecurityDirection() {
        return securityDirection;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronSecurityDirection direction = Arrays.stream(NeutronSecurityDirection.values()).filter(o -> o.getNeutronSecurityDirection().equals(neutronValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getSecurityDirection().value();
    }

    public static String getByGcValue(String gcValue){
        NeutronSecurityDirection direction = Arrays.stream(NeutronSecurityDirection.values()).filter(o -> o.getSecurityDirection().value().equals(gcValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getNeutronSecurityDirection();
    }
}
