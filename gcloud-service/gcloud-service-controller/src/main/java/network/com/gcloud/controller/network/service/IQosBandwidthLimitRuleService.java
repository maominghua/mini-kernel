package com.gcloud.controller.network.service;

import com.gcloud.controller.network.entity.QosBandwidthLimitRule;
import com.gcloud.controller.network.entity.QosPolicy;
import com.gcloud.header.compute.enums.QosDirection;

/**
 * Created by yaowj on 2018/10/30.
 */
public interface IQosBandwidthLimitRuleService {
    QosBandwidthLimitRule create(String policyId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);
    QosBandwidthLimitRule create(QosPolicy qosPolicy, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);
    void update(String ruleId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);
    void update(QosPolicy qosPolicy, QosBandwidthLimitRule rule, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);
    void deleteQosBandwidthLimitRule(String ruleId);
    void deleteQosBandwidthLimitRule(QosPolicy qosPolicy, QosBandwidthLimitRule rule);
}
