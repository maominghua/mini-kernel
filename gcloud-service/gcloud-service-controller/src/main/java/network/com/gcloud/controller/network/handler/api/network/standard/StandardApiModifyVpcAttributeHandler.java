package com.gcloud.controller.network.handler.api.network.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ModifyVpcAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyVpcAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyVpcAttributeReplyMsg;

@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="ModifyVpcAttribute", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcId")
public class StandardApiModifyVpcAttributeHandler extends MessageHandler<StandardApiModifyVpcAttributeMsg, StandardApiModifyVpcAttributeReplyMsg>{

	@Autowired
	IVpcService service;
	
	@Override
	public StandardApiModifyVpcAttributeReplyMsg handle(StandardApiModifyVpcAttributeMsg msg) throws GCloudException {
		service.updateVpc(toParams(msg));
		msg.setObjectId(msg.getVpcId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.ROUTER_NAME, msg.getVpcId()));
		return new StandardApiModifyVpcAttributeReplyMsg();
	}
	
	public ModifyVpcAttributeMsg toParams(StandardApiModifyVpcAttributeMsg msg) {
		ModifyVpcAttributeMsg params = BeanUtil.copyProperties(msg, ModifyVpcAttributeMsg.class);
		return params;
	}

}
