package com.gcloud.controller.network.handler.api.subnet;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DescribeSubnetParams;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DescribeSubnetModel;
import com.gcloud.header.network.msg.api.ApiDescribeSubnetMsg;
import com.gcloud.header.network.msg.api.ApiDescribeSubnetReplyMsg;

@ApiHandler(module=Module.ECS,subModule=SubModule.SUBNET,action="DescribeSubnet")
public class ApiDescribeSubnetHandler extends MessageHandler<ApiDescribeSubnetMsg, ApiDescribeSubnetReplyMsg>{
	
	@Autowired
	private ISubnetService subnetService;
	
	@Override
	public ApiDescribeSubnetReplyMsg handle(ApiDescribeSubnetMsg msg) throws GCloudException {
		DescribeSubnetParams params = BeanUtil.copyProperties(msg, DescribeSubnetParams.class);
		PageResult<DescribeSubnetModel> response = subnetService.describeSubnet(params, msg.getCurrentUser());
		
		ApiDescribeSubnetReplyMsg reply = new ApiDescribeSubnetReplyMsg();
		reply.init(response);
		return reply;
	}

}
