package com.gcloud.controller.network.handler.api.floatingip;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.ApiDetailEipAddressMsg;
import com.gcloud.header.network.msg.api.ApiDetailEipAddressReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@ApiHandler(module=Module.ECS,subModule=SubModule.EIPADDRSS,action="DetailEipAddress")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.EIP, resourceIdField = "allocationId")
public class ApiDetailEipAddressHandler extends MessageHandler<ApiDetailEipAddressMsg, ApiDetailEipAddressReplyMsg>{
	@Autowired
	IFloatingIpService eipService;

	@Override
	public ApiDetailEipAddressReplyMsg handle(ApiDetailEipAddressMsg msg) throws GCloudException {
		ApiDetailEipAddressReplyMsg reply = new ApiDetailEipAddressReplyMsg();
		reply.setDetailEipAddress(eipService.detail(msg));
		return reply;
	}
}
