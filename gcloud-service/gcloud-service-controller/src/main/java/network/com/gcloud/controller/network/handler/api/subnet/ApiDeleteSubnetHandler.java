package com.gcloud.controller.network.handler.api.subnet;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.DeleteSubnetMsg;

@GcLog(taskExpect="删除子网")
@ApiHandler(module=Module.ECS,subModule=SubModule.SUBNET,action="DeleteSubnet")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SUBNET, resourceIdField = "subnetId")
public class ApiDeleteSubnetHandler extends MessageHandler<DeleteSubnetMsg, ApiReplyMessage> {
	@Autowired
	private ISubnetService subnetService;
	
	@Override
	public ApiReplyMessage handle(DeleteSubnetMsg msg) throws GCloudException {
		subnetService.deleteSubnet(msg.getSubnetId());
		msg.setObjectId(msg.getSubnetId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.SUBNET_NAME, msg.getSubnetId()));
		return new ApiReplyMessage();
	}

}
