package com.gcloud.controller.network.handler.api.port;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DetailNetworkInterfaceParams;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailNic;
import com.gcloud.header.network.msg.api.ApiDetailNetworkInterfaceMsg;
import com.gcloud.header.network.msg.api.ApiDetailNetworkInterfaceReplyMsg;

@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="DetailNetworkInterface")
public class ApiDetailNetworkInterfaceHandler extends MessageHandler<ApiDetailNetworkInterfaceMsg, ApiDetailNetworkInterfaceReplyMsg>{

	@Autowired
	private IPortService portService;
	
	@Override
	public ApiDetailNetworkInterfaceReplyMsg handle(ApiDetailNetworkInterfaceMsg msg) throws GCloudException {
		DetailNetworkInterfaceParams params = BeanUtil.copyProperties(msg, DetailNetworkInterfaceParams.class);
		DetailNic response = portService.detailNetworkInterface(params, msg.getCurrentUser());
		
		ApiDetailNetworkInterfaceReplyMsg reply = new ApiDetailNetworkInterfaceReplyMsg();
		reply.setDetailNetworkInterface(response);
		return reply;
	}

}
