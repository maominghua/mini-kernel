package com.gcloud.controller.network.handler.api.floatingip.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.AssociateEipAddressParams;
import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.standard.StandardApiAssociateEipAddressMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiAssociateEipAddressReplyMsg;

@GcLog(taskExpect="绑定弹性公网IP地址成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.EIPADDRSS,action="AssociateEipAddress", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.EIP, resourceIdField = "allocationId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.PORT, resourceIdField = "instanceId")
public class StandardApiAssociateEipAddressHandler extends MessageHandler<StandardApiAssociateEipAddressMsg, StandardApiAssociateEipAddressReplyMsg>{

	@Autowired
	IFloatingIpService eipService;
	
	@Override
	public StandardApiAssociateEipAddressReplyMsg handle(StandardApiAssociateEipAddressMsg msg) throws GCloudException {
		AssociateEipAddressParams params = BeanUtil.copyProperties(msg, AssociateEipAddressParams.class);
		eipService.associateEipAddress(params);
		
		msg.setObjectId(msg.getAllocationId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.EIP_NAME, msg.getAllocationId()));
		return new StandardApiAssociateEipAddressReplyMsg();
	}

}
