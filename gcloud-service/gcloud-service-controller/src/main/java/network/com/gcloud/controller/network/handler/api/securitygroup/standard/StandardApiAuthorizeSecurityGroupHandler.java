package com.gcloud.controller.network.handler.api.securitygroup.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.AuthorizeSecurityGroupParams;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.standard.StandardApiAuthorizeSecurityGroupMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiAuthorizeSecurityGroupReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="创建安全组规则成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="AuthorizeSecurityGroup", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SECURITYGROUP, resourceIdField = "securityGroupId")
public class StandardApiAuthorizeSecurityGroupHandler extends MessageHandler<StandardApiAuthorizeSecurityGroupMsg, StandardApiAuthorizeSecurityGroupReplyMsg>{

	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public StandardApiAuthorizeSecurityGroupReplyMsg handle(StandardApiAuthorizeSecurityGroupMsg msg)
			throws GCloudException {
		AuthorizeSecurityGroupParams params = BeanUtil.copyProperties(msg, AuthorizeSecurityGroupParams.class);
		securityGroupService.authorizeSecurityGroup(params, msg.getCurrentUser());
		msg.setObjectId(msg.getSecurityGroupId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.SECURITYGROUP_NAME, msg.getSecurityGroupId()));
		return new StandardApiAuthorizeSecurityGroupReplyMsg();
	}

}
