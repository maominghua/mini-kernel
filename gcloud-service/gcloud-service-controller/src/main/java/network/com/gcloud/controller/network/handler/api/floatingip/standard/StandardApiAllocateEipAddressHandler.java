package com.gcloud.controller.network.handler.api.floatingip.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.AllocateEipAddressResponse;
import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.standard.StandardApiAllocateEipAddressMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiAllocateEipAddressReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="申请弹性公网IP地址成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.EIPADDRSS,action="AllocateEipAddress", versions = {ApiVersion.Standard})
public class StandardApiAllocateEipAddressHandler extends MessageHandler<StandardApiAllocateEipAddressMsg, StandardApiAllocateEipAddressReplyMsg>{

	@Autowired
	IFloatingIpService eipService;
	
	@Override
	public StandardApiAllocateEipAddressReplyMsg handle(StandardApiAllocateEipAddressMsg msg) throws GCloudException {
		AllocateEipAddressResponse res = eipService.allocateEipAddress(msg.getNetworkId(), msg.getBandwidth(), msg.getRegion(), msg.getCurrentUser());
		StandardApiAllocateEipAddressReplyMsg reply = new StandardApiAllocateEipAddressReplyMsg();
		reply.setAllocationId(res.getAllocationId());
		reply.setEipAddress(res.getEipAddress());
		msg.setObjectId(msg.getNetworkId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.NETWORK_NAME, msg.getNetworkId()));
		return reply;
	}

}
