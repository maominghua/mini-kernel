package com.gcloud.controller.network.handler.api.floatingip;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IFloatingIpService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.ApiEipAddressStatisticsMsg;
import com.gcloud.header.network.msg.api.ApiEipAddressStatisticsReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@GcLog(taskExpect="弹性IP统计")
@ApiHandler(module=Module.ECS,subModule=SubModule.EIPADDRSS,action="EipAddressStatistics")
public class ApiEipAddressStatisticsHandler extends MessageHandler<ApiEipAddressStatisticsMsg, ApiEipAddressStatisticsReplyMsg>{
	@Autowired
	IFloatingIpService eipService;
	
	@Override
	public ApiEipAddressStatisticsReplyMsg handle(ApiEipAddressStatisticsMsg msg) throws GCloudException {
		return eipService.statistic(msg.getCurrentUser());
	}

}
