package com.gcloud.controller.network.handler.api.subnet;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.CreateSubnetParams;
import com.gcloud.controller.network.service.ISubnetService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateSubnetMsg;
import com.gcloud.header.network.msg.api.CreateSubnetReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@GcLog(taskExpect="创建子网")
@ApiHandler(module=Module.ECS,subModule=SubModule.SUBNET,action="CreateSubnet")
public class ApiCreateSubnetHandler  extends MessageHandler<CreateSubnetMsg, CreateSubnetReplyMsg> {
	@Autowired
	private ISubnetService subnetService;

	@Override
	public CreateSubnetReplyMsg handle(CreateSubnetMsg msg) throws GCloudException {
		CreateSubnetReplyMsg reply = new CreateSubnetReplyMsg();
		CreateSubnetParams params = new CreateSubnetParams();
		params.setCidrBlock(msg.getCidrBlock());
		params.setDnsNameServers(msg.getDnsNameServers());
		params.setGatewayIp(msg.getGatewayIp());
		params.setNetworkId(msg.getNetworkId());
		params.setSubnetName(msg.getSubnetName());
		reply.setSubnetId(subnetService.createSubnet(params, msg.getCurrentUser()));
		return reply;
	}

}
