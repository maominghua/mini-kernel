package com.gcloud.controller.network.service;

import com.gcloud.controller.network.entity.QosPolicy;

/**
 * Created by yaowj on 2018/10/30.
 */
public interface IQosPolicyService {

    QosPolicy create(Integer providerType, String name, String description, Boolean isDefault, Boolean shared);

    void delete(String id);

    void updateQosLimit(String policyId, Integer egress, Integer ingress);

    QosPolicy createQosLimit(Integer provider, Integer egress, Integer ingress);
}
