package com.gcloud.controller.network.prop;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "gcloud.controller.network.physnet")
public class NetworkProp {
	private List<String> vlanPhysnets;
	private List<String> flatPhysnets;
}
