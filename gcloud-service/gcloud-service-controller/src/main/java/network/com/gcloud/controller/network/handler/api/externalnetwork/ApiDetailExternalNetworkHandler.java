package com.gcloud.controller.network.handler.api.externalnetwork;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DetailExternalNetworkParams;
import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailExternalNetwork;
import com.gcloud.header.network.msg.api.ApiDetailExternalNetworkMsg;
import com.gcloud.header.network.msg.api.ApiDetailExternalNetworkReplyMsg;

@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="DetailExternalNetwork")
public class ApiDetailExternalNetworkHandler extends MessageHandler<ApiDetailExternalNetworkMsg, ApiDetailExternalNetworkReplyMsg>{

	@Autowired
	INetworkService networkService;
	
	@Override
	public ApiDetailExternalNetworkReplyMsg handle(ApiDetailExternalNetworkMsg msg) throws GCloudException {
		DetailExternalNetworkParams params = BeanUtil.copyProperties(msg, DetailExternalNetworkParams.class);
		DetailExternalNetwork response = networkService.detailExternalNetwork(params, msg.getCurrentUser());
		
		ApiDetailExternalNetworkReplyMsg reply = new ApiDetailExternalNetworkReplyMsg();
		reply.setDetailExternalNetwork(response);
		return reply;
	}

}
