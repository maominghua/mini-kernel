package com.gcloud.controller.network.handler.api.network.standard;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.model.DescribeNetworkInterfacesParams;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.utils.ApiUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.enums.DeviceOwner;
import com.gcloud.header.network.enums.standard.StandardNetworkInterfaceStatus;
import com.gcloud.header.network.model.NetworkInterfaceSet;
import com.gcloud.header.network.model.standard.StandardNetworkInterfaceSet;
import com.gcloud.header.network.msg.api.standard.StandardDescribeNetworkInterfacesMsg;
import com.gcloud.header.network.msg.api.standard.StandardDescribeNetworkInterfacesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="DescribeNetworkInterfaces", versions = {ApiVersion.Standard})
public class StandardApiDescribeNetworkInterfacesHandler extends MessageHandler<StandardDescribeNetworkInterfacesMsg, StandardDescribeNetworkInterfacesReplyMsg> {

	@Autowired
	private IPortService portService;

	@Override
	public StandardDescribeNetworkInterfacesReplyMsg handle(StandardDescribeNetworkInterfacesMsg msg) throws GCloudException {

		DescribeNetworkInterfacesParams params = toParams(msg);
		PageResult<NetworkInterfaceSet> response = portService.describe(params, msg.getCurrentUser());
		PageResult<StandardNetworkInterfaceSet> standardResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));

		StandardDescribeNetworkInterfacesReplyMsg replyMsg = new StandardDescribeNetworkInterfacesReplyMsg();
		replyMsg.init(standardResponse);
		return replyMsg;
	}

	public List<StandardNetworkInterfaceSet> toStandardReply(List<NetworkInterfaceSet> datas){
		if(datas == null){
			return null;
		}

		List<StandardNetworkInterfaceSet> standardDatas = new ArrayList<>();
		for(NetworkInterfaceSet data : datas){
			StandardNetworkInterfaceSet standardData = BeanUtil.copyProperties(data, StandardNetworkInterfaceSet.class);
			standardData.setInstanceId(data.getDeviceId());
			standardData.setInstanceType(data.getDeviceType());

			//转换状态
			if(StringUtils.isNotBlank(data.getDeviceId())) {
				standardData.setStatus(StandardNetworkInterfaceStatus.IN_USE.getValue());
			} else {
				standardData.setStatus(StandardNetworkInterfaceStatus.standardStatus(data.getStatus()));
			}
			standardDatas.add(standardData);
		}

		return standardDatas;

	}

	public DescribeNetworkInterfacesParams toParams(StandardDescribeNetworkInterfacesMsg msg){
		DescribeNetworkInterfacesParams params = BeanUtil.copyProperties(msg, DescribeNetworkInterfacesParams.class);
		params.setDeviceOwners(Arrays.asList(DeviceOwner.COMPUTE.getValue(), DeviceOwner.FOREIGN.getValue()));
		params.setIncludeOwnerless(true);

		return params;
	}
}
