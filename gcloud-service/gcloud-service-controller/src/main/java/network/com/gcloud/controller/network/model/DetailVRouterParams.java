package com.gcloud.controller.network.model;

public class DetailVRouterParams {
	private String vRouterId;

	public String getvRouterId() {
		return vRouterId;
	}

	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
}
