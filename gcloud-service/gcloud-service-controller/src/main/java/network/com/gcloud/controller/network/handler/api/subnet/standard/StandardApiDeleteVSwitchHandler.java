package com.gcloud.controller.network.handler.api.subnet.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.ISwitchService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.standard.StandardApiDeleteVSwitchMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiDeleteVSwitchReplyMsg;

@GcLog(taskExpect="删除交换机成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.VSWITCH,action="DeleteVSwitch", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SUBNET, resourceIdField = "vSwitchId")
public class StandardApiDeleteVSwitchHandler extends MessageHandler<StandardApiDeleteVSwitchMsg, StandardApiDeleteVSwitchReplyMsg>{

	@Autowired
	private ISwitchService switchService;
	
	@Override
	public StandardApiDeleteVSwitchReplyMsg handle(StandardApiDeleteVSwitchMsg msg) throws GCloudException {
		switchService.deleteVSwitch(msg.getvSwitchId());
		msg.setObjectId(msg.getvSwitchId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.SUBNET_NAME, msg.getvSwitchId()));
		return new StandardApiDeleteVSwitchReplyMsg();
	}

}
