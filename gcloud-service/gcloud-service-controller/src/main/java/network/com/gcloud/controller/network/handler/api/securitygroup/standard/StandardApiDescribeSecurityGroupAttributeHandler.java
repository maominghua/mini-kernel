package com.gcloud.controller.network.handler.api.securitygroup.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.model.DescribeSecurityGroupAttributeResponse;
import com.gcloud.controller.network.service.ISecurityGroupService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeSecurityGroupAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeSecurityGroupAttributeReplyMsg;

@ApiHandler(module=Module.ECS,subModule=SubModule.SECURITYGROUP,action="DescribeSecurityGroupAttribute", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SECURITYGROUP, resourceIdField = "securityGroupId")
public class StandardApiDescribeSecurityGroupAttributeHandler extends MessageHandler<StandardApiDescribeSecurityGroupAttributeMsg, StandardApiDescribeSecurityGroupAttributeReplyMsg>{

	@Autowired
	ISecurityGroupService securityGroupService;
	
	@Override
	public StandardApiDescribeSecurityGroupAttributeReplyMsg handle(StandardApiDescribeSecurityGroupAttributeMsg msg)
			throws GCloudException {
		DescribeSecurityGroupAttributeResponse res = securityGroupService.describeSecurityGroupAttribute(msg.getSecurityGroupId(), msg.getDirection(), msg.getEtherType(), msg.getRegion());
		return BeanUtil.copyProperties(res, StandardApiDescribeSecurityGroupAttributeReplyMsg.class);
	}

}
