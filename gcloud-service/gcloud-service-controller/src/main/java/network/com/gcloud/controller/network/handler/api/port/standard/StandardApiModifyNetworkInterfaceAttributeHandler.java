package com.gcloud.controller.network.handler.api.port.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyNetworkInterfaceAttributeMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiModifyNetworkInterfaceAttributeReplyMsg;

@GcLog(taskExpect="修改网卡属性成功")
@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="ModifyNetworkInterfaceAttribute", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.PORT, resourceIdField = "networkInterfaceId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SECURITYGROUP, resourceIdField = "securityGroupIds")
public class StandardApiModifyNetworkInterfaceAttributeHandler extends MessageHandler<StandardApiModifyNetworkInterfaceAttributeMsg, StandardApiModifyNetworkInterfaceAttributeReplyMsg>{

	@Autowired
	private IPortService portService;
	
	@Override
	public StandardApiModifyNetworkInterfaceAttributeReplyMsg handle(StandardApiModifyNetworkInterfaceAttributeMsg msg)
			throws GCloudException {
		portService.update(msg.getNetworkInterfaceId(), msg.getSecurityGroupIds(), msg.getNetworkInterfaceName());
		msg.setObjectId(msg.getNetworkInterfaceId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.PORT_NAME, msg.getNetworkInterfaceId()));
		return new StandardApiModifyNetworkInterfaceAttributeReplyMsg();
	}

}
