package com.gcloud.controller.network.handler.api.externalnetwork;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.INetworkService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ApiTplExternalNetworksMsg;
import com.gcloud.header.network.msg.api.ApiTplExternalNetworksReplyMsg;
@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="TplExternalNetworks")
public class ApiTplExternalNetworksHandler extends MessageHandler<ApiTplExternalNetworksMsg, ApiTplExternalNetworksReplyMsg>{
	@Autowired
	INetworkService service;

	@Override
	public ApiTplExternalNetworksReplyMsg handle(ApiTplExternalNetworksMsg msg) throws GCloudException {
		ApiTplExternalNetworksReplyMsg reply = new ApiTplExternalNetworksReplyMsg();
		reply.setExternalNetworks(service.tplExternalNetworks(msg));
		return reply;
	}
}
