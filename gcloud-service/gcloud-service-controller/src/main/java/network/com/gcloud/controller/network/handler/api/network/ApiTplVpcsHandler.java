package com.gcloud.controller.network.handler.api.network;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.msg.api.ApiTplVpcsMsg;
import com.gcloud.header.network.msg.api.ApiTplVpcsReplyMsg;

@ApiHandler(module=Module.ECS, subModule=SubModule.NETWORK, action="TplVpcs")
public class ApiTplVpcsHandler extends MessageHandler<ApiTplVpcsMsg, ApiTplVpcsReplyMsg>{
	@Autowired
	IVpcService service;

	@Override
	public ApiTplVpcsReplyMsg handle(ApiTplVpcsMsg msg) throws GCloudException {
		ApiTplVpcsReplyMsg reply = new ApiTplVpcsReplyMsg();
		reply.setVpcs(service.tplVpcs(msg));
		return reply;
	}
}
