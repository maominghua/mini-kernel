package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.QosDirection;

import java.util.Arrays;

public enum NeutronQosDirection {
    EGRESS(org.openstack4j.model.network.QosDirection.EGRESS, QosDirection.EGRESS),
    INGRESS(org.openstack4j.model.network.QosDirection.INGRESS, QosDirection.INGRESS);

    NeutronQosDirection(org.openstack4j.model.network.QosDirection neutronQosDirection, QosDirection qosDirection) {
        this.neutronQosDirection = neutronQosDirection;
        this.qosDirection = qosDirection;
    }

    private org.openstack4j.model.network.QosDirection neutronQosDirection;
    private QosDirection qosDirection;

    public QosDirection getQosDirection() {
        return qosDirection;
    }

    public org.openstack4j.model.network.QosDirection getNeutronQosDirection() {
        return neutronQosDirection;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronQosDirection direction = Arrays.stream(NeutronQosDirection.values()).filter(o -> o.getNeutronQosDirection().toJson().equals(neutronValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getQosDirection().value();
    }

    public static org.openstack4j.model.network.QosDirection getByGcValue(QosDirection gcValue){
        NeutronQosDirection direction = Arrays.stream(NeutronQosDirection.values()).filter(o -> o.getQosDirection().equals(gcValue)).findFirst().orElse(null);
        return direction == null ? null : direction.getNeutronQosDirection();
    }
}
