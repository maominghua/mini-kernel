package com.gcloud.controller.network.model;

public class DetailNetworkInterfaceParams {
	private String networkInterfaceId;

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
}
