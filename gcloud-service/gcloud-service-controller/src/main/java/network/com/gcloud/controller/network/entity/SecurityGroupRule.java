package com.gcloud.controller.network.entity;

import com.gcloud.controller.ResourceProviderEntity;
import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import java.util.Date;

@Table(name = "gc_security_group_rules", jdbc = "controllerJdbcTemplate")
public class SecurityGroupRule extends ResourceProviderEntity {//gc_security_group_rules
    @ID
    private String id;
    private String securityGroupId;
    private String protocol;
    private Integer portRangeMin;
    private Integer portRangeMax;
    private String remoteIpPrefix;
    private String remoteGroupId;
    private String direction;
    private String ethertype;
    private Date createTime;
    private Date updatedAt;
    private String userId;
    private String description;
    private String tenantId;

    public static final String ID = "id";
    public static final String SECURITY_GROUP_ID = "securityGroupId";
    public static final String PROTOCOL = "protocol";
    public static final String PORT_RANGE_MIN = "portRangeMin";
    public static final String PORT_RANGE_MAX = "portRangeMax";
    public static final String REMOTE_IP_PREFIX = "remoteIpPrefix";
    public static final String REMOTE_GROUP_ID = "remoteGroupId";
    public static final String DIRECTION = "direction";
    public static final String ETHERTYPE = "ethertype";
    public static final String CREATE_TIME = "createTime";
    public static final String UPDATED_AT = "updatedAt";
    public static final String USER_ID = "userId";
    public static final String DESCRIPTION = "description";
    public static final String TENANT_ID = "tenantId";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public Integer getPortRangeMin() {
        return portRangeMin;
    }

    public void setPortRangeMin(Integer portRangeMin) {
        this.portRangeMin = portRangeMin;
    }

    public Integer getPortRangeMax() {
        return portRangeMax;
    }

    public void setPortRangeMax(Integer portRangeMax) {
        this.portRangeMax = portRangeMax;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getRemoteIpPrefix() {
        return remoteIpPrefix;
    }

    public void setRemoteIpPrefix(String remoteIpPrefix) {
        this.remoteIpPrefix = remoteIpPrefix;
    }

    public String getRemoteGroupId() {
        return remoteGroupId;
    }

    public void setRemoteGroupId(String remoteGroupId) {
        this.remoteGroupId = remoteGroupId;
    }


    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getEthertype() {
        return ethertype;
    }

    public void setEthertype(String ethertype) {
        this.ethertype = ethertype;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date time) {
        this.updatedAt = time;
    }

    public String updateId(String id) {
        this.setId(id);
        return ID;
    }

    public String updateSecurityGroupId(String securityGroupId) {
        this.setSecurityGroupId(securityGroupId);
        return SECURITY_GROUP_ID;
    }

    public String updateProtocol(String protocol) {
        this.setProtocol(protocol);
        return PROTOCOL;
    }

    public String updatePortRangeMin(Integer portRangeMin) {
        this.setPortRangeMin(portRangeMin);
        return PORT_RANGE_MIN;
    }

    public String updatePortRangeMax(Integer portRangeMax) {
        this.setPortRangeMax(portRangeMax);
        return PORT_RANGE_MAX;
    }

    public String updateRemoteIpPrefix(String remoteIpPrefix) {
        this.setRemoteIpPrefix(remoteIpPrefix);
        return REMOTE_IP_PREFIX;
    }

    public String updateRemoteGroupId(String remoteGroupId) {
        this.setRemoteGroupId(remoteGroupId);
        return REMOTE_GROUP_ID;
    }

    public String updateDirection(String direction) {
        this.setDirection(direction);
        return DIRECTION;
    }

    public String updateEthertype(String ethertype) {
        this.setEthertype(ethertype);
        return ETHERTYPE;
    }

    public String updateCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return CREATE_TIME;
    }

    public String updateUpdatedAt(Date updatedAt) {
        this.setUpdatedAt(updatedAt);
        return UPDATED_AT;
    }

    public String updateUserId(String userId) {
        this.setUserId(userId);
        return USER_ID;
    }

    public String updateDescription(String description) {
        this.setDescription(description);
        return DESCRIPTION;
    }

    public String updateTenantId(String tenantId) {
        this.setTenantId(tenantId);
        return TENANT_ID;
    }
}
