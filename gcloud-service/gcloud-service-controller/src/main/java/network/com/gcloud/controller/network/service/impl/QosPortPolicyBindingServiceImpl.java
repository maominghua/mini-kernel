package com.gcloud.controller.network.service.impl;

import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.PortDao;
import com.gcloud.controller.network.dao.QosPolicyDao;
import com.gcloud.controller.network.dao.QosPortPolicyBindingDao;
import com.gcloud.controller.network.entity.Port;
import com.gcloud.controller.network.entity.QosPolicy;
import com.gcloud.controller.network.entity.QosPortPolicyBinding;
import com.gcloud.controller.network.provider.IQosProvider;
import com.gcloud.controller.network.service.IQosPortPolicyBindingService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.enums.ResourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yaowj on 2018/10/30.
 */
@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class QosPortPolicyBindingServiceImpl implements IQosPortPolicyBindingService {

    @Autowired
    private PortDao portDao;

    @Autowired
    private QosPolicyDao qosPolicyDao;

    @Autowired
    private QosPortPolicyBindingDao qosPortPolicyBindingDao;

    @Override
    public void bind(String portId, String policyId) {

        QosPolicy qosPolicy = qosPolicyDao.getById(policyId);
        if(qosPolicy == null){
            throw new GCloudException("::qos 不存在");
        }
        Port port = portDao.getById(portId);
        if(port == null){
            throw new GCloudException("::port 不存在");
        }

        QosPortPolicyBinding bind = new QosPortPolicyBinding();
        bind.setPolicyId(policyId);
        bind.setPortId(portId);
        qosPortPolicyBindingDao.save(bind);

        IQosProvider qosProvider = checkAndGetProvider(port.getProvider());
        qosProvider.bindPort(port.getProviderRefId(), qosPolicy.getProviderRefId());
    }

    private IQosProvider checkAndGetProvider(Integer providerType) {
        IQosProvider provider = ResourceProviders.checkAndGet(ResourceType.QOS, providerType);
        return provider;
    }
}
