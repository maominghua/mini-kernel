package com.gcloud.controller.network.dao;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.DescribeSubnetParams;
import com.gcloud.controller.network.model.DescribeVSwitchesParams;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.DeviceOwner;

import org.apache.poi.poifs.filesystem.FilteringDirectoryNode;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaowj on 2018/10/25.
 */
@Repository
public class SubnetDao extends JdbcBaseDaoImpl<Subnet, String> {
	public <E> PageResult<E> describeVSwitches(DescribeVSwitchesParams params, Class<E> clazz, CurrentUser currentUser){
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "s.");
		
		StringBuffer sql = new StringBuffer();
		List<Object> listParams = new ArrayList<Object>();

//        sql.append("select s.id as vSwitchId,s.network_id as vpcId,s.cidr as cidrBlock,s.name as vSwitchName,s.zone_id as zoneId,s.router_id as vRouterId from gc_subnets s");
        sql.append("select s.*, rs.router_id from gc_subnets s");
        sql.append(" left join");
        sql.append(" (select group_concat(rp.router_id) router_id, i.subnet_id from gc_router_ports rp ");
        sql.append(" left join gc_ports p on rp.port_id = p.id left join gc_ipallocations i on p.id = i.port_id");
        sql.append(" where rp.port_type = ? group by i.subnet_id) rs");
        sql.append(" on s.id = rs.subnet_id");
        sql.append(" where 1=1");

        listParams.add(DeviceOwner.ROUTER.getValue());

        if(params.getVpcId() != null && StringUtils.isNotBlank(params.getVpcId())) {
            sql.append(" and rs.router_id = ?");
            listParams.add(params.getVpcId());
        }
        if(params.getNetworkId() != null && StringUtils.isNotBlank(params.getNetworkId())) {
            sql.append(" and s.network_id = ?");
            listParams.add(params.getNetworkId());
        }
        if(params.getvSwitchId() != null && StringUtils.isNotBlank(params.getvSwitchId())) {
        	sql.append(" and s.id = ?");
        	listParams.add(params.getvSwitchId());
        }
        
        sql.append(sqlModel.getWhereSql());
        listParams.addAll(sqlModel.getParams());
        
        sql.append(" order by s.create_time desc");

        return findBySql(sql.toString(),listParams, params.getPageNumber(), params.getPageSize(), clazz);
	}

	public boolean hasSubnet(String networkId){
	    List<Object> values = new ArrayList<>();

	    String sql = "select * from gc_subnets t where t.network_id = ? limit 1";
        values.add(networkId);
        List<Subnet> subnets = findBySql(sql, values);
        return subnets != null && subnets.size() > 0;

    }
	
	public <E>PageResult<E> describeSubnet(DescribeSubnetParams params, Class<E> clazz, CurrentUser currentUser) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select s.*, z.name as zoneName from gc_subnets as s");
		sql.append(" left join gc_zones as z on s.zone_id = z.id");
		sql.append(" where s.network_id = ? and s.tenant_id in (").append(SqlUtil.inPreStr(currentUser.getUserTenants().size())).append(")");
		values.add(params.getNetworkId());
		values.addAll(currentUser.getUserTenants());
		
		return findBySql(sql.toString(), values, params.getPageNumber(), params.getPageSize(), clazz);
	}
	
	public <E> List<E> getSubnets(Class<E> clazz, CurrentUser currentUser){
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "s.");
		
		StringBuffer sql = new StringBuffer();
		List<Object> listParams = new ArrayList<Object>();
		sql.append("select * from gc_subnets s where 1=1 ");
		
		sql.append(sqlModel.getWhereSql());
        listParams.addAll(sqlModel.getParams());
        
        return findBySql(sql.toString(),listParams, clazz);
	}
}
