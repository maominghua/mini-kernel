package com.gcloud.controller.network.provider.enums.neutron;


import com.gcloud.header.compute.enums.NetProtocol;

import java.util.Arrays;

public enum NeutronNetProtocol {
    TCP("TCP", NetProtocol.TCP),
    UDP("UDP", NetProtocol.UDP),
    ICMP("ICMP", NetProtocol.ICMP);

    NeutronNetProtocol(String neutronNetProtocol, NetProtocol netProtocol) {
        this.neutronNetProtocol = neutronNetProtocol;
        this.netProtocol = netProtocol;
    }

    private String neutronNetProtocol;
    private NetProtocol netProtocol;

    public String getNeutronNetProtocol() {
        return neutronNetProtocol;
    }

    public NetProtocol getNetProtocol() {
        return netProtocol;
    }

    public static String toGCloudValue(String neutronValue){
        NeutronNetProtocol protocol = Arrays.stream(NeutronNetProtocol.values()).filter(o -> o.getNeutronNetProtocol().equals(neutronValue)).findFirst().orElse(null);
        return protocol == null ? null : protocol.getNetProtocol().value();
    }

    public static String getByGcValue(String gcValue){
        NeutronNetProtocol protocol = Arrays.stream(NeutronNetProtocol.values()).filter(o -> o.getNetProtocol().value().equals(gcValue)).findFirst().orElse(null);
        return protocol == null ? null : protocol.getNeutronNetProtocol();
    }
}
