package com.gcloud.controller.network.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.header.compute.enums.QosDirection;

public interface IQosProvider extends IResourceProvider {

    String createQosPolicy(String name, String description, Boolean isDefault, Boolean shared);

    void deleteQosPolicy(String refId);

    String createQosBandwidthLimitRule(String policyRefId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);

    void updateQosBandwidthLimitRule(String policyRefId, String ruleRefId, Integer maxKbps, Integer maxBurstKbps, QosDirection qosDirection);

    void deleteQosBandwidthLimitRule(String policyRefId, String ruleRefId);

    void bindPort(String portRefId, String policyRefId);

    void bindFip(String fipRefId, String policyRefId);

}
