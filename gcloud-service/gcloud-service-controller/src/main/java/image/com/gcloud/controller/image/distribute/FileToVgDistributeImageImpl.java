package com.gcloud.controller.image.distribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.enums.ImageDistributeTargetType;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.image.msg.node.DownloadImageMsg;

@Component
public class FileToVgDistributeImageImpl implements IDistributeImage {
	@Autowired
	ImageProp prop;
	
	@Autowired
	private MessageBus bus;

	@Override
	public void distributeImage(String imageId, String target, String taskId) {
		String controllerNode = MessageUtil.controllerServiceId().substring(MessageUtil.controllerServiceId().indexOf("-") + 1);
		DownloadImageMsg downloadMsg = new DownloadImageMsg();
        downloadMsg.setServiceId(MessageUtil.imageServiceId(controllerNode));
        downloadMsg.setImageId(imageId);
        downloadMsg.setImagePath(prop.getImageFilesystemStoreDir() + imageId);
        downloadMsg.setProvider(ProviderType.GCLOUD.name().toLowerCase());
        downloadMsg.setImageStroageType(ImageDriverEnum.FILE.getStorageType());
        downloadMsg.setTarget(target);
        downloadMsg.setTargetType(ImageDistributeTargetType.VG.value());
        downloadMsg.setTaskId(taskId);
        
        bus.send(downloadMsg);
		//后续考虑统一放到node节点上执行，也更好统一的feedback，并且符合长操作统一在node执行的规范
		//将镜像文件dd到对应的vg上  dd if=$1 of=/dev/$2/$3 bs=5M
		//需要创建镜像lv？  // lvcreate -L 2500 -n volumeName poolName
		/*Image img = imageDao.getById(imageId);
		LvmUtil.lvCreate(target, "img-" + imageId, (int)Math.ceil(img.getSize()/1024.0/1024.0/1024.0), "创建镜像：" + imageId + "lv失败");
		LvmUtil.activeLv(LvmUtil.getImagePath(target, imageId));
        LvmUtil.ddToVg(prop.getImageFilesystemStoreDir() + imageId, target, "img-" + imageId, "::复制镜像到vg:" + target + "失败");
		//feedback task
        if(StringUtils.isNotBlank(taskId)) {
        	WorkFlowEngine.feedbackHandler(taskId, FeedbackState.SUCCESS.name(), "");
        }*/
	}
	
	/*@Override
	public void deleteImageCache(String imageId, String target, String taskId) {
		//删掉vg上对应的image cache
	}*/

}
