package com.gcloud.controller.image.distribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.controller.image.driver.ImageDriverEnum;
import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.controller.image.enums.ImageDistributeTargetType;
import com.gcloud.controller.image.enums.ImageStoreStatus;
import com.gcloud.controller.image.prop.ImageProp;
import com.gcloud.controller.image.service.IImageStoreService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.engine.WorkFlowEngine;
import com.gcloud.core.workflow.enums.FeedbackState;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.image.msg.node.DownloadImageMsg;

@Component
public class FileToRbdDistributeImageImpl implements IDistributeImage {
	
	@Autowired
	ImageProp prop;
	
	@Autowired
	IImageStoreService imageStoreService;
	
	@Autowired
	private MessageBus bus;

	@Override
	public void distributeImage(String imageId, String target, String taskId) {
		String controllerNode = MessageUtil.controllerServiceId().substring(MessageUtil.controllerServiceId().indexOf("-") + 1);
		DownloadImageMsg downloadMsg = new DownloadImageMsg();
        downloadMsg.setServiceId(MessageUtil.imageServiceId(controllerNode));
        downloadMsg.setImageId(imageId);
        downloadMsg.setImagePath(prop.getImageFilesystemStoreDir() + imageId);
        downloadMsg.setProvider(ProviderType.GCLOUD.name().toLowerCase());
        downloadMsg.setImageStroageType(ImageDriverEnum.FILE.getStorageType());
        downloadMsg.setTarget(target);
        downloadMsg.setTargetType(ImageDistributeTargetType.RBD.value());
        downloadMsg.setTaskId(taskId);
        
        bus.send(downloadMsg);
		
		
		//将qcow2镜像文件转化为raw格式的临时文件
		//qemu-img convert -f qcow2 -O raw windows2003.qcow2 windows2003.raw
		/*String[] formatCmd = new String[]{"qemu-img", "convert", "-f", "qcow2", "-O", "raw", prop.getImageFilesystemStoreDir() + imageId, prop.getImageFilesystemStoreDir() + imageId + ".raw"};
        this.exec(formatCmd, "::qcow2镜像文件转化为raw格式失败");
		//将raw文件copy到rbd上
		//rbd -p images import windows2003.raw --image ${imageId}
        String[] copyCmd = new String[]{"rbd", "-p", "images", "import", prop.getImageFilesystemStoreDir() + imageId + ".raw", "--image", imageId};
        this.exec(copyCmd, "::将raw格式镜像文件copy到images池上失败");
        //删除临时文件
        String[] deleteCmd = new String[]{"rm", "-f", prop.getImageFilesystemStoreDir() + imageId + ".raw"};
        this.exec(deleteCmd, "::删除临时raw格式镜像文件失败");
        
        Map<String, Object> props = new HashMap<String, Object>();
		props.put("imageId", imageId);
		props.put("storeTarget", "images");
		ImageStore store = imageStoreService.findUniqueByProperties(props);
		
		List<String> updateFields = new ArrayList<String>();
		updateFields.add(store.updateStatus(ImageStoreStatus.ACTIVE.value()));
		imageStoreService.update(store, updateFields);
        
		//feedback task
        if(StringUtils.isNotBlank(taskId)) {
        	WorkFlowEngine.feedbackHandler(taskId, FeedbackState.SUCCESS.name(), "");
        }*/
	}
	
	/*private void exec(String[] cmd, String errorCode) {
		int res = SystemUtil.runAndGetCode(cmd);
		if(res != 0) {
			throw new GCloudException(errorCode);
		}
	}*/

	/*@Override
	public void deleteImageCache(String imageId, String target,String storeType, String taskId) {
		//删掉images池上对应的image cache , rbd rm  -p images  imageId 
		String[] deleteCmd = new String[]{"rbd", "rm", "-p", "images" , imageId};
        int deleteRes = SystemUtil.runAndGetCode(deleteCmd);
        if(deleteRes != 0) {
        	throw new GCloudException("::删除images池上的镜像" + imageId + "失败");
        }
	}*/

}
