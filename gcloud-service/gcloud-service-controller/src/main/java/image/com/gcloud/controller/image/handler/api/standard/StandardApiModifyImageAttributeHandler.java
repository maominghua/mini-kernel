package com.gcloud.controller.image.handler.api.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.msg.api.standard.StandardApiModifyImageAttributeMsg;
import com.gcloud.header.image.msg.api.standard.StandardApiModifyImageAttributeReplyMsg;

@GcLog(taskExpect = "修改镜像属性")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="ModifyImageAttribute", versions = {ApiVersion.Standard})
public class StandardApiModifyImageAttributeHandler extends MessageHandler<StandardApiModifyImageAttributeMsg, StandardApiModifyImageAttributeReplyMsg>{

	@Autowired
    private IImageService imageService;
	
	@Override
	public StandardApiModifyImageAttributeReplyMsg handle(StandardApiModifyImageAttributeMsg msg)
			throws GCloudException {
		imageService.updateImage(msg.getImageId(), msg.getImageName());
        msg.setObjectId(msg.getImageId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.IMAGE_NAME, msg.getImageId()));
        return new StandardApiModifyImageAttributeReplyMsg();
	}

}
