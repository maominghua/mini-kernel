package com.gcloud.controller.image.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.model.DetailImage;
import com.gcloud.header.image.msg.api.ApiDetailImageMsg;
import com.gcloud.header.image.msg.api.ApiDetailImageReplyMsg;
import com.gcloud.controller.image.model.DetailImageParams;

@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DetailImage")
public class ApiDetailImageHandler extends MessageHandler<ApiDetailImageMsg, ApiDetailImageReplyMsg>{
	@Autowired
	private IImageService imageService;
	 
	@Override
	public ApiDetailImageReplyMsg handle(ApiDetailImageMsg msg) throws GCloudException {
		DetailImageParams params = BeanUtil.copyProperties(msg, DetailImageParams.class);
		DetailImage response = imageService.detailImage(params, msg.getCurrentUser());
		
		ApiDetailImageReplyMsg repley = new ApiDetailImageReplyMsg();
		repley.setDetailImage(response);
		return repley;
	}

}
