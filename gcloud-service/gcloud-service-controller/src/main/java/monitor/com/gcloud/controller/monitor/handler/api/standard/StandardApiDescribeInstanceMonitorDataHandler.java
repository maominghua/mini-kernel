package com.gcloud.controller.monitor.handler.api.standard;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.monitor.service.IMonitorService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.monitor.model.DescribeInstanceMonitorDataResponse;
import com.gcloud.header.monitor.model.InstanceMonitorDataType;
import com.gcloud.header.monitor.model.standard.StandardDescribeInstanceMonitorDataResponse;
import com.gcloud.header.monitor.model.standard.StandardInstanceMonitorDataType;
import com.gcloud.header.monitor.msg.api.ApiDescribeInstanceMonitorDataHandlerMsg;
import com.gcloud.header.monitor.msg.api.ApiDescribeInstanceMonitorDataHandlerReplyMsg;
import com.gcloud.header.monitor.msg.api.standard.StandardApiDescribeInstanceMonitorDataMsg;
import com.gcloud.header.monitor.msg.api.standard.StandardApiDescribeInstanceMonitorDataReplyMsg;

@ApiHandler(module = Module.ECS, subModule = SubModule.VM ,action = "DescribeInstanceMonitorData", versions = {ApiVersion.Standard})
public class StandardApiDescribeInstanceMonitorDataHandler extends MessageHandler<StandardApiDescribeInstanceMonitorDataMsg, StandardApiDescribeInstanceMonitorDataReplyMsg>{
	
	@Autowired
    private IMonitorService monitorService;
	
	@Override
	public StandardApiDescribeInstanceMonitorDataReplyMsg handle(StandardApiDescribeInstanceMonitorDataMsg msg)
			throws GCloudException {
		ApiDescribeInstanceMonitorDataHandlerReplyMsg reply = new ApiDescribeInstanceMonitorDataHandlerReplyMsg();

		try {
			reply = monitorService.describeInstanceMonitorData(toParams(msg));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		StandardApiDescribeInstanceMonitorDataReplyMsg stdReply = toStandardReply(reply);
        return stdReply;
	}
	
	public StandardApiDescribeInstanceMonitorDataReplyMsg toStandardReply(ApiDescribeInstanceMonitorDataHandlerReplyMsg reply) {
		if(null == reply) {
			return null;
		}
		
		StandardApiDescribeInstanceMonitorDataReplyMsg stdReply = new StandardApiDescribeInstanceMonitorDataReplyMsg();
		StandardDescribeInstanceMonitorDataResponse stdResponse = new StandardDescribeInstanceMonitorDataResponse();
		
		DescribeInstanceMonitorDataResponse response = reply.getMonitorData();
		if(null == response) {
			stdResponse = null;
		} else {
			List<InstanceMonitorDataType> data = response.getInstanceMonitorData();
			List<StandardInstanceMonitorDataType> stdData = new ArrayList<>();
			for (InstanceMonitorDataType item : data) {
				StandardInstanceMonitorDataType tmp = BeanUtil.copyProperties(item, StandardInstanceMonitorDataType.class);
				stdData.add(tmp);
			}
			stdResponse.setInstanceMonitorData(stdData);
		}
		
		stdReply.setMonitorData(stdResponse);
		return stdReply;
	}
	
	public ApiDescribeInstanceMonitorDataHandlerMsg toParams(StandardApiDescribeInstanceMonitorDataMsg msg) {
		ApiDescribeInstanceMonitorDataHandlerMsg params = BeanUtil.copyProperties(msg, ApiDescribeInstanceMonitorDataHandlerMsg.class);
		return params;
	}

}
