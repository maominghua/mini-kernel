package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDeleteLoadBalancerListenerMsg;
import com.gcloud.header.slb.msg.api.ApiDeleteLoadBalancerListenerReplyMsg;

@GcLog(taskExpect = "删除监听器")
@ApiHandler(module=Module.SLB,action="DeleteLoadBalancerListener")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiDeleteLoadBalancerListenerHandler extends MessageHandler<ApiDeleteLoadBalancerListenerMsg, ApiDeleteLoadBalancerListenerReplyMsg>{

	@Autowired
	ILoadBalancerListenerService service;
	
	@Override
	public ApiDeleteLoadBalancerListenerReplyMsg handle(ApiDeleteLoadBalancerListenerMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		service.deleteLoadBalancerListener(msg.getListenerId());
		ApiDeleteLoadBalancerListenerReplyMsg reply = new ApiDeleteLoadBalancerListenerReplyMsg();
		return reply;
	}

}
