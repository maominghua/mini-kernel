package com.gcloud.controller.slb.handler.api.vservergroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.IVServerGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiCreateVServerGroupMsg;
import com.gcloud.header.slb.msg.api.ApiCreateVServerGroupReplyMsg;
@GcLog(taskExpect = "创建后端服务器组")
@ApiHandler(module=Module.SLB,action="CreateVServerGroup")
public class ApiCreateVServerGroupHandler extends MessageHandler<ApiCreateVServerGroupMsg, ApiCreateVServerGroupReplyMsg> {
	@Autowired
	private IVServerGroupService vServerGroupService;
	
	@Override
	public ApiCreateVServerGroupReplyMsg handle(ApiCreateVServerGroupMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		ApiCreateVServerGroupReplyMsg reply=new ApiCreateVServerGroupReplyMsg();
		String id=vServerGroupService.createVServerGroup(msg.getLoadBalancerId(), msg.getvServerGroupName(), msg.getvServerGroupProtocol());
		reply.setvServerGroupId(id);
		return reply;
	}

}
