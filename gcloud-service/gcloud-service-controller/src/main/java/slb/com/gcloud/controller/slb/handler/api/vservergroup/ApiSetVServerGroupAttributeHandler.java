package com.gcloud.controller.slb.handler.api.vservergroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.IVServerGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetVServerGroupAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiSetVServerGroupAttributeReplyMsg;
@GcLog(taskExpect = "更新后端服务器组")
@ApiHandler(module=Module.SLB,action="SetVServerGroupAttribute")
public class ApiSetVServerGroupAttributeHandler extends MessageHandler<ApiSetVServerGroupAttributeMsg, ApiSetVServerGroupAttributeReplyMsg> {
	
	@Autowired
	private IVServerGroupService vServerGroupService;
	
	@Override
	public ApiSetVServerGroupAttributeReplyMsg handle(ApiSetVServerGroupAttributeMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		vServerGroupService.setVServerGroupAttribute(msg.getvServerGroupId(), msg.getvServerGroupName());
		msg.setObjectId(msg.getvServerGroupId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.VSEVERGROUP_NAME, msg.getvServerGroupId()));
		return new ApiSetVServerGroupAttributeReplyMsg();
	}

}
