package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetLoadBalancerHTTPListenerMsg;
import com.gcloud.header.slb.msg.api.ApiSetLoadBalancerHTTPListenerReplyMsg;

@GcLog(taskExpect = "配置HTTP监听器")
@ApiHandler(module=Module.SLB,action="SetLoadBalancerHTTPListenerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiSetLoadBalancerHTTPListenerHandler extends MessageHandler<ApiSetLoadBalancerHTTPListenerMsg, ApiSetLoadBalancerHTTPListenerReplyMsg> {

	@Autowired
	ILoadBalancerListenerService service;

	@Override
	public ApiSetLoadBalancerHTTPListenerReplyMsg handle(ApiSetLoadBalancerHTTPListenerMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		service.setLoadBalancerHTTPListenerAttribute(msg.getListenerId(), msg.getvServerGroupId());
		ApiSetLoadBalancerHTTPListenerReplyMsg reply = new ApiSetLoadBalancerHTTPListenerReplyMsg();
		return reply;
	}

}
