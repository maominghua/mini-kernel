package com.gcloud.controller.slb.dao;


import org.springframework.stereotype.Repository;

import com.gcloud.controller.slb.entity.VServerGroup;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;

@Repository
public class VServerGroupDao extends JdbcBaseDaoImpl<VServerGroup, String>{

}
