package com.gcloud.controller.slb.service;

import java.util.List;

import com.gcloud.controller.slb.entity.VServerGroup;
import com.gcloud.controller.slb.model.DescribeVServerGroupAttributeResponse;

public interface IVServerGroupService {
	 String createVServerGroup(String loadBalancerId,String vServerGroupName,String vServerGroupProtocol);
	 void setVServerGroupAttribute(String vServerGroupId,String vServerGroupName);
	 List<VServerGroup> describeVServerGroups(String loadBalancerId);
	 DescribeVServerGroupAttributeResponse describeVServerGroupAttribute(String vServerGroupId);
	 void deleteVServerGroup(String vServerGroupId);
	 void addVServerGroupBackendServers(String vServerGroupId,String backendServers);
	 void removeVServerGroupBackendServers(String vServerGroupId,String backendServers);
	 void setVServerGroupBackendServers(String vServerGroupId,String backendServers);
}
