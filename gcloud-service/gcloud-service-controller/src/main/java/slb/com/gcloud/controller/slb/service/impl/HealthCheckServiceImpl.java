package com.gcloud.controller.slb.service.impl;

import com.gcloud.controller.slb.dao.ListenerDao;
import com.gcloud.controller.slb.dao.VServerGroupDao;
import com.gcloud.controller.slb.entity.Listener;
import com.gcloud.controller.slb.entity.VServerGroup;
import com.gcloud.controller.slb.model.DescribeHealthCheckAttributeResponse;
import com.gcloud.controller.slb.model.SetHealthCheckAttributeParams;
import com.gcloud.controller.slb.provider.IHealthCheckProvider;
import com.gcloud.controller.slb.service.IHealthCheckService;
import com.gcloud.core.exception.GCloudException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
@Slf4j
@Transactional(propagation = Propagation.REQUIRES_NEW)
@Service
public class HealthCheckServiceImpl implements IHealthCheckService{
	@Autowired
	VServerGroupDao vServerGroupDao;
	@Autowired
	ListenerDao listenerDao;
	@Autowired
	private IHealthCheckProvider healthCheckProvider;
	public void setHealthCheckAttribute(SetHealthCheckAttributeParams params) {
		String refId=null;
		VServerGroup vsg = vServerGroupDao.getById(params.getResourceId());
		if(vsg==null) {
			Listener listener= listenerDao.getById(params.getResourceId());
			if(listener!=null)
				refId=listener.getProviderRefId();
		}else {
			refId=vsg.getProviderRefId();
		}
		if(refId==null) {
			throw new GCloudException("Not Found");
		}
		params.setResourceId(refId);
		healthCheckProvider.setHealthCheckAttribute(params);
	}
	public DescribeHealthCheckAttributeResponse describeHealthCheckAttribute(String resourceId) {
		String refId=null;
		VServerGroup vsg = vServerGroupDao.getById(resourceId);
		if(vsg==null) {
			Listener listener= listenerDao.getById(resourceId);
			if(listener!=null)
				refId=listener.getProviderRefId();
		}else {
			refId=vsg.getProviderRefId();
		}
		if(refId==null) {
			throw new GCloudException("Not Found");
		}
		return healthCheckProvider.describeHealthCheckAttribute(refId);
	}

}
