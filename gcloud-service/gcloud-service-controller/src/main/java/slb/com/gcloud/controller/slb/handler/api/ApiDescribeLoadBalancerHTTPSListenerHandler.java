package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.model.DescribeLoadBalancerHTTPSListenerAttributeResponse;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerHTTPSListenerMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerHTTPSListenerReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeLoadBalancerHTTPSListenerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiDescribeLoadBalancerHTTPSListenerHandler extends MessageHandler<ApiDescribeLoadBalancerHTTPSListenerMsg, ApiDescribeLoadBalancerHTTPSListenerReplyMsg>{

	@Autowired
	ILoadBalancerListenerService service;

	@Override
	public ApiDescribeLoadBalancerHTTPSListenerReplyMsg handle(ApiDescribeLoadBalancerHTTPSListenerMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		DescribeLoadBalancerHTTPSListenerAttributeResponse response = service.describeLoadBalancerHTTPSListenerAttribute(msg.getListenerId());
		ApiDescribeLoadBalancerHTTPSListenerReplyMsg reply = BeanUtil.copyProperties(response, ApiDescribeLoadBalancerHTTPSListenerReplyMsg.class);
		return reply;
	}

}
