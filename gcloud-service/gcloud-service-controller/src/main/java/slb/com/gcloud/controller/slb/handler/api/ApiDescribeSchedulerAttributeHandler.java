package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.model.DescribeSchedulerAttributeResponse;
import com.gcloud.controller.slb.service.ISchedulerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDescribeSchedulerAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeSchedulerAttributeReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeSchedulerAttribute")
public class ApiDescribeSchedulerAttributeHandler extends MessageHandler<ApiDescribeSchedulerAttributeMsg, ApiDescribeSchedulerAttributeReplyMsg> {

	@Autowired
	ISchedulerService service;
	
	@Override
	public ApiDescribeSchedulerAttributeReplyMsg handle(ApiDescribeSchedulerAttributeMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		DescribeSchedulerAttributeResponse response = service.describeSchedulerAttribute(msg.getResourceId(), msg.getProtocol());
		ApiDescribeSchedulerAttributeReplyMsg reply = new ApiDescribeSchedulerAttributeReplyMsg();
		reply.setScheduler(response.getScheduler());
		return reply;
	}

}
