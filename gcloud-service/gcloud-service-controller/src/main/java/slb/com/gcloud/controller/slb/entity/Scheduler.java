package com.gcloud.controller.slb.entity;


import java.util.Date;

import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

@Table(name="gc_slb_scheduler",jdbc="controllerJdbcTemplate")
public class Scheduler {
	
	@ID
	private String id;
	private String protocol;
	private String scheduler;
	private String listenerId;
	private String vserverGroupId;
	private Integer provider;
	private String providerRefId;
	private Date updatedAt;
	
	public static final String ID = "id";
	public static final String PROTOCOL = "protocol";
	public static final String SCHEDULER = "scheduler";
	public static final String LISTENER_ID = "listenerId";
	public static final String VSERVER_GROUP_ID = "vserverGroupId";
	public static final String PROVIDER = "provider";
	public static final String PROVIDER_REF_ID = "providerRefId";
	public static final String UPDATED_AT = "updatedAt";
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getScheduler() {
		return scheduler;
	}
	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}
	public String getListenerId() {
		return listenerId;
	}
	public void setListenerId(String listenerId) {
		this.listenerId = listenerId;
	}
	public String getVserverGroupId() {
		return vserverGroupId;
	}
	public void setVserverGroupId(String vserverGroupId) {
		this.vserverGroupId = vserverGroupId;
	}
	public Integer getProvider() {
		return provider;
	}
	public void setProvider(Integer provider) {
		this.provider = provider;
	}
	public String getProviderRefId() {
		return providerRefId;
	}
	public void setProviderRefId(String providerRefId) {
		this.providerRefId = providerRefId;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String updateId(String id) {
        this.setId(id);
        return ID;
    }
	public String updateProtocol(String protocol) {
        this.setProtocol(protocol);
        return PROTOCOL;
    }
	public String updateScheduler(String scheduler) {
        this.setScheduler(scheduler);
        return SCHEDULER;
    }
	public String updateListenerId(String listenerId) {
        this.setListenerId(listenerId);
        return LISTENER_ID;
    }
	public String updateVserverGroupId(String vserverGroupId) {
        this.setVserverGroupId(vserverGroupId);
        return VSERVER_GROUP_ID;
    }
	public String updateProvider(Integer provider) {
        this.setProvider(provider);
        return PROVIDER;
    }
	public String updateProviderRefId(String providerRefId) {
        this.setProviderRefId(providerRefId);
        return PROVIDER_REF_ID;
    }
	public String updateUpdatedAt(Date updatedAt) {
        this.setUpdatedAt(updatedAt);
        return UPDATED_AT;
    }
	
	
}
