package com.gcloud.controller.slb.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.slb.model.DescribeLoadBalancerHTTPListenerAttributeResponse;
import com.gcloud.controller.slb.model.DescribeLoadBalancerHTTPSListenerAttributeResponse;
import com.gcloud.controller.slb.model.DescribeLoadBalancerTCPListenerAttributeResponse;

public interface ILoadBalancerListenerProvider extends IResourceProvider {
	
	String createLoadBalancerHTTPListener(String loadBalancerId, Integer listenerPort, String vServerGroupId);
    
    String createLoadBalancerHTTPSListener(String loadBalancerId, Integer listenerPort, String vServerGroupId, String serverCertificateId);
    
    String createLoadBalancerTCPListener(String loadBalancerId, Integer listenerPort, String vServerGroupId);
    
    void setLoadBalancerHTTPListenerAttribute(String listenerId, String vServerGroupId);
    
    void setLoadBalancerHTTPSListenerAttribute(String listenerId, String vServerGroupId, String serverCertificateId);
    
    void setLoadBalancerTCPListenerAttribute(String listenerId, String vServerGroupId);
    
    void deleteLoadBalancerListener(String listenerId);
    
    DescribeLoadBalancerHTTPListenerAttributeResponse describeLoadBalancerHTTPListenerAttribute(String listenerId);
    
    DescribeLoadBalancerHTTPSListenerAttributeResponse describeLoadBalancerHTTPSListenerAttribute(String listenerId);
    
    DescribeLoadBalancerTCPListenerAttributeResponse describeLoadBalancerTCPListenerAttribute(String listenerId);
}
