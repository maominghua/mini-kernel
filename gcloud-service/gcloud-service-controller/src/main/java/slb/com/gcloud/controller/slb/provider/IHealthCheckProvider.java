package com.gcloud.controller.slb.provider;

import com.gcloud.controller.slb.model.DescribeHealthCheckAttributeResponse;
import com.gcloud.controller.slb.model.SetHealthCheckAttributeParams;

public interface IHealthCheckProvider {
	void setHealthCheckAttribute(SetHealthCheckAttributeParams params);
	DescribeHealthCheckAttributeResponse describeHealthCheckAttribute(String resourceId);
}
