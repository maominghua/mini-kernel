package com.gcloud.controller.slb.service;

import com.gcloud.controller.slb.model.DescribeHealthCheckAttributeResponse;
import com.gcloud.controller.slb.model.SetHealthCheckAttributeParams;

public interface IHealthCheckService {
	void setHealthCheckAttribute(SetHealthCheckAttributeParams params);
	DescribeHealthCheckAttributeResponse describeHealthCheckAttribute(String resourceId);
}
