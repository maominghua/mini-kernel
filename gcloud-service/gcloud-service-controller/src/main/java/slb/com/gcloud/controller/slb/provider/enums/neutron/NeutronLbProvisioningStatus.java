package com.gcloud.controller.slb.provider.enums.neutron;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.IResourceState;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.slb.enums.LbProvisioningStatus;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class NeutronLbProvisioningStatus implements IResourceState {


    public enum State{
        ACTIVE(org.openstack4j.model.network.ext.LbProvisioningStatus.ACTIVE, LbProvisioningStatus.ACTIVE),
        DOWN(org.openstack4j.model.network.ext.LbProvisioningStatus.DOWN, LbProvisioningStatus.DOWN),
        CREATED(org.openstack4j.model.network.ext.LbProvisioningStatus.CREATED, LbProvisioningStatus.CREATED),
        PENDING_CREATE(org.openstack4j.model.network.ext.LbProvisioningStatus.PENDING_CREATE, LbProvisioningStatus.PENDING_CREATE),
        PENDING_UPDATE(org.openstack4j.model.network.ext.LbProvisioningStatus.PENDING_UPDATE, LbProvisioningStatus.PENDING_UPDATE),
        PENDING_DELETE(org.openstack4j.model.network.ext.LbProvisioningStatus.PENDING_DELETE, LbProvisioningStatus.PENDING_DELETE),
        INACTIVE(org.openstack4j.model.network.ext.LbProvisioningStatus.INACTIVE, LbProvisioningStatus.INACTIVE),
        ERROR(org.openstack4j.model.network.ext.LbProvisioningStatus.ERROR, LbProvisioningStatus.ERROR);


        private org.openstack4j.model.network.ext.LbProvisioningStatus neutronStatus;
        private LbProvisioningStatus status;

        State(org.openstack4j.model.network.ext.LbProvisioningStatus neutronStatus, LbProvisioningStatus status) {
            this.neutronStatus = neutronStatus;
            this.status = status;

        }


        public static String value(String neutronStatus){
            NeutronLbProvisioningStatus.State state = getByNeutronStatus(neutronStatus);
            return state == null ? null : state.getStatus().value();
        }

        public static String cnName(String neutronStatus){
            NeutronLbProvisioningStatus.State state = getByNeutronStatus(neutronStatus);
            return state == null ? null : state.getStatus().getCnName();
        }

        public static NeutronLbProvisioningStatus.State getByNeutronStatus(String neutronStatus){
            if(StringUtils.isBlank(neutronStatus)){
                return null;
            }
            return Arrays.stream(NeutronLbProvisioningStatus.State.values()).filter(s -> s.getNeutronStatus().name().equalsIgnoreCase(neutronStatus)).findFirst().orElse(null);
        }

        public org.openstack4j.model.network.ext.LbProvisioningStatus getNeutronStatus() {
            return neutronStatus;
        }

        public LbProvisioningStatus getStatus() {
            return status;
        }
    }

    @Override
    public ResourceType resourceType() {
        return ResourceType.SLB;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.NEUTRON;
    }

    @Override
    public String value(String status) {
        return NeutronLbProvisioningStatus.State.value(status);
    }

    @Override
    public String cnName(String status) {
        return NeutronLbProvisioningStatus.State.cnName(status);
    }
}
