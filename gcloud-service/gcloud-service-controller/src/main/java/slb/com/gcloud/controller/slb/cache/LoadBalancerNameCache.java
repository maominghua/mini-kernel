package com.gcloud.controller.slb.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.core.cache.Cache;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.service.SpringUtil;
@Component
public class LoadBalancerNameCache extends Cache<String> {

	@Override
	public Map<String, String> requestCache() {
		Map<String,String> result=new HashMap<String, String>();
		LoadBalancerDao loadBalancerDao = SpringUtil.getBean(LoadBalancerDao.class);
		List<LoadBalancer> list = loadBalancerDao.findAll();
		for(LoadBalancer item: list){
			result.put(item.getId(),item.getName()==null?item.getId():item.getName());
		}
		return result;
	}

	@Override
	public CacheType getType() {
		return CacheType.LOADBALANCER_NAME;
	}

	@Override
	public String getValue(String key) {
		LoadBalancerDao loadBalancerDao = SpringUtil.getBean(LoadBalancerDao.class);
		LoadBalancer loadBalancer = loadBalancerDao.getById(key);
		return loadBalancer!=null?(loadBalancer.getName()==null?loadBalancer.getId():loadBalancer.getName()):null;
	}

}
