package com.gcloud.controller.slb.provider.enums;

public enum SchedulerPolicy {
	
	 ROUND_ROBIN("ROUND_ROBIN", "轮询"),
	 LEAST_CONNECTIONS("LEAST_CONNECTIONS", "最少连接数"),
	 SOURCE_IP("SOURCE_IP", "ip绑定");
	 
	 private String value;
	 private String cnName;
	 
	 SchedulerPolicy(String value, String cnName){
		 this.value = value;
		 this.cnName = cnName;
	 }
	 
	 public String getValue() {
		 return this.value;
	 }
	 
	 public String getCnName() {
		 return this.cnName;
	 }
}
