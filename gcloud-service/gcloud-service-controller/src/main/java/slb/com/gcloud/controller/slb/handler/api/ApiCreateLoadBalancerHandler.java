package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerMsg;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerReplyMsg;


@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@ApiHandler(module=Module.SLB,action="CreateLoadBalancer")
public class ApiCreateLoadBalancerHandler extends MessageHandler<CreateLoadBalancerMsg, CreateLoadBalancerReplyMsg> {
    @Autowired
	ILoadBalancerService   service;
	
	public CreateLoadBalancerReplyMsg handle(CreateLoadBalancerMsg msg) throws GCloudException {
		// TODO Auto-generated method stub		
		return service.createLb(msg);
	}

}
