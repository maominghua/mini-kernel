package com.gcloud.controller.slb.service;

import com.gcloud.controller.slb.model.DescribeSchedulerAttributeResponse;

public interface ISchedulerService {
	
	void setSchedulerAttribute(String resourceId, String protocol, String scheduler);
    
    DescribeSchedulerAttributeResponse describeSchedulerAttribute(String resourceId, String protocol);
}
