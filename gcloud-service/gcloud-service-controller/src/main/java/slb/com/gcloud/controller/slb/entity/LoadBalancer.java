package com.gcloud.controller.slb.entity;

import com.gcloud.controller.ResourceProviderEntity;
import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;

import java.util.Date;

@Table(name = "gc_slb", jdbc = "controllerJdbcTemplate")
public class LoadBalancer extends ResourceProviderEntity {

    @ID
    private String id;
    private String name;
    private String regionId;
    private String vipSubnetId;
    private String vipPortId;
    private String status;
    private Date createTime;
    private Date updatedAt;
    private Integer provider;
    private String providerRefId;
    private String userId;
    private String tenantId;

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String REGION_ID = "regionId";
    public static final String VIP_SUBNET_ID = "vipSubnetId";
    public static final String VIP_PORT_ID = "vipPortId";
    public static final String STATUS = "status";
    public static final String CREATE_TIME = "createTime";
    public static final String UPDATED_AT = "updatedAt";
    public static final String PROVIDER = "provider";
    public static final String PROVIDER_REF_ID = "providerRefId";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getVipSubnetId() {
        return vipSubnetId;
    }

    public void setVipSubnetId(String vipSubnetId) {
        this.vipSubnetId = vipSubnetId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getProvider() {
        return provider;
    }

    public void setProvider(Integer provider) {
        this.provider = provider;
    }

    public String getProviderRefId() {
        return providerRefId;
    }

    public void setProviderRefId(String providerRefId) {
        this.providerRefId = providerRefId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getVipPortId() {
        return vipPortId;
    }

    public void setVipPortId(String vipPortId) {
        this.vipPortId = vipPortId;
    }

    public String updateId(String id) {
        this.setId(id);
        return ID;
    }

    public String updateName(String name) {
        this.setName(name);
        return NAME;
    }

    public String updateRegionId(String regionId) {
        this.setRegionId(regionId);
        return REGION_ID;
    }

    public String updateStatus(String status) {
        this.setStatus(status);
        return STATUS;
    }

    public String updateCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return CREATE_TIME;
    }

    public String updateProvider(Integer provider) {
        this.setProvider(provider);
        return PROVIDER;
    }

    public String updateProviderRefId(String providerRefId) {
        this.setProviderRefId(providerRefId);
        return PROVIDER_REF_ID;
    }

    public String updateUpdatedAt(Date updatedAt) {
        this.setUpdatedAt(updatedAt);
        return UPDATED_AT;
    }

    public String updateVipSubnetId(String vipSubnetId){
        this.setVipSubnetId(vipSubnetId);
        return VIP_SUBNET_ID;
    }

    public String updateVipPortId(String vipPortId){
        this.setVipPortId(vipPortId);
        return VIP_PORT_ID;
    }

}
