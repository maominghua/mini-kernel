package com.gcloud.controller.slb.service;

import com.gcloud.controller.slb.model.DescribeLoadBalancerAttributeResponse;
import com.gcloud.controller.slb.model.DescribeLoadBalancersParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.slb.model.DetailLoadBalancerResponse;
import com.gcloud.header.slb.model.LoadBalancerModel;
import com.gcloud.header.slb.msg.api.ApiLoadBalancersStatisticsReplyMsg;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerMsg;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerReplyMsg;

public interface ILoadBalancerService {
	CreateLoadBalancerReplyMsg  createLb(CreateLoadBalancerMsg msg);
	
	void  setLoadBalancerName(String loadBalancerId, String loadBalancerName);
	
	void  deleteLoadBalancer(String loadBalancerId);
	
	DescribeLoadBalancerAttributeResponse describeLoadBalancerAttribute(String loadBalancerId);
	
	PageResult<LoadBalancerModel> describeLoadBalancers(DescribeLoadBalancersParams params, CurrentUser currentUser);
    
	DetailLoadBalancerResponse detailLoadBalancer(String loadBalancerId);
	
	ApiLoadBalancersStatisticsReplyMsg statistic(CurrentUser currentUser);
}
