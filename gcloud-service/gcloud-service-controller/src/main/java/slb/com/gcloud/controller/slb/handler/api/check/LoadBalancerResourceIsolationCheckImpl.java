package com.gcloud.controller.slb.handler.api.check;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.core.currentUser.policy.service.ResourceIsolationCheckImpl;

@Service
public class LoadBalancerResourceIsolationCheckImpl extends ResourceIsolationCheckImpl {
	@Autowired
    private LoadBalancerDao lbDao;

	@Override
	public String getResourceTenantId(String resourceId) {
		LoadBalancer lb = lbDao.getById(resourceId);
		return lb==null?null:lb.getTenantId();
	}

}
