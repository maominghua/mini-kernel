package com.gcloud.controller.slb.handler.api.healthcheck;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.model.SetHealthCheckAttributeParams;
import com.gcloud.controller.slb.service.IHealthCheckService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetHealthCheckAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiSetHealthCheckAttributeReplyMsg;
@GcLog(taskExpect = "设置健康检查")
@ApiHandler(module=Module.SLB,action="SetHealthCheckAttribute")
public class ApiSetHealthCheckAttributeHandler extends MessageHandler<ApiSetHealthCheckAttributeMsg, ApiSetHealthCheckAttributeReplyMsg> {
	@Autowired
	private IHealthCheckService healthCheckService; 
	@Override
	public ApiSetHealthCheckAttributeReplyMsg handle(ApiSetHealthCheckAttributeMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		SetHealthCheckAttributeParams params=new SetHealthCheckAttributeParams();
		params.setResourceId(msg.getResourceId());
		params.setProtocol(msg.getProtocol());
		params.setHealthCheck(msg.getHealthCheck());
		params.setHealthCheckInterval(msg.getHealthCheckInterval());
		params.setHealthCheckTimeout(msg.getHealthCheckTimeout());
		params.setHealthCheckType(msg.getHealthCheckType());
		params.setHealthCheckURI(msg.getHealthCheckURI());
		params.setHealthyThreshold(msg.getHealthyThreshold());
		params.setUnhealthyThreshold(msg.getUnhealthyThreshold());
		healthCheckService.setHealthCheckAttribute(params);
		return new ApiSetHealthCheckAttributeReplyMsg();
	}

}
