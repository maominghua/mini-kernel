package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.model.DescribeLoadBalancerAttributeResponse;
import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerAttributeMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerAttributeReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeLoadBalancerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiDescribeLoadBalancerAttributeHandler extends MessageHandler<ApiDescribeLoadBalancerAttributeMsg, ApiDescribeLoadBalancerAttributeReplyMsg>{

	@Autowired
	ILoadBalancerService service;

	@Override
	public ApiDescribeLoadBalancerAttributeReplyMsg handle(ApiDescribeLoadBalancerAttributeMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		DescribeLoadBalancerAttributeResponse response = service.describeLoadBalancerAttribute(msg.getLoadBalancerId());
		ApiDescribeLoadBalancerAttributeReplyMsg reply = BeanUtil.copyProperties(response, ApiDescribeLoadBalancerAttributeReplyMsg.class);
		return reply;
	}
}
