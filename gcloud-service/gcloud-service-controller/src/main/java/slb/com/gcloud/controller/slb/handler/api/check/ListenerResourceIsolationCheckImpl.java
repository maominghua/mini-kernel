package com.gcloud.controller.slb.handler.api.check;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.slb.dao.ListenerDao;
import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.entity.Listener;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.core.currentUser.policy.service.ResourceIsolationCheckImpl;

@Service
public class ListenerResourceIsolationCheckImpl extends ResourceIsolationCheckImpl {
	@Autowired
	private ListenerDao listenerDao;
	
	@Autowired
    private LoadBalancerDao lbDao;
	
	@Override
	public String getResourceTenantId(String resourceId) {
		Listener listener = listenerDao.getById(resourceId);
		if(listener == null) {
			return null;
		}
		LoadBalancer lb = lbDao.getById(listener.getLoadbalancerId());
		return lb==null?null:lb.getTenantId();
	}

}
