package com.gcloud.controller.slb.model;

public class DescribeSchedulerAttributeResponse {

	
	private String scheduler;

	public String getScheduler() {
		return scheduler;
	}

	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}
	
	
}
