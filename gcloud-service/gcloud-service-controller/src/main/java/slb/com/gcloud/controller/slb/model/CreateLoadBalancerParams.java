package com.gcloud.controller.slb.model;


public class CreateLoadBalancerParams {
	
	private String  loadBalancerName;
	private String   vSwitchId;
	public String getLoadBalancerName() {
		return loadBalancerName;
	}
	public void setLoadBalancerName(String loadBalancerName) {
		this.loadBalancerName = loadBalancerName;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

}
