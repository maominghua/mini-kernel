package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.DeleteLoadBalancerMsg;

@GcLog(taskExpect = "删除负载均衡")
@ApiHandler(module=Module.SLB,action="DeleteLoadBalancer")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiDeleteLoadBalancerHandler extends MessageHandler<DeleteLoadBalancerMsg, ApiReplyMessage> {
	
	@Autowired
	ILoadBalancerService   service;

	@Override
	public ApiReplyMessage handle(DeleteLoadBalancerMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		service.deleteLoadBalancer(msg.getLoadBalancerId());
		msg.setObjectId(msg.getLoadBalancerId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.LOADBALANCER_NAME, msg.getLoadBalancerId()));
		return new ApiReplyMessage();
	}

}
