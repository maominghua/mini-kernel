package com.gcloud.controller.slb.handler.api.vservergroup;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.IVServerGroupService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetVServerGroupBackendServersMsg;
import com.gcloud.header.slb.msg.api.ApiSetVServerGroupBackendServersReplyMsg;
@GcLog(taskExpect = "设置后端服务器属性")
@ApiHandler(module=Module.SLB,action="SetVServerGroupBackendServers")
public class ApiSetVServerGroupBackendServersHandler extends MessageHandler<ApiSetVServerGroupBackendServersMsg, ApiSetVServerGroupBackendServersReplyMsg> {
	
	@Autowired
	IVServerGroupService vServerGroupService;
	@Override
	public ApiSetVServerGroupBackendServersReplyMsg handle(ApiSetVServerGroupBackendServersMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		vServerGroupService.setVServerGroupBackendServers(msg.getvServerGroupId(),msg.getBackendServers());
		msg.setObjectId(msg.getvServerGroupId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.VSEVERGROUP_NAME, msg.getvServerGroupId()));
		return new ApiSetVServerGroupBackendServersReplyMsg();
	}

}
