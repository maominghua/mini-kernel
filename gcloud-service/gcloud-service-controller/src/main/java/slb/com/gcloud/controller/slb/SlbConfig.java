package com.gcloud.controller.slb;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.handler.api.check.ListenerResourceIsolationCheckImpl;
import com.gcloud.controller.slb.handler.api.check.LoadBalancerResourceIsolationCheckImpl;

@Component
public class SlbConfig {
	@PostConstruct
	public void init() {
		ResourceIsolationCheckType.LOADBALANCER.setCheckClazz(LoadBalancerResourceIsolationCheckImpl.class);
		ResourceIsolationCheckType.LISTENER.setCheckClazz(ListenerResourceIsolationCheckImpl.class);
	}
}
