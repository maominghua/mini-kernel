package com.gcloud.controller.slb.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.gcloud.controller.slb.dao.VServerGroupDao;
import com.gcloud.controller.slb.entity.VServerGroup;
import com.gcloud.core.cache.Cache;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.service.SpringUtil;
@Component
public class VServerGroupNameCache extends Cache<String> {

	@Override
	public Map<String, String> requestCache() {
		Map<String,String> result=new HashMap<String, String>();
		VServerGroupDao vServerGroupDao = SpringUtil.getBean(VServerGroupDao.class);
		List<VServerGroup> list = vServerGroupDao.findAll();
		for(VServerGroup item: list){
			result.put(item.getId(),item.getName()==null?item.getId():item.getName());
		}
		return result;
	}

	@Override
	public CacheType getType() {
		return CacheType.VSEVERGROUP_NAME;
	}

	@Override
	public String getValue(String key) {
		VServerGroupDao vServerGroupDao = SpringUtil.getBean(VServerGroupDao.class);
		VServerGroup vServerGroup = vServerGroupDao.getById(key);
		return vServerGroup!=null?(vServerGroup.getName()==null?vServerGroup.getId():vServerGroup.getName()):null;
	}

}
