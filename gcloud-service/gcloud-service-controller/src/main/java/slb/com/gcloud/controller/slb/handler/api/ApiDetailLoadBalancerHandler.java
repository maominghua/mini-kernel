package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.slb.msg.api.ApiDetailLoadBalancerMsg;
import com.gcloud.header.slb.msg.api.ApiDetailLoadBalancerReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.QUERY)
@ApiHandler(module=Module.SLB,action="DetailLoadBalancer")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LOADBALANCER, resourceIdField = "loadBalancerId")
public class ApiDetailLoadBalancerHandler extends MessageHandler<ApiDetailLoadBalancerMsg, ApiDetailLoadBalancerReplyMsg>{
	@Autowired
	ILoadBalancerService   service;

	@Override
	public ApiDetailLoadBalancerReplyMsg handle(ApiDetailLoadBalancerMsg msg) throws GCloudException {
		ApiDetailLoadBalancerReplyMsg reply = new ApiDetailLoadBalancerReplyMsg();
		reply.setDetailLoadBalancer(service.detailLoadBalancer(msg.getLoadBalancerId()));
		return reply;
	}
}
