package com.gcloud.controller.slb.provider;

import java.util.List;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.slb.model.DescribeVServerGroupAttributeResponse;
import com.gcloud.header.slb.model.VServerGroupSetType;

public interface IVServerGroupProvider extends IResourceProvider{
	String createVServerGroup(String loadBalancerId, String vServerGroupName, String vServerGroupProtocol);
	void setVServerGroupAttribute(String vServerGroupId, String vServerGroupName);
	List<VServerGroupSetType> describeVServerGroups(String loadBalancerId);
	DescribeVServerGroupAttributeResponse describeVServerGroupAttribute(String vServerGroupId);
	void deleteVServerGroup(String vServerGroupId);
	void addVServerGroupBackendServers(String vServerGroupId,String backendServers);
	void removeVServerGroupBackendServers(String vServerGroupId,String backendServers);
	void setVServerGroupBackendServers(String vServerGroupId,String backendServers);
}
