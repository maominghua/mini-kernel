package com.gcloud.controller.slb.service.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.ResourceProviders;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.controller.slb.model.DescribeLoadBalancerAttributeResponse;
import com.gcloud.controller.slb.model.DescribeLoadBalancersParams;
import com.gcloud.controller.slb.provider.ILoadBalancerProvider;
import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.network.enums.NetworkStatus;
import com.gcloud.header.slb.enums.LbProvisioningStatus;
import com.gcloud.header.slb.model.DetailLoadBalancerResponse;
import com.gcloud.header.slb.model.LoadBalancerModel;
import com.gcloud.header.slb.msg.api.ApiLoadBalancersStatisticsReplyMsg;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerMsg;
import com.gcloud.header.slb.msg.api.CreateLoadBalancerReplyMsg;
import com.gcloud.header.slb.msg.api.ListenerPortAndProtocol;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Transactional(propagation = Propagation.REQUIRES_NEW)
@Service
public class LoadBalancerServiceImpl implements ILoadBalancerService {

    @Autowired
    private LoadBalancerDao lberDao;

    @Autowired
	private SubnetDao subnetDao;

    @Autowired
	private IPortService portService;

    @Override
    public CreateLoadBalancerReplyMsg createLb(CreateLoadBalancerMsg msg) {

    	Subnet subnet = subnetDao.getById(msg.getvSwitchId());
    	if(subnet == null){
			throw new GCloudException("::找不到子网");
		}

        return this.getProviderOrDefault().createLb(msg.getLoadBalancerName(), msg.getvSwitchId(), subnet.getProviderRefId(), msg.getRegion(), msg.getCurrentUser());
    }

    @Override
    public void setLoadBalancerName(String loadBalancerId, String loadBalancerName) {
        LoadBalancer lber = lberDao.getById(loadBalancerId);
        if (null == lber) {
			log.debug("找不到负载均衡器");
            throw new GCloudException("0110101::找不到负载均衡器");
        }  
        lber.setName(loadBalancerName);
        List<String> updateField = new ArrayList<String>();
        updateField.add(lber.updateName(loadBalancerName));
        updateField.add(lber.updateUpdatedAt(new Date()));
        lberDao.update(lber, updateField);
        CacheContainer.getInstance().put(CacheType.LOADBALANCER_NAME, loadBalancerId, loadBalancerName);
        this.checkAndGetProvider(lber.getProvider()).updateBalancerName(lber.getProviderRefId(), loadBalancerName);

    }

    @Override
    public void deleteLoadBalancer(String loadBalancerId) {
        LoadBalancer lber = lberDao.getById(loadBalancerId);
        if (null == lber) {
            log.debug("找不到负载均衡");
            throw new GCloudException("0110302::找不到负载均衡");
        }
        lberDao.delete(lber);
		portService.cleanPortData(lber.getVipPortId());
        this.checkAndGetProvider(lber.getProvider()).deleteLoadBalancer(lber.getProviderRefId());  
    }

    private ILoadBalancerProvider getProviderOrDefault() {
        ILoadBalancerProvider provider = ResourceProviders.getDefault(ResourceType.SLB);
        return provider;
    }

    private ILoadBalancerProvider checkAndGetProvider(Integer providerType) {
        ILoadBalancerProvider provider = ResourceProviders.checkAndGet(ResourceType.SLB, providerType);
        return provider;
    }

	@Override
	public DescribeLoadBalancerAttributeResponse describeLoadBalancerAttribute(String loadBalancerId) {
		// TODO Auto-generated method stub
		LoadBalancer lber = lberDao.getById(loadBalancerId);
		if (null == lber) {
			log.debug("找不到负载均衡器");
            throw new GCloudException("0110402::找不到负载均衡器");
        }
		DescribeLoadBalancerAttributeResponse response = new DescribeLoadBalancerAttributeResponse();
		List<String> loadBalancerIds = new ArrayList<String>();
		loadBalancerIds.add(loadBalancerId);
		List<LoadBalancerModel> loadBalancers = lberDao.list(loadBalancerIds);
		for(LoadBalancerModel model: loadBalancers) {
			response.setLoadBalancerId(model.getLoadBalancerId());
			response.setLoadBalancerName(model.getLoadBalancerName());
			response.setLoadBalancerStatus(model.getLoadBalancerStatus());
			response.setVpcId(model.getVpcId());
			response.setvSwitchId(model.getvSwitchId());
			response.setAddress(model.getAddress());
			response.setCreateTime(model.getCreateTime());
			List<Integer> listenerPorts = new ArrayList<Integer>();
			List<ListenerPortAndProtocol> listenerPortsAndProtocol = new ArrayList<ListenerPortAndProtocol>();
			if( model.getListener() != null && StringUtils.isNotBlank(model.getListener())) {
	            for(String listener: model.getListener().split(",")) {
	            	int index = listener.indexOf("#");
	            	int lastIndex = listener.lastIndexOf("#");
	            	
	            	String id = listener.substring(0, index);
	            	Integer port = Integer.valueOf(listener.substring(index+1, lastIndex));
	            	String protocol = listener.substring(lastIndex+1);
	            	listenerPorts.add(port);
	            	ListenerPortAndProtocol pp = new ListenerPortAndProtocol();
	            	pp.setListenerId(id);
	            	pp.setListenerPort(port);
	            	pp.setListenerProtocol(protocol);
	            	listenerPortsAndProtocol.add(pp);
	            }	
	            response.setListenerPorts(listenerPorts);
	            response.setListenerPortsAndProtocol(listenerPortsAndProtocol);
			}
		}
		
		return response;
	}

	@Override
	public PageResult<LoadBalancerModel> describeLoadBalancers(DescribeLoadBalancersParams params, CurrentUser currentUser) {
		PageResult<LoadBalancerModel> pages = lberDao.getByPage(params.getPageNumber(), params.getPageSize(), null, currentUser);
		pages.getList().forEach(p -> p.setLoadBalancerCnStatus(LbProvisioningStatus.getCnName(p.getLoadBalancerStatus())));
		return pages;
	}

	@Override
	public DetailLoadBalancerResponse detailLoadBalancer(String loadBalancerId) {
		List<String> loadBalancers = new ArrayList<String>();
		loadBalancers.add(loadBalancerId);
		List<LoadBalancerModel> lbers = lberDao.list(loadBalancers);
		if (lbers.size() == 0) {
            throw new GCloudException("0110602::找不到负载均衡器");
        }
		DetailLoadBalancerResponse res = new DetailLoadBalancerResponse();
		res = BeanUtil.copyProperties(lbers.get(0), DetailLoadBalancerResponse.class);
		res.setLoadBalancerCnStatus(LbProvisioningStatus.getCnName(res.getLoadBalancerStatus()));
		return res;
	}

	@Override
	public ApiLoadBalancersStatisticsReplyMsg statistic(CurrentUser currentUser) {
		ApiLoadBalancersStatisticsReplyMsg reply = new ApiLoadBalancersStatisticsReplyMsg();
		reply.setAllNum(lberDao.getAllLbNum(currentUser));
		return reply;
	}



}
