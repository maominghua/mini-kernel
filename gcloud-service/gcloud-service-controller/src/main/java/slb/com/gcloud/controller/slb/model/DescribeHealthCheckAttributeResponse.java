package com.gcloud.controller.slb.model;

public class DescribeHealthCheckAttributeResponse {
	private Integer unhealthyThreshold; // 不健康检查阈值
	private Integer healthCheckTimeout; // 每次健康检查响应的最大超时间，单位为秒
	private Integer healthCheckInterval; // 健康检查的时间间隔，单位为秒
	private String healthCheck; // 健康检查开关
	private String healthCheckURI; // 用于健康检查的URI。
	private Integer healthyThreshold; // 健康检查连续成功多少次后，将后端服务器的健康检查状态由fail判定为success
	private String healthCheckType;
	public Integer getUnhealthyThreshold() {
		return unhealthyThreshold;
	}
	public void setUnhealthyThreshold(Integer unhealthyThreshold) {
		this.unhealthyThreshold = unhealthyThreshold;
	}
	public Integer getHealthCheckTimeout() {
		return healthCheckTimeout;
	}
	public void setHealthCheckTimeout(Integer healthCheckTimeout) {
		this.healthCheckTimeout = healthCheckTimeout;
	}
	public Integer getHealthCheckInterval() {
		return healthCheckInterval;
	}
	public void setHealthCheckInterval(Integer healthCheckInterval) {
		this.healthCheckInterval = healthCheckInterval;
	}
	public String getHealthCheck() {
		return healthCheck;
	}
	public void setHealthCheck(String healthCheck) {
		this.healthCheck = healthCheck;
	}
	public String getHealthCheckURI() {
		return healthCheckURI;
	}
	public void setHealthCheckURI(String healthCheckURI) {
		this.healthCheckURI = healthCheckURI;
	}
	public Integer getHealthyThreshold() {
		return healthyThreshold;
	}
	public void setHealthyThreshold(Integer healthyThreshold) {
		this.healthyThreshold = healthyThreshold;
	}
	public String getHealthCheckType() {
		return healthCheckType;
	}
	public void setHealthCheckType(String healthCheckType) {
		this.healthCheckType = healthCheckType;
	}
}
