package com.gcloud.controller.slb.provider;

import com.gcloud.controller.IResourceProvider;
import com.gcloud.controller.slb.model.DescribeSchedulerAttributeResponse;

public interface ISchedulerProvider extends IResourceProvider {
    
    void setSchedulerAttribute(String resourceId, String protocol, String scheduler);
    
    DescribeSchedulerAttributeResponse describeSchedulerAttribute(String resourceId, String protocol);
}
