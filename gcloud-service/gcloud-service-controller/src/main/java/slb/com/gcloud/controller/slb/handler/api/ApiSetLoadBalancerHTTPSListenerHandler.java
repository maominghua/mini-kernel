package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiSetLoadBalancerHTTPSListenerMsg;
import com.gcloud.header.slb.msg.api.ApiSetLoadBalancerHTTPSListenerReplyMsg;

@GcLog(taskExpect = "配置HTTPS监听器")
@ApiHandler(module=Module.SLB,action="SetLoadBalancerHTTPSListenerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiSetLoadBalancerHTTPSListenerHandler extends MessageHandler<ApiSetLoadBalancerHTTPSListenerMsg, ApiSetLoadBalancerHTTPSListenerReplyMsg>{

	@Autowired
	ILoadBalancerListenerService service;
	
	@Override
	public ApiSetLoadBalancerHTTPSListenerReplyMsg handle(ApiSetLoadBalancerHTTPSListenerMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		service.setLoadBalancerHTTPSListenerAttribute(msg.getListenerId(), msg.getvServerGroupId(), msg.getServerCertificateId());
		ApiSetLoadBalancerHTTPSListenerReplyMsg reply = new ApiSetLoadBalancerHTTPSListenerReplyMsg();
		return reply;
	}

}
