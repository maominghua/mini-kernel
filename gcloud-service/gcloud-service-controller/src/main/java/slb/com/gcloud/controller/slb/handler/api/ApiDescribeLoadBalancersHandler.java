package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.model.DescribeLoadBalancersParams;
import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.slb.model.LoadBalancerModel;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancersMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancersReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeLoadBalancers")
public class ApiDescribeLoadBalancersHandler extends MessageHandler<ApiDescribeLoadBalancersMsg, ApiDescribeLoadBalancersReplyMsg>{

	@Autowired
	ILoadBalancerService service;
	
	@Override
	public ApiDescribeLoadBalancersReplyMsg handle(ApiDescribeLoadBalancersMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		DescribeLoadBalancersParams params = BeanUtil.copyProperties(msg, DescribeLoadBalancersParams.class);
        PageResult<LoadBalancerModel> response = service.describeLoadBalancers(params, msg.getCurrentUser());
        ApiDescribeLoadBalancersReplyMsg replyMsg = new ApiDescribeLoadBalancersReplyMsg();
        replyMsg.init(response);
        return replyMsg;
	}

}
