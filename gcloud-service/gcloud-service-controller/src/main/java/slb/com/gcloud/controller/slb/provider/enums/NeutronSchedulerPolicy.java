package com.gcloud.controller.slb.provider.enums;

import org.openstack4j.model.network.ext.LbMethod;

public enum NeutronSchedulerPolicy {
	 ROUND_ROBIN(LbMethod.ROUND_ROBIN, SchedulerPolicy.ROUND_ROBIN),
	 LEAST_CONNECTIONS(LbMethod.LEAST_CONNECTIONS, SchedulerPolicy.LEAST_CONNECTIONS),
	 SOURCE_IP(LbMethod.SOURCE_IP, SchedulerPolicy.SOURCE_IP);
	
	private LbMethod neutronSchedulerPolicy;
	private SchedulerPolicy schedulerPolicy;
	
	NeutronSchedulerPolicy(LbMethod neutronSchedulerPolicy, SchedulerPolicy schedulerPolicy){
		this.neutronSchedulerPolicy = neutronSchedulerPolicy;
		this.schedulerPolicy = schedulerPolicy;
	}
	
	public LbMethod getNeutronSchedulerPolicy() {
		return this.neutronSchedulerPolicy;
	}
	
	public SchedulerPolicy getSchedulerPolicy() {
		return this.schedulerPolicy;
	}
}
