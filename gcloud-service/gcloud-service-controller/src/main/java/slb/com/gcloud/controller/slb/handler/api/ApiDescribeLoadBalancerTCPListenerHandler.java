package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.model.DescribeLoadBalancerTCPListenerAttributeResponse;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerTCPListenerMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerTCPListenerReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeLoadBalancerTCPListenerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiDescribeLoadBalancerTCPListenerHandler extends MessageHandler<ApiDescribeLoadBalancerTCPListenerMsg, ApiDescribeLoadBalancerTCPListenerReplyMsg> {

	@Autowired
	ILoadBalancerListenerService service;
	
	@Override
	public ApiDescribeLoadBalancerTCPListenerReplyMsg handle(ApiDescribeLoadBalancerTCPListenerMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		DescribeLoadBalancerTCPListenerAttributeResponse response = service.describeLoadBalancerTCPListenerAttribute(msg.getListenerId());
		ApiDescribeLoadBalancerTCPListenerReplyMsg reply = BeanUtil.copyProperties(response, ApiDescribeLoadBalancerTCPListenerReplyMsg.class);
		return reply;
	}

}
