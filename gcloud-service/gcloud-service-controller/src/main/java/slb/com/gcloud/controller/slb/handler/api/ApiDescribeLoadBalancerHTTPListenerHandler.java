package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.controller.slb.model.DescribeLoadBalancerHTTPListenerAttributeResponse;
import com.gcloud.controller.slb.service.ILoadBalancerListenerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerHTTPListenerMsg;
import com.gcloud.header.slb.msg.api.ApiDescribeLoadBalancerHTTPListenerReplyMsg;

@ApiHandler(module=Module.SLB,action="DescribeLoadBalancerHTTPListenerAttribute")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.LISTENER, resourceIdField = "listenerId")
public class ApiDescribeLoadBalancerHTTPListenerHandler extends MessageHandler<ApiDescribeLoadBalancerHTTPListenerMsg, ApiDescribeLoadBalancerHTTPListenerReplyMsg> {

	@Autowired
	ILoadBalancerListenerService service;
	
	@Override
	public ApiDescribeLoadBalancerHTTPListenerReplyMsg handle(ApiDescribeLoadBalancerHTTPListenerMsg msg)
			throws GCloudException {
		// TODO Auto-generated method stub
		DescribeLoadBalancerHTTPListenerAttributeResponse response = service.describeLoadBalancerHTTPListenerAttribute(msg.getListenerId());
		ApiDescribeLoadBalancerHTTPListenerReplyMsg reply = BeanUtil.copyProperties(response, ApiDescribeLoadBalancerHTTPListenerReplyMsg.class);
		return reply;
	}

}
