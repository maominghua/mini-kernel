package com.gcloud.controller.slb.handler.api;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.slb.service.ILoadBalancerService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.slb.msg.api.ApiLoadBalancersStatisticsMsg;
import com.gcloud.header.slb.msg.api.ApiLoadBalancersStatisticsReplyMsg;

@ApiHandler(module = Module.SLB,action = "LoadBalancersStatistics")
@GcLog(taskExpect = "负载均衡统计")
public class ApiLoadBalancersStatisticsHandler extends MessageHandler<ApiLoadBalancersStatisticsMsg, ApiLoadBalancersStatisticsReplyMsg>{
	@Autowired
	ILoadBalancerService   service;
	
	@Override
	public ApiLoadBalancersStatisticsReplyMsg handle(ApiLoadBalancersStatisticsMsg msg) throws GCloudException {
		return service.statistic(msg.getCurrentUser());
	}

}
