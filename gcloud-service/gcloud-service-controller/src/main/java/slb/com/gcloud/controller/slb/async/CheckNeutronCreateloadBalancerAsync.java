package com.gcloud.controller.slb.async;

import com.gcloud.controller.ResourceStates;
import com.gcloud.controller.async.LogAsync;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.core.async.AsyncResult;
import com.gcloud.core.async.AsyncStatus;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import org.openstack4j.api.networking.PortService;
import org.openstack4j.model.network.ext.LbProvisioningStatus;
import org.openstack4j.model.network.ext.LoadBalancerV2;

import java.util.ArrayList;
import java.util.List;

public class CheckNeutronCreateloadBalancerAsync extends LogAsync {

    private String loadBalancerId;
    private String loadBalancerRefId;
    private LoadBalancerV2 lber;

    @Override
    public AsyncResult execute() {
        // TODO Auto-generated method stub
        AsyncStatus asyncStatus = null;
        NeutronProviderProxy proxy = SpringUtil.getBean(NeutronProviderProxy.class);
        lber = proxy.getLoadBalancer(loadBalancerRefId);
        if (lber.getProvisioningStatus().equals(LbProvisioningStatus.ACTIVE)) {
            asyncStatus = AsyncStatus.SUCCEED;
        } else if (lber == null || lber.getProvisioningStatus().equals(LbProvisioningStatus.ERROR) || lber.getProvisioningStatus().equals(LbProvisioningStatus.INACTIVE)) {
            asyncStatus = AsyncStatus.FAILED;
        } else {
            asyncStatus = AsyncStatus.RUNNING;
        }
        return new AsyncResult(asyncStatus);
    }

    private void handler() {
        LoadBalancerDao loadBalancerDao = SpringUtil.getBean(LoadBalancerDao.class);
        LoadBalancer loadBalancer = new LoadBalancer();
        loadBalancer.setId(loadBalancerId);
        List<String> updateField = new ArrayList<>();
        updateField.add(loadBalancer.updateStatus(ResourceStates.status(ResourceType.SLB, ProviderType.NEUTRON, lber.getProvisioningStatus().name())));

        loadBalancerDao.update(loadBalancer, updateField);

    }

    public void successHandler() {
        this.handler();
    }

    public void failHandler() {

        if (lber == null) {
            LoadBalancerDao loadBalancerDao = SpringUtil.getBean(LoadBalancerDao.class);
            PortService portService = SpringUtil.getBean(PortService.class);

            LoadBalancer lb = loadBalancerDao.getById(loadBalancerId);
            loadBalancerDao.deleteById(lb.getId());
            portService.delete(lb.getVipPortId());

        } else {
            this.handler();
        }
    }

    public String getLoadBalancerRefId() {
        return loadBalancerRefId;
    }

    public void setLoadBalancerRefId(String loadBalancerRefId) {
        this.loadBalancerRefId = loadBalancerRefId;
    }

    public String getLoadBalancerId() {
        return loadBalancerId;
    }

    public void setLoadBalancerId(String loadBalancerId) {
        this.loadBalancerId = loadBalancerId;
    }

    @Override
    public long timeout() {
        // TODO Auto-generated method stub

        return 0;
    }

}
