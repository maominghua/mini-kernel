package com.gcloud.controller.slb.model;

import java.util.ArrayList;
import java.util.List;

public class DescribeVServerGroupAttributeResponse {
	private String vServerGroupId;
	private String vServerGroupName;
	private List<BackendServer> backendServers = new ArrayList<BackendServer>();
	public String getvServerGroupId() {
		return vServerGroupId;
	}
	public void setvServerGroupId(String vServerGroupId) {
		this.vServerGroupId = vServerGroupId;
	}
	public String getvServerGroupName() {
		return vServerGroupName;
	}
	public void setvServerGroupName(String vServerGroupName) {
		this.vServerGroupName = vServerGroupName;
	}
	public List<BackendServer> getBackendServers() {
		return backendServers;
	}
	public void setBackendServers(List<BackendServer> backendServers) {
		this.backendServers = backendServers;
	}
}
