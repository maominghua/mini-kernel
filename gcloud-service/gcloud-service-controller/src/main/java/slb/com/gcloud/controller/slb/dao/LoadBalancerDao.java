package com.gcloud.controller.slb.dao;

import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.controller.utils.SqlUtil;
import com.gcloud.core.currentUser.policy.enums.UserResourceFilterPolicy;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.core.currentUser.policy.service.IUserResourceFilterPolicy;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.slb.model.LoadBalancerModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LoadBalancerDao extends JdbcBaseDaoImpl<LoadBalancer, String> {
	
	
	public  void updateLoadBalancerStatus(String loadBalancerId, String status) {
		LoadBalancer lber = this.getById(loadBalancerId);
		lber.setStatus(status);
		this.update(lber);
		
	}
	
	public PageResult<LoadBalancerModel> getByPage(int pageNum, int pageSize, List<String> loadBalancers, CurrentUser currentUser) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "t.");
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		
		sql.append("select t.*, ll.listener,rs.router_id vpc_id,ipa.ip_address address from gc_slb t ");
		sql.append(" left join (select l.loadbalancer_id, GROUP_CONCAT(CONCAT_WS('#', l.id, l.listener_port, l.listener_protocol)) listener from gc_slb_listener l group by l.loadbalancer_id) ll");
		sql.append(" on t.id = ll.loadbalancer_id");
		sql.append(" left join");
		sql.append(" (select ip.port_id, group_concat(ifnull(ip.ip_address, '')) ip_address from gc_ipallocations ip group by ip.port_id) ipa");
		sql.append(" on t.vip_port_id = ipa.port_id");

		sql.append(" left join");
		sql.append(" (select group_concat(rp.router_id) router_id, i.subnet_id from gc_router_ports rp ");
		sql.append(" left join gc_ports p on rp.port_id = p.id left join gc_ipallocations i on p.id = i.port_id group by i.subnet_id) rs");
		sql.append(" on t.vip_subnet_id = rs.subnet_id");
		sql.append(" where 1 = 1");
		
		if(loadBalancers != null && loadBalancers.size() > 0){      
		    String inPreSql = SqlUtil.inPreStr(loadBalancers.size());
		    sql.append(" and t.id in (").append(inPreSql).append(")");
		    values.addAll(loadBalancers);
		}
		
		sql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		
		sql.append(" order by t.create_time desc");
		
		return this.findBySql(sql.toString(), values, pageNum, pageSize, LoadBalancerModel.class);
	}
	
	public List<LoadBalancerModel> list(List<String> loadBalancers) {
		StringBuffer sql = new StringBuffer();
		List<Object> values = new ArrayList<>();

		sql.append("select t.*, ll.listener,rs.router_id vpc_id,ipa.ip_address address from gc_slb t ");
		sql.append(" left join (select l.loadbalancer_id, GROUP_CONCAT(CONCAT_WS('#', l.id, l.listener_port, l.listener_protocol)) listener from gc_slb_listener l group by l.loadbalancer_id) ll");
		sql.append(" on t.id = ll.loadbalancer_id");
		sql.append(" left join");
		sql.append(" (select ip.port_id, group_concat(ifnull(ip.ip_address, '')) ip_address from gc_ipallocations ip group by ip.port_id) ipa");
		sql.append(" on t.vip_port_id = ipa.port_id");

		sql.append(" left join");
		sql.append(" (select group_concat(rp.router_id) router_id, i.subnet_id from gc_router_ports rp ");
		sql.append(" left join gc_ports p on rp.port_id = p.id left join gc_ipallocations i on p.id = i.port_id group by i.subnet_id) rs");
		sql.append(" on t.vip_subnet_id = rs.subnet_id");
		sql.append(" where 1 = 1");
		
		if(loadBalancers != null && loadBalancers.size() > 0){      
		   String inPreSql = SqlUtil.inPreStr(loadBalancers.size());
		   sql.append(" and t.id in (").append(inPreSql).append(")");
		   values.addAll(loadBalancers);
		}

		sql.append(" order by t.create_time desc");
		
		return this.findBySql(sql.toString(), values, LoadBalancerModel.class);
	}
	
	public int getAllLbNum(CurrentUser currentUser) {
		IUserResourceFilterPolicy filterPolicy = (IUserResourceFilterPolicy)SpringUtil.getBean(UserResourceFilterPolicy.TYPICAL.getFilterPolicyClazz());
		FilterPolicyModel sqlModel = filterPolicy.filterPolicy(currentUser, "s.");
		StringBuffer countsql = new StringBuffer();
		List<Object> values = new ArrayList<>();
		countsql.append("select count(s.id) from gc_slb s  where 1=1 ");
		countsql.append(sqlModel.getWhereSql());
		values.addAll(sqlModel.getParams());
		return this.countBySql(countsql.toString(), values);
	}

}
