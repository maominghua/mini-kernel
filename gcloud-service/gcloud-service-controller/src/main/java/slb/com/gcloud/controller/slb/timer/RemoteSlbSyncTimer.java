package com.gcloud.controller.slb.timer;

import com.gcloud.controller.slb.dao.LoadBalancerDao;
import com.gcloud.controller.slb.entity.LoadBalancer;
import com.gcloud.controller.slb.provider.ILoadBalancerProvider;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.header.enums.ProviderType;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@QuartzTimer(fixedDelay = 1800 * 1000L)  // half hour
@Slf4j
public class RemoteSlbSyncTimer extends GcloudJobBean {

    private long lastUpdateTime = 0L;
    private static final String updateTimeKey = "SLBLastUpdateTime";

    @Autowired
    private LoadBalancerDao dao;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.debug("remote SLB data sync start ...");
        Object time = CacheContainer.getInstance().get(CacheType.TIMER, updateTimeKey);
        if (time == null) {
            log.warn("get SLB last update time from redis got null");
        } else {
            lastUpdateTime = (long)time;
        }
        for (ILoadBalancerProvider provider : SpringUtil.getBeans(ILoadBalancerProvider.class)) {
            log.debug("syncing with provider: " + provider.getClass().getName());
            if (provider.providerType() == ProviderType.GCLOUD) continue;

            this.syncSlb(provider);
        }

        lastUpdateTime = (new Date()).getTime();
        CacheContainer.getInstance().put(CacheType.TIMER, updateTimeKey, lastUpdateTime);
        log.info("RemoteLBDataSyncTimer end. update time: " + lastUpdateTime/1000);
    }

    private void syncSlb(ILoadBalancerProvider provider) {

        LoadBalancer loadBalancer = dao.findOneByProperty(LoadBalancer.PROVIDER, provider.providerType().getValue());
        if(loadBalancer == null){
            log.debug(String.format("%s loadBalancer 没有数据不进行同步", provider.providerType().name()));
            return;
        }

        List<LoadBalancer> lb = provider.list(null);
        List<String> fields = Arrays.asList(LoadBalancer.STATUS, LoadBalancer.UPDATED_AT);
        for (LoadBalancer l : lb) {
            if (l.getUpdatedAt().getTime() <= lastUpdateTime) continue;
            try {
                Map<String, Object> items = new HashMap<>();
                items.put(LoadBalancer.PROVIDER, l.getProvider());
                items.put(LoadBalancer.PROVIDER_REF_ID, l.getProviderRefId());
                List<LoadBalancer> entities = this.dao.findByProperties(items);

                if (entities == null || entities.size() == 0) {
                    log.warn("can not found load balancer with provider [" + l.getProvider() + "] and provider_ref_id ["
                            + l.getProviderRefId() + "], ignore update status.");
                    continue;
                }
                if (entities.size() > 1) {
                    log.warn("found more than one matched entity, ignore.");
                    continue;
                }

                LoadBalancer target = entities.get(0);
                target.setUpdatedAt(l.getUpdatedAt());
                target.setStatus(l.getStatus());
                this.dao.update(target, fields);
            }
            catch (Exception e) {
                log.error("got exception when syncing load balancer status: " + e.getMessage());
            }
        }
    }
}