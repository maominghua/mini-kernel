package com.gcloud.controller.log.prop;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


import lombok.Data;

@Component
@Data
@ConfigurationProperties(prefix = "gcloud.controller.log")
public class LogProp {
	private boolean enableFileLog=true;
	private int logKeptDays=7;
	private int fileLogKeptDays=30;
}
