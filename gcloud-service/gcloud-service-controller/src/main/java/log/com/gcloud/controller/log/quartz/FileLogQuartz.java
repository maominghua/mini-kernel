package com.gcloud.controller.log.quartz;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.controller.log.dao.LogDao;
import com.gcloud.controller.log.prop.LogProp;
import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;

import lombok.extern.slf4j.Slf4j;

/**日志记录归档任务
 * @author dengyf
 *
 */
@Component
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
@QuartzTimer(fixedRate=24* 60 * 60 * 1000L)
public class FileLogQuartz extends GcloudJobBean{
	@Autowired
    private LogDao logDao;
	
	@Autowired
	private LogProp prop;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		try {
			if(prop.isEnableFileLog()) {
				log.debug("日志记录归档定时任务开始");
				logDao.fileLog(prop.getLogKeptDays(), prop.getFileLogKeptDays());
				log.debug("日志记录归档定时任务结束");
			}
		} catch(Exception ex) {
			log.error("日志记录归档任务失败", ex);
		}
	}

}
