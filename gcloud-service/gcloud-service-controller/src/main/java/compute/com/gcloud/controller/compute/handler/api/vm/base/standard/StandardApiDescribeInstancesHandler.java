package com.gcloud.controller.compute.handler.api.vm.base.standard;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.handler.api.model.DescribeInstancesParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.utils.ApiUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.enums.standard.StandardInstanceStatus;
import com.gcloud.header.compute.msg.api.model.InstanceAttributesType;
import com.gcloud.header.compute.msg.api.model.standard.StandardInstanceAttributesType;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstancesMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstancesReplyMsg;


@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "DescribeInstances", versions = {ApiVersion.Standard})
public class StandardApiDescribeInstancesHandler extends MessageHandler<StandardApiDescribeInstancesMsg, StandardApiDescribeInstancesReplyMsg>{

	@Autowired
	private IVmBaseService vmBaseService;
	
	@Autowired
	private IVolumeService volumeService;
	
	@Autowired
	private IStoragePoolService storagePoolService;
	
	@Override
	public StandardApiDescribeInstancesReplyMsg handle(StandardApiDescribeInstancesMsg msg) throws GCloudException {
		DescribeInstancesParams params = toParams(msg);
		if(StringUtils.isNotBlank(msg.getAttachDiskId())) {
			Volume vol = volumeService.getVolume(msg.getAttachDiskId());
			if(vol != null) {
				StoragePool storagePool = storagePoolService.getPoolById(vol.getPoolId());
				if(storagePool!=null && StringUtils.isNotBlank(storagePool.getHostname())) {
					params.setHostname(storagePool.getHostname());
				}
			}
		}
		PageResult<InstanceAttributesType> response = vmBaseService.describeInstances(params, msg.getCurrentUser());
		PageResult<StandardInstanceAttributesType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
		
		StandardApiDescribeInstancesReplyMsg replyMsg = new StandardApiDescribeInstancesReplyMsg();
		replyMsg.init(stdResponse);
		return replyMsg;
	}
	
	public List<StandardInstanceAttributesType> toStandardReply(List<InstanceAttributesType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardInstanceAttributesType> stdList = new ArrayList<>();
		for (InstanceAttributesType data : list) {
			StandardInstanceAttributesType stdData = BeanUtil.copyBean(data, StandardInstanceAttributesType.class);
			stdList.add(stdData);
		}
		return stdList;
	}
	
	public DescribeInstancesParams toParams(StandardApiDescribeInstancesMsg msg) {
		DescribeInstancesParams params = BeanUtil.copyProperties(msg, DescribeInstancesParams.class);
		
		if(StringUtils.isNotBlank(msg.getStatus())) {
			StandardInstanceStatus stdStatus = StandardInstanceStatus.value(msg.getStatus());
			if(null == stdStatus) {
				throw new GCloudException("::不支持该状态的查询");
			}
			if(stdStatus.getGcStatus() == null || stdStatus.getGcStatus().size() == 1) {
				params.setStatues(null);
				params.setStatus(stdStatus.getValue());
			} else {
				params.setStatus(null);
				params.setStatues(stdStatus.getGcStatusValues());
			}
		}
		return params;
	}

}
