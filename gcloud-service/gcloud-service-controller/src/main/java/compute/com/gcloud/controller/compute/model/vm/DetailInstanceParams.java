package com.gcloud.controller.compute.model.vm;

public class DetailInstanceParams {
	private String instanceId;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
