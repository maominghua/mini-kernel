package com.gcloud.controller.compute.handler.api.vm.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.handler.api.model.DescribeInstancesParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.entity.Volume;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.InstanceAttributesType;
import com.gcloud.header.compute.msg.api.vm.base.ApiDescribeInstancesMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiDescribeInstancesReplyMsg;

@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "DescribeInstances")
public class ApiDescribeInstancesHandler extends MessageHandler<ApiDescribeInstancesMsg, ApiDescribeInstancesReplyMsg> {
	@Autowired
	private IVmBaseService vmBaseService;
	
	@Autowired
	private IVolumeService volumeService;
	
	@Autowired
	private IStoragePoolService storagePoolService;
	
	@Override
	public ApiDescribeInstancesReplyMsg handle(ApiDescribeInstancesMsg msg) throws GCloudException {
		DescribeInstancesParams params = BeanUtil.copyProperties(msg, DescribeInstancesParams.class);
		if(StringUtils.isNotBlank(msg.getAttachDiskId())) {
			Volume vol = volumeService.getVolume(msg.getAttachDiskId());
			if(vol != null) {
				StoragePool storagePool = storagePoolService.getPoolById(vol.getPoolId());
				if(storagePool!=null && StringUtils.isNotBlank(storagePool.getHostname())) {
					params.setHostname(storagePool.getHostname());
				}
			}
		}
		PageResult<InstanceAttributesType> response = vmBaseService.describeInstances(params, msg.getCurrentUser());
		ApiDescribeInstancesReplyMsg replyMsg = new ApiDescribeInstancesReplyMsg();
		replyMsg.init(response);
		return replyMsg;
	}

}
