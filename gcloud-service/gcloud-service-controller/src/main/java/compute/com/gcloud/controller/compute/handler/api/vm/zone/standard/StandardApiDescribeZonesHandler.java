package com.gcloud.controller.compute.handler.api.vm.zone.standard;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.controller.utils.ApiUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
import com.gcloud.header.compute.msg.api.model.standard.StandardZoneType;
import com.gcloud.header.compute.msg.api.vm.zone.standard.StandardApiDescribeZonesMsg;
import com.gcloud.header.compute.msg.api.vm.zone.standard.StandardApiDescribeZonesReplyMsg;


@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "DescribeZones", versions = {ApiVersion.Standard})
public class StandardApiDescribeZonesHandler extends MessageHandler<StandardApiDescribeZonesMsg, StandardApiDescribeZonesReplyMsg>{

	@Autowired
    private IVmZoneService zoneService;
	
	@Override
	public StandardApiDescribeZonesReplyMsg handle(StandardApiDescribeZonesMsg msg) throws GCloudException {
		StandardApiDescribeZonesReplyMsg reply = new StandardApiDescribeZonesReplyMsg();
		PageResult<AvailableZone> response = zoneService.describeZones(msg.getPageNumber(), msg.getPageSize());
		PageResult<StandardZoneType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
		
		reply.init(stdResponse);
		return reply;
	}
	
	public List<StandardZoneType> toStandardReply(List<AvailableZone> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardZoneType> stdList = new ArrayList<>();
		for (AvailableZone data : list) {
			StandardZoneType stdData = BeanUtil.copyProperties(data, StandardZoneType.class);
			stdList.add(stdData);
		}
		return stdList;
	}

}
