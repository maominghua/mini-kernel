package com.gcloud.controller.compute.handler.api.vm.tpl;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.tpl.ApiTplInstancesMsg;
import com.gcloud.header.compute.msg.api.vm.tpl.ApiTplInstancesReplyMsg;

@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "TplInstances")
@GcLog(taskExpect = "拓扑图-云服务器列表")
public class ApiTplInstancesHandler extends MessageHandler<ApiTplInstancesMsg, ApiTplInstancesReplyMsg>{
	@Autowired
	private IVmBaseService vmBaseService;

	@Override
	public ApiTplInstancesReplyMsg handle(ApiTplInstancesMsg msg) throws GCloudException {
		ApiTplInstancesReplyMsg reply = new ApiTplInstancesReplyMsg();
		reply.setInstances(vmBaseService.tplInstances(msg));
		return reply;
	}
}
