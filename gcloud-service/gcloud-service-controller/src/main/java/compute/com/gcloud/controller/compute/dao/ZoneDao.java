
package com.gcloud.controller.compute.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.poifs.filesystem.FilteringDirectoryNode;
import org.springframework.stereotype.Repository;

import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;

@Jdbc("controllerJdbcTemplate")
@Repository
public class ZoneDao extends JdbcBaseDaoImpl<AvailableZoneEntity, String> {

    public PageResult<AvailableZoneEntity> page(Integer pageNumber, Integer pageSize){

        String sql = "select * from gc_zones";

        return findBySql(sql, pageNumber, pageSize);

    }
    
    public List<AvailableZoneEntity> findByIds(List<String> ids) {
    	if(null == ids || ids.isEmpty()) {
    		return null;
    	}
    	
    	StringBuffer sql = new StringBuffer();
    	List<Object> values = new ArrayList<>();
    	
    	sql.append("select * from gc_zones where id in (").append(SqlUtil.inPreStr(ids.size())).append(")");
    	values.addAll(ids);
    	
    	return findBySql(sql.toString(), values);
    }

}
