package com.gcloud.controller.compute.handler.api.vm.network.standard;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.network.DetachPortInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.network.DetachPortWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.network.DetachPortWorkflow;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.workflow.core.handler.BaseWorkFlowHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.network.standard.StandardApiDetachNetworkInterfaceMsg;
import com.gcloud.header.compute.msg.api.network.standard.StandardApiDetachNetworkInterfaceReplyMsg;

@LongTask
@GcLog(isMultiLog = true, taskExpect = "卸载网卡")
@ApiHandler(module= Module.ECS, subModule = SubModule.NETWORKINTERFACE, action="DetachNetworkInterface", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.PORT, resourceIdField = "networkInterfaceId")
public class StandardApiDetachNetworkInterfaceHandler extends BaseWorkFlowHandler<StandardApiDetachNetworkInterfaceMsg, StandardApiDetachNetworkInterfaceReplyMsg>{

	@Override
	public Object preProcess(StandardApiDetachNetworkInterfaceMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public StandardApiDetachNetworkInterfaceReplyMsg process(StandardApiDetachNetworkInterfaceMsg msg)
			throws GCloudException {
		StandardApiDetachNetworkInterfaceReplyMsg replyMessage = new StandardApiDetachNetworkInterfaceReplyMsg();
        DetachPortInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DetachPortInitFlowCommandRes.class);
        replyMessage.setTaskId(res.getTaskId());
        return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return DetachPortWorkflow.class;
	}
	
	@Override
    public Object initParams(StandardApiDetachNetworkInterfaceMsg msg) {
        DetachPortWorkflowReq req = new DetachPortWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setNetworkInterfaceId(msg.getNetworkInterfaceId());
        req.setInTask(false);
        return req;
    }

}
