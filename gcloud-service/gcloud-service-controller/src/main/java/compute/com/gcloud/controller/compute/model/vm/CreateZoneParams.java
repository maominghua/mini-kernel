package com.gcloud.controller.compute.model.vm;

public class CreateZoneParams {
    private String zoneName;
    private boolean enabled = true;
    
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
