package com.gcloud.controller.compute.handler.node.vm.senior;

import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.header.compute.msg.node.vm.senior.InstanceFsFreezeReplyMsg;

import lombok.extern.slf4j.Slf4j;
@Handler
@Slf4j
public class InstanceFsFreezeReplyHandler extends AsyncMessageHandler<InstanceFsFreezeReplyMsg>{

	@Override
	public void handle(InstanceFsFreezeReplyMsg msg) {
		log.debug("InstanceFsFreezeReplyHandler taskId:" + msg.getTaskId() + ",success:" + msg.getSuccess());
	}

}
