package com.gcloud.controller.compute.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.compute.entity.ZoneInstanceTypeEntity;
import com.gcloud.core.util.SqlUtil;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
import com.gcloud.framework.db.jdbc.annotation.Jdbc;

@Jdbc("controllerJdbcTemplate")
@Repository
public class ZoneInstanceTypeDao extends JdbcBaseDaoImpl<ZoneInstanceTypeEntity, String>{
	public void deleteByInstanceTypeIdAndZoneId(String instanceId, List<String> zoneIds) {
		StringBuffer sql = new StringBuffer();
		List<String> values = new ArrayList<>();
		
		sql.append("delete from gc_zone_instance_types where instance_type_id = ? and zone_id in (")
			.append(SqlUtil.inPreStr(zoneIds.size())).append(")");
		values.add(instanceId);
		values.addAll(zoneIds);
		Object[] arrayValues = values.toArray();
		
		this.jdbcTemplate.update(sql.toString(), arrayValues);
	}
}
