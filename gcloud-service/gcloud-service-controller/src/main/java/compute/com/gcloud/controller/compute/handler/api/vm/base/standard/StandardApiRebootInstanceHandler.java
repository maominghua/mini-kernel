package com.gcloud.controller.compute.handler.api.vm.base.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiRebootInstanceMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiRebootInstanceReplyMsg;

@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "RebootInstance", versions = {ApiVersion.Standard})
@LongTask
@GcLog(taskExpect = "重启云服务器")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class StandardApiRebootInstanceHandler extends MessageHandler<StandardApiRebootInstanceMsg, StandardApiRebootInstanceReplyMsg>{

	@Autowired
	private IVmBaseService vmBaseService;
	
	@Override
	public StandardApiRebootInstanceReplyMsg handle(StandardApiRebootInstanceMsg msg) throws GCloudException {
		vmBaseService.rebootInstance(msg.getInstanceId(), msg.getForceStop(), false);
		
        msg.setInstanceId(msg.getInstanceId());
        msg.setObjectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId()));

        StandardApiRebootInstanceReplyMsg reply = new StandardApiRebootInstanceReplyMsg();
        return reply;
	}
	
}
