package com.gcloud.controller.compute.handler.api.vm.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.model.vm.DetailInstanceParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.DetailInstance;
import com.gcloud.header.compute.msg.api.vm.base.ApiDetailInstanceMsg;
import com.gcloud.header.compute.msg.api.vm.base.ApiDetailInstanceReplyMsg;

@ApiHandler(module = Module.ECS,subModule=SubModule.VM,action = "DetailInstance")
public class ApiDetailInstanceHandler extends MessageHandler<ApiDetailInstanceMsg, ApiDetailInstanceReplyMsg>{

	@Autowired
	private IVmBaseService vmBaseService;
	
	@Override
	public ApiDetailInstanceReplyMsg handle(ApiDetailInstanceMsg msg) throws GCloudException {
		DetailInstanceParams params = BeanUtil.copyProperties(msg, DetailInstanceParams.class);
		DetailInstance response = vmBaseService.detailInstance(params, msg.getCurrentUser());
		ApiDetailInstanceReplyMsg reply = new ApiDetailInstanceReplyMsg();
		reply.setDetailInstance(response);
		return reply;
	}

}
