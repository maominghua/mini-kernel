package com.gcloud.controller.compute.handler.api.vm.trash.standard;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.workflow.model.trash.DeleteInstanceInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.trash.DeleteInstanceWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.trash.DeleteInstanceWorkflow;
import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.workflow.core.handler.BaseWorkFlowHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.trash.standard.StandardApiDeleteInstanceMsg;
import com.gcloud.header.compute.msg.api.trash.standard.StandardApiDeleteInstanceReplyMsg;
import com.gcloud.header.log.model.Task;

@LongTask
@GcLog(isMultiLog = true, taskExpect = "删除实例")
@ApiHandler(module= Module.ECS, subModule = SubModule.VM, action="DeleteInstance", versions = {ApiVersion.Standard})
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
public class StandardApiDeleteInstanceHandler extends BaseWorkFlowHandler<StandardApiDeleteInstanceMsg, StandardApiDeleteInstanceReplyMsg>{

	@Override
	public Object preProcess(StandardApiDeleteInstanceMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public StandardApiDeleteInstanceReplyMsg process(StandardApiDeleteInstanceMsg msg) throws GCloudException {
		StandardApiDeleteInstanceReplyMsg replyMessage = new StandardApiDeleteInstanceReplyMsg();
		DeleteInstanceInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), DeleteInstanceInitFlowCommandRes.class);
		msg.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId()).expect("删除实例").build());
		
		return replyMessage;
	}

	@Override
	public Class getWorkflowClass() {
		return DeleteInstanceWorkflow.class;
	}
	
	@Override
    public Object initParams(StandardApiDeleteInstanceMsg msg) {
        DeleteInstanceWorkflowReq req = new DeleteInstanceWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setInTask(false);
        return req;
    }
}
