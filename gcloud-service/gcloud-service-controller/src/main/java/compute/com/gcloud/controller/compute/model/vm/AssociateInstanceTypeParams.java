package com.gcloud.controller.compute.model.vm;

import java.util.List;

public class AssociateInstanceTypeParams {
	private String instanceTypeId;
//	private String zoneId;
	private List<String> zoneIds;
	
	public String getInstanceTypeId() {
		return instanceTypeId;
	}
	public void setInstanceTypeId(String instanceTypeId) {
		this.instanceTypeId = instanceTypeId;
	}
	public List<String> getZoneIds() {
		return zoneIds;
	}
	public void setZoneIds(List<String> zoneIds) {
		this.zoneIds = zoneIds;
	}
}
