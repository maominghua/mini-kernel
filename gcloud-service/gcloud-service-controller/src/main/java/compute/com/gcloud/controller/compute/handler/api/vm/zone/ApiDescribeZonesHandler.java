
package com.gcloud.controller.compute.handler.api.vm.zone;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.handler.api.model.DescribeZonesParams;
import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
import com.gcloud.header.compute.msg.api.vm.zone.ApiDescribeZonesMsg;
import com.gcloud.header.compute.msg.api.vm.zone.ApiDescribeZonesReplyMsg;

@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "DescribeZones")
public class ApiDescribeZonesHandler extends MessageHandler<ApiDescribeZonesMsg, ApiDescribeZonesReplyMsg> {

    @Autowired
    private IVmZoneService zoneService;

    @Override
    public ApiDescribeZonesReplyMsg handle(ApiDescribeZonesMsg msg) throws GCloudException {
        DescribeZonesParams params = BeanUtil.copyProperties(msg, DescribeZonesParams.class);
        PageResult<AvailableZone> page = this.zoneService.describeZones(params.getPageNumber(), params.getPageSize());
        ApiDescribeZonesReplyMsg replyMsg = new ApiDescribeZonesReplyMsg();
        replyMsg.init(page);
        return replyMsg;
    }

}
