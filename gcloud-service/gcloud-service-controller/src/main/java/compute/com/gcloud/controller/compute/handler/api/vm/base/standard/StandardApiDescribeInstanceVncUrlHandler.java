package com.gcloud.controller.compute.handler.api.vm.base.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstanceVncUrlMsg;
import com.gcloud.header.compute.msg.api.vm.base.standard.StandardApiDescribeInstanceVncUrlReplyMsg;

@ApiHandler(module = Module.ECS, subModule= SubModule.VM, action = "DescribeInstanceVncUrl", versions = {ApiVersion.Standard})
public class StandardApiDescribeInstanceVncUrlHandler extends MessageHandler<StandardApiDescribeInstanceVncUrlMsg, StandardApiDescribeInstanceVncUrlReplyMsg>{

	@Autowired
    private IVmBaseService vmBaseService;
	
	@Override
	public StandardApiDescribeInstanceVncUrlReplyMsg handle(StandardApiDescribeInstanceVncUrlMsg msg)
			throws GCloudException {
		String url = vmBaseService.queryInstanceVNC(msg.getInstanceId());
		StandardApiDescribeInstanceVncUrlReplyMsg replyMsg = new StandardApiDescribeInstanceVncUrlReplyMsg();

        replyMsg.setVncUrl(url);
        replyMsg.setSuccess(true);
        return replyMsg;
	}

}
