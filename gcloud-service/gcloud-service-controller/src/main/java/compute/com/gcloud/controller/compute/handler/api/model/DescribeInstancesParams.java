package com.gcloud.controller.compute.handler.api.model;

import java.util.List;

import com.gcloud.common.model.PageParams;

public class DescribeInstancesParams extends PageParams{
	private String instanceName;
	private String status;
	private List<String> statues;
	private String zoneId;
	private List<String> instanceIds;
	private String hostname;
	
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String> getStatues() {
		return statues;
	}
	public void setStatues(List<String> statues) {
		this.statues = statues;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public List<String> getInstanceIds() {
		return instanceIds;
	}
	public void setInstanceIds(List<String> instanceIds) {
		this.instanceIds = instanceIds;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
}
