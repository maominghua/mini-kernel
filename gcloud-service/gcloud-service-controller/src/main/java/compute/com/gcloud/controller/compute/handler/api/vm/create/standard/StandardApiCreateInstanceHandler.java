package com.gcloud.controller.compute.handler.api.vm.create.standard;

import java.util.UUID;

import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceFlowInitCommandRes;
import com.gcloud.controller.compute.workflow.model.vm.CreateInstanceWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.create.CreateInstanceWorkflow;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.core.workflow.core.handler.BaseWorkFlowHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.compute.msg.api.vm.create.standard.StandardApiCreateInstanceMsg;
import com.gcloud.header.compute.msg.api.vm.create.standard.StandardApiCreateInstanceReplyMsg;
import com.gcloud.header.log.model.Task;

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@LongTask
@GcLog(isMultiLog = true, taskExpect = "创建云服务器")
@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "CreateInstance", versions = {ApiVersion.Standard})
public class StandardApiCreateInstanceHandler extends BaseWorkFlowHandler<StandardApiCreateInstanceMsg, StandardApiCreateInstanceReplyMsg> {
	@Override
	public StandardApiCreateInstanceReplyMsg process(StandardApiCreateInstanceMsg msg) throws GCloudException {
		StandardApiCreateInstanceReplyMsg reply = new StandardApiCreateInstanceReplyMsg();
		// 记录操作日志，任务流无论单操作还是批量 都用这种方式记录操作日志
		CreateInstanceFlowInitCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), CreateInstanceFlowInitCommandRes.class);
		String except = String.format("创建云服务器[%s]成功", res.getInstanceId());
		reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(res.getInstanceId()).objectName(res.getInstanceName()).expect(except).errorCode(res.getErrorMsg()).build());
		reply.setInstanceId(res.getInstanceId());

		String requestId = UUID.randomUUID().toString();
		reply.setRequestId(requestId);
		reply.setSuccess(true);
		return reply;
	}
	
	@Override
	public Class getWorkflowClass() {
		return CreateInstanceWorkflow.class;
	}
	
	@Override
	public Object preProcess(StandardApiCreateInstanceMsg msg) throws GCloudException {
		return null;
	}
	
	@Override
    public Object initParams(StandardApiCreateInstanceMsg msg) {
        CreateInstanceWorkflowReq req = BeanUtil.copyProperties(msg, CreateInstanceWorkflowReq.class);
        req.setSubnetId(msg.getvSwitchId());
        req.setIpAddress(msg.getPrivateIpAddress());
        req.setInTask(false);
        return req;
    }
}
