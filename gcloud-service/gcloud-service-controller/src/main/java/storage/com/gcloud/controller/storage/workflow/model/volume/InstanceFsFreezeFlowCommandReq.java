package com.gcloud.controller.storage.workflow.model.volume;

public class InstanceFsFreezeFlowCommandReq {
	private OnLineSnapshotWorkflowPreRes resParam;

	public OnLineSnapshotWorkflowPreRes getResParam() {
		return resParam;
	}

	public void setResParam(OnLineSnapshotWorkflowPreRes resParam) {
		this.resParam = resParam;
	}
	
}
