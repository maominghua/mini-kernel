package com.gcloud.controller.storage.model;

public class DetailDiskParams {
	private String diskId;

	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}
}
