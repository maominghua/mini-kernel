package com.gcloud.controller.storage.workflow.model.volume;

import com.gcloud.header.api.model.CurrentUser;

public class OnLineSnapshotWorkflowReq {
	private String diskId;
	private String name;
	private String description;
	private CurrentUser currentUser;
	
	public String getDiskId() {
		return diskId;
	}
	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public CurrentUser getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}
}
