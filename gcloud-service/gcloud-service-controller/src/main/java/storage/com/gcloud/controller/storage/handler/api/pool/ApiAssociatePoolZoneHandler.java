package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.AssociatePoolZoneParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiAssociatePoolZoneMsg;
import com.gcloud.header.storage.msg.api.pool.ApiAssociatePoolZoneReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;

@GcLog(taskExpect="存储池关联可用区")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.ASSOCIATE_POOL_ZONE)
public class ApiAssociatePoolZoneHandler extends MessageHandler<ApiAssociatePoolZoneMsg, ApiAssociatePoolZoneReplyMsg>{

	@Autowired
	private IStoragePoolService storagePoolService;
	
	@Override
	public ApiAssociatePoolZoneReplyMsg handle(ApiAssociatePoolZoneMsg msg) throws GCloudException {
		AssociatePoolZoneParams params = BeanUtil.copyProperties(msg, AssociatePoolZoneParams.class);
		storagePoolService.associatePoolZone(params, msg.getCurrentUser());
		
		ApiAssociatePoolZoneReplyMsg reply = new ApiAssociatePoolZoneReplyMsg();
		return reply;
	}

}
