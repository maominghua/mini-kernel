package com.gcloud.controller.storage.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.controller.storage.workflow.model.volume.CreateSnapshotFlowCommandReq;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;

@Component
@Scope("prototype")
@Slf4j
public class CreateSnapshotFlowCommand extends BaseWorkFlowCommand{
    
    @Autowired
    private ISnapshotService snapshotService;
    
	@Override
	protected Object process() throws Exception {
		CreateSnapshotFlowCommandReq req = (CreateSnapshotFlowCommandReq)getReqParams();
		snapshotService.createSnapshot(req.getDiskId(), req.getName(), req.getDescription(), req.getCurrentUser(), getTaskId(), req.getSnapshotId());
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		CreateSnapshotFlowCommandReq req = (CreateSnapshotFlowCommandReq)getReqParams();
		snapshotService.deleteSnapshot(req.getSnapshotId(), getTaskId());
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return CreateSnapshotFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}
