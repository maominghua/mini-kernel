package com.gcloud.controller.storage.model;

import com.gcloud.common.model.PageParams;

import java.util.List;

/**
 * Created by yaowj on 2018/9/28.
 */
public class DescribeDisksParams extends PageParams {

    private String diskType;
    private String status;
    private List<String> statuses;
    private String diskName;
    private String instanceId;
    private List<String> diskIds;
    private String hostname;
    private boolean emptyList;

    public String getDiskType() {
        return diskType;
    }

    public void setDiskType(String diskType) {
        this.diskType = diskType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiskName() {
        return diskName;
    }

    public void setDiskName(String diskName) {
        this.diskName = diskName;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

	public List<String> getDiskIds() {
		return diskIds;
	}

	public void setDiskIds(List<String> diskIds) {
		this.diskIds = diskIds;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public boolean isEmptyList() {
        return emptyList;
    }

    public void setEmptyList(boolean emptyList) {
        this.emptyList = emptyList;
    }
}
