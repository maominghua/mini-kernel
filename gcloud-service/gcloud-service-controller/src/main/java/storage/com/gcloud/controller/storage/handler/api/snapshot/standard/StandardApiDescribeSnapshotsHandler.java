package com.gcloud.controller.storage.handler.api.snapshot.standard;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.DescribeSnapshotsParams;
import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.controller.utils.ApiUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.SnapshotType;
import com.gcloud.header.storage.model.standard.StandardSnapshotType;
import com.gcloud.header.storage.msg.api.snapshot.standard.StandardApiDescribeSnapshotsMsg;
import com.gcloud.header.storage.msg.api.snapshot.standard.StandardApiDescribeSnapshotsReplyMsg;

@ApiHandler(module= Module.ECS,subModule=SubModule.SNAPSHOT, action="DescribeSnapshots", versions = {ApiVersion.Standard})
public class StandardApiDescribeSnapshotsHandler extends MessageHandler<StandardApiDescribeSnapshotsMsg, StandardApiDescribeSnapshotsReplyMsg>{

	@Autowired
    private ISnapshotService snapshotService;
	
	@Override
	public StandardApiDescribeSnapshotsReplyMsg handle(StandardApiDescribeSnapshotsMsg msg) throws GCloudException {
		DescribeSnapshotsParams params = BeanUtil.copyProperties(msg, DescribeSnapshotsParams.class);
        PageResult<SnapshotType> response = snapshotService.describeSnapshots(params, msg.getCurrentUser());
        PageResult<StandardSnapshotType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
        
        StandardApiDescribeSnapshotsReplyMsg reply = new StandardApiDescribeSnapshotsReplyMsg();
        reply.init(stdResponse);
        return reply;
	}
	
	public List<StandardSnapshotType> toStandardReply(List<SnapshotType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardSnapshotType> stdList = new ArrayList<>();
		for (SnapshotType data : list) {
			StandardSnapshotType stdData = BeanUtil.copyBean(data, StandardSnapshotType.class);
			stdList.add(stdData);
		}
		return stdList;
	}

}
