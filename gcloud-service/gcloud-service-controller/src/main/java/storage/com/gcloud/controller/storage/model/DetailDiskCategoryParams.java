package com.gcloud.controller.storage.model;

public class DetailDiskCategoryParams {
	private String diskCategoryId;

	public String getDiskCategoryId() {
		return diskCategoryId;
	}

	public void setDiskCategoryId(String diskCategoryId) {
		this.diskCategoryId = diskCategoryId;
	}
}
