package com.gcloud.controller.storage.workflow.model.volume;

public class OnLineSnapshotWorkflowPreRes {
	private boolean needFsFreeze;
	private String instanceId;
	public boolean isNeedFsFreeze() {
		return needFsFreeze;
	}
	public void setNeedFsFreeze(boolean needFsFreeze) {
		this.needFsFreeze = needFsFreeze;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
}
