package com.gcloud.controller.storage.workflow.model.volume;

import com.gcloud.core.workflow.model.WorkflowFirstStepResException;

public class OnLineSnapshotInitFlowCommandRes  extends WorkflowFirstStepResException{
	private String snapshotId;
	private String taskId;

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
}
