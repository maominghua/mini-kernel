package com.gcloud.controller.storage.workflow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.image.dao.ImagePropertyDao;
import com.gcloud.controller.image.entity.ImageProperty;
import com.gcloud.controller.image.entity.enums.ImagePropertyItem;
import com.gcloud.controller.storage.dao.VolumeAttachmentDao;
import com.gcloud.controller.storage.entity.VolumeAttachment;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotWorkflowPreRes;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotWorkflowReq;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import com.gcloud.header.compute.enums.PlatformType;
import com.gcloud.header.compute.enums.VmState;

import lombok.extern.slf4j.Slf4j;
@Component
@Scope("prototype")
@Slf4j
public class OnLineSnapshotWorkflow extends BaseWorkFlows{
	@Autowired
    private VolumeAttachmentDao volumeAttachmentDao;
    
    @Autowired
    private InstanceDao instanceDao;
    
    @Autowired
    private ImagePropertyDao imagePropertyDao;
	
	@Override
	public String getFlowTypeCode() {
		return "OnLineSnapshotWorkflow";
	}

	@Override
	public Object preProcess() {
		OnLineSnapshotWorkflowReq req = (OnLineSnapshotWorkflowReq)getReqParams();
		OnLineSnapshotWorkflowPreRes res = new OnLineSnapshotWorkflowPreRes();
		List<VolumeAttachment> attachs = volumeAttachmentDao.findByProperty("volumeId", req.getDiskId());
		boolean needFsFreeze = false;
        if(attachs.size() > 0) {
        	VmInstance vm = instanceDao.getById(attachs.get(0).getInstanceUuid());
        	res.setInstanceId(attachs.get(0).getInstanceUuid());
        	Map<String, Object> props = new HashMap<String, Object>();
        	props.put("imageId", vm.getImageId());
        	props.put("name", ImagePropertyItem.OS_TYPE.value());
        	ImageProperty imgProp = imagePropertyDao.findUniqueByProperties(props);
        	if(!vm.getState().equals(VmState.STOPPED.value())) {
    			throw new GCloudException("::需要在关机或者卸载的状态下才能进行快照操作");
    		}
        	/*if(imgProp.getValue().equals(PlatformType.WINDOWS.getValue())) {
        		//windows 需要关机状态
        		if(!vm.getState().equals(VmState.STOPPED.value())) {
        			throw new GCloudException("::windows虚拟机需要在关机的状态下才能进行快照操作");
        		}
        	} else {//linux
        		if(vm.getState().equals(VmState.PENDING.value()) || vm.getState().equals(VmState.RUNNING.value())) {
        			needFsFreeze = true;
        		}
        	}*/
        }
        
        res.setNeedFsFreeze(needFsFreeze);
        
		return res;
	}

	@Override
	public void process() {
		
	}

	@Override
	protected Class<?> getReqParamClass() {
		return OnLineSnapshotWorkflowReq.class;
	}

}
