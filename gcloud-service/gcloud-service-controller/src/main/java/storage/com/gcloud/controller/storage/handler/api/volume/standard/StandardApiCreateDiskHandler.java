package com.gcloud.controller.storage.handler.api.volume.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.CreateDiskParams;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiCreateDiskMsg;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiCreateDiskReplyMsg;

@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@LongTask
@GcLog(taskExpect = "创建磁盘")
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="CreateDisk", versions = {ApiVersion.Standard})
public class StandardApiCreateDiskHandler extends MessageHandler<StandardApiCreateDiskMsg, StandardApiCreateDiskReplyMsg>{

	@Autowired
    private IVolumeService volumeService;
	
	@Override
	public StandardApiCreateDiskReplyMsg handle(StandardApiCreateDiskMsg msg) throws GCloudException {
		CreateDiskParams params = BeanUtil.copyProperties(msg, CreateDiskParams.class);
        params.setTaskId(msg.getTaskId());
        String volumeId = volumeService.create(params, msg.getCurrentUser());
        msg.setObjectId(volumeId);
        StandardApiCreateDiskReplyMsg reply = new StandardApiCreateDiskReplyMsg();
        reply.setDiskId(volumeId);
        return reply;
	}

}
