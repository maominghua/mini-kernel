package com.gcloud.controller.storage.handler.api.snapshot;

import com.gcloud.controller.storage.workflow.OnLineSnapshotWorkflow;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotInitFlowCommandRes;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotWorkflowReq;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.workflow.core.handler.BaseWorkFlowHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.log.model.Task;
import com.gcloud.header.storage.msg.api.snapshot.ApiCreateSnapshotMsg;
import com.gcloud.header.storage.msg.api.snapshot.ApiCreateSnapshotReplyMsg;

import java.util.UUID;

import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;

/**
 * Created by yaowj on 2018/11/7.
 */
@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@LongTask
@GcLog(isMultiLog=true, taskExpect = "创建快照")
@ApiHandler(module= Module.ECS,subModule=SubModule.SNAPSHOT, action="CreateSnapshot")
public class ApiCreateSnapshotHandler  extends BaseWorkFlowHandler<ApiCreateSnapshotMsg, ApiCreateSnapshotReplyMsg> {

	@Override
	public Object preProcess(ApiCreateSnapshotMsg msg) throws GCloudException {
		return null;
	}

	@Override
	public ApiCreateSnapshotReplyMsg process(ApiCreateSnapshotMsg msg) throws GCloudException {
		ApiCreateSnapshotReplyMsg reply = new ApiCreateSnapshotReplyMsg();
        // 记录操作日志，任务流无论单操作还是批量 都用这种方式记录操作日志
		OnLineSnapshotInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), OnLineSnapshotInitFlowCommandRes.class);
        String except = String.format("磁盘[%s]快照成功", msg.getDiskId());
        reply.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getDiskId())
        		.objectName(CacheContainer.getInstance().getString(CacheType.VOLUME_NAME, msg.getDiskId())).expect(except).build());
        reply.setSnapshotId(res.getSnapshotId());
        String requestId = UUID.randomUUID().toString();
        reply.setRequestId(requestId);
        reply.setSuccess(true);
        return reply;
	}

	@Override
	public Class getWorkflowClass() {
		return OnLineSnapshotWorkflow.class;
	}
	
	@Override
	public Object initParams(ApiCreateSnapshotMsg msg) {
		OnLineSnapshotWorkflowReq params = new OnLineSnapshotWorkflowReq();
		params.setCurrentUser(msg.getCurrentUser());
		params.setDescription(msg.getDescription());
		params.setDiskId(msg.getDiskId());
		params.setName(msg.getSnapshotName());
		return params;
	}

    /*@Override
    public ApiCreateSnapshotReplyMsg handle(ApiCreateSnapshotMsg msg) throws GCloudException {
        String id = snapshotService.createSnapshot(msg.getDiskId(), msg.getSnapshotName(), msg.getDescription(), msg.getCurrentUser(), msg.getTaskId());
        ApiCreateSnapshotReplyMsg reply = new ApiCreateSnapshotReplyMsg();
        reply.setSnapshotId(id);
        return reply;
    }*/
}
