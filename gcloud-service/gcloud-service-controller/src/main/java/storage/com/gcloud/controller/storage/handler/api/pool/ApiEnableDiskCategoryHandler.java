package com.gcloud.controller.storage.handler.api.pool;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.EnableDiskCategoryParams;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.pool.ApiEnableDiskCategoryMsg;
import com.gcloud.header.storage.msg.api.pool.ApiEnableDiskCategoryReplyMsg;
import com.gcloud.header.storage.msg.api.pool.StoragePoolActions;

@GcLog(taskExpect="启用禁用磁盘类型")
@ApiHandler(module = Module.ECS, subModule = SubModule.DISK, action = StoragePoolActions.ENABLE_DISK_CATEGORY)
public class ApiEnableDiskCategoryHandler extends MessageHandler<ApiEnableDiskCategoryMsg, ApiEnableDiskCategoryReplyMsg>{

	@Autowired
    private IStoragePoolService poolService;
	
	@Override
	public ApiEnableDiskCategoryReplyMsg handle(ApiEnableDiskCategoryMsg msg) throws GCloudException {
		EnableDiskCategoryParams params = BeanUtil.copyProperties(msg, EnableDiskCategoryParams.class);
		poolService.enableDiskCategory(params, msg.getCurrentUser());
		
		ApiEnableDiskCategoryReplyMsg reply = new ApiEnableDiskCategoryReplyMsg();
		return reply;
	}

}
