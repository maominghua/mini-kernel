package com.gcloud.controller.storage.model;

public class AssociatePoolZoneParams{
	private String poolId;
	private String zoneId;
	
	public String getPoolId() {
		return poolId;
	}
	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
}
