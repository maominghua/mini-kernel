package com.gcloud.controller.storage.handler.api.volume.standard;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.storage.model.DescribeDisksParams;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.controller.utils.ApiUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.enums.standard.StandardDiskStatus;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.model.standard.StandardDiskItemType;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiDescribeDisksMsg;
import com.gcloud.header.storage.msg.api.volume.standard.StandardApiDescribeDisksReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaowj on 2018/9/29.
 */
@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="DescribeDisks", versions = {ApiVersion.Standard})
public class StandardApiDescribeDisksHandler extends MessageHandler<StandardApiDescribeDisksMsg, StandardApiDescribeDisksReplyMsg> {

    @Autowired
    private IVolumeService volumeService;
    
    @Autowired
    private IVmBaseService vmBaseService;

    @Override
    public StandardApiDescribeDisksReplyMsg handle(StandardApiDescribeDisksMsg msg) throws GCloudException {

        DescribeDisksParams params = toParams(msg);
        if(StringUtils.isNotBlank(msg.getAttachInstanceId())) {
        	params.setHostname(vmBaseService.getInstanceById(msg.getAttachInstanceId()).getHostname());
        }
        PageResult<DiskItemType> response = volumeService.describeDisks(params, msg.getCurrentUser());
        PageResult<StandardDiskItemType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
        StandardApiDescribeDisksReplyMsg replyMsg = new StandardApiDescribeDisksReplyMsg();
        replyMsg.init(stdResponse);
        return replyMsg;
    }

    public List<StandardDiskItemType> toStandardReply(List<DiskItemType> datas){
        if(datas == null){
            return null;
        }

        List<StandardDiskItemType> standardDatas = new ArrayList<>();
        for(DiskItemType data : datas){
            StandardDiskItemType standardData = BeanUtil.copyProperties(data, StandardDiskItemType.class);
            //转换状态
            standardData.setStatus(StandardDiskStatus.standardStatus(data.getStatus()));

            standardDatas.add(standardData);
        }

        return standardDatas;

    }

    public DescribeDisksParams toParams(StandardApiDescribeDisksMsg msg){
        DescribeDisksParams params = BeanUtil.copyProperties(msg, DescribeDisksParams.class);
        if(msg.getDiskType() != null && "all".equals(msg.getDiskType())){
            params.setDiskType(null);
        }

        if(msg.getStatus() != null){

            if(!"All".equals(msg.getStatus())){
                StandardDiskStatus stdStatus = StandardDiskStatus.value(msg.getStatus());
                if(stdStatus == null){
                    throw new GCloudException("0060316:不支持此状态查询");
                }
                if(stdStatus.getGcStatus() == null || stdStatus.getGcStatus().size() == 0){
                    params.setEmptyList(true);
                }else{
                    params.setStatus(null);
                    params.setStatuses(stdStatus.getGcStatusValues());
                }
            }
            params.setStatus(null);
        }

        return params;
    }
}
