package com.gcloud.controller.storage.handler.api.volume;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.storage.model.DetailDiskParams;
import com.gcloud.controller.storage.service.IVolumeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.DetailDisk;
import com.gcloud.header.storage.msg.api.volume.ApiDetailDiskMsg;
import com.gcloud.header.storage.msg.api.volume.ApiDetailDiskReplyMsg;

@ApiHandler(module= Module.ECS,subModule=SubModule.DISK, action="DetailDisk")
public class ApiDetailDiskHandler extends MessageHandler<ApiDetailDiskMsg, ApiDetailDiskReplyMsg>{

	@Autowired
    private IVolumeService volumeService;
	
	@Override
	public ApiDetailDiskReplyMsg handle(ApiDetailDiskMsg msg) throws GCloudException {
		DetailDiskParams params = BeanUtil.copyBean(msg, DetailDiskParams.class);
		DetailDisk response = volumeService.detailDisk(params, msg.getCurrentUser());
		
		ApiDetailDiskReplyMsg reply = new ApiDetailDiskReplyMsg();
		reply.setDetailDisk(response);
		return reply;
	}

}
