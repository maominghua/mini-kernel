package com.gcloud.controller.storage.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.storage.workflow.model.volume.InstanceFsFreezeFlowCommandReq;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.enums.FsfreezeType;
import com.gcloud.header.compute.msg.node.vm.senior.InstanceFsFreezeMsg;

import lombok.extern.slf4j.Slf4j;

@Component
@Scope("prototype")
@Slf4j
public class InstanceFsThawFlowCommand extends BaseWorkFlowCommand{
	@Autowired
    private MessageBus messageBus;
	
	@Autowired
    private IVmBaseService vmBaseService;
	
	@Override
	public boolean judgeExecute() {
		InstanceFsFreezeFlowCommandReq req = (InstanceFsFreezeFlowCommandReq)getReqParams();
		return req.getResParam().isNeedFsFreeze();
	}
	
	@Override
	protected Object process() throws Exception {
		InstanceFsFreezeFlowCommandReq req = (InstanceFsFreezeFlowCommandReq)getReqParams();
		VmInstance vm = vmBaseService.getInstanceById(req.getResParam().getInstanceId());
		InstanceFsFreezeMsg msg = new InstanceFsFreezeMsg();
		msg.setFsfreezeType(FsfreezeType.THAWED.getValue());
		msg.setInstanceId(vm.getId());
		msg.setServiceId(MessageUtil.computeServiceId(vm.getHostname()));
		msg.setTaskId(getTaskId());
		
		messageBus.send(msg);
		return null;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return InstanceFsFreezeFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}

}
