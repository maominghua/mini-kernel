package com.gcloud.controller.storage.workflow;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.storage.workflow.model.volume.OnLineSnapshotInitFlowCommandRes;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;

import lombok.extern.slf4j.Slf4j;
@Component
@Scope("prototype")
@Slf4j
public class OnLineSnapshotInitFlowCommand extends BaseWorkFlowCommand{
	@Override
	protected Object process() throws Exception {
		String snapshotId = StringUtils.genUuid();
		OnLineSnapshotInitFlowCommandRes res = new OnLineSnapshotInitFlowCommandRes();
		res.setSnapshotId(snapshotId);
		return res;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return null;
	}

	@Override
	protected Class<?> getResParamClass() {
		return OnLineSnapshotInitFlowCommandRes.class;
	}

}
