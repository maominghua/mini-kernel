package com.gcloud.image.driver;

import com.gcloud.core.service.SpringUtil;

public enum ImageDriverNodeEnum {
	FILE_NODE("file", "node", FileStoreNodeDriver.class),
	FILE_VG("file", "vg", FileStoreVgDriver.class),
	FILE_RBD("file", "rbd", FileStoreRbdDriver.class),
	RBD_NODE("rbd", "node", RbdStoreNodeDriver.class),
	RBD_VG("rbd", "node", RbdStoreVgDriver.class);

	private String imageStorageType;
	private String storageType;
	private Class driverClazz;
	
	ImageDriverNodeEnum(String imageStorageType, String storageType, Class clazz){
		this.imageStorageType = imageStorageType;
		this.storageType = storageType;
		this.driverClazz = clazz;
	}
	
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	
	public Class getDriverClazz() {
		return driverClazz;
	}

	public void setDriverClazz(Class driverClazz) {
		this.driverClazz = driverClazz;
	}

	public String getImageStorageType() {
		return imageStorageType;
	}

	public void setImageStorageType(String imageStorageType) {
		this.imageStorageType = imageStorageType;
	}

	public static IImageStoreNodeDriver getByType(String imageStorageType, String storageType) {
		for (ImageDriverNodeEnum driverE : ImageDriverNodeEnum.values()) {
			if(driverE.getImageStorageType().equals(imageStorageType) && driverE.getStorageType().equals(storageType)) {
				return (IImageStoreNodeDriver)SpringUtil.getBean(driverE.driverClazz);
			}
		}
		return null;
	}
}
