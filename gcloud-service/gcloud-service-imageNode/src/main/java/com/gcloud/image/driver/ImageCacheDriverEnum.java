package com.gcloud.image.driver;

import com.gcloud.core.service.SpringUtil;

public enum ImageCacheDriverEnum {
	NODE("node", ImageCacheFileStoreDriverImpl.class),
	RBD("rbd", ImageCacheRbdStoreDriverImpl.class),
	VG("vg", ImageCacheVgStoreDriverImpl.class);

	private String storageType;
	private Class driverClazz;
	
	ImageCacheDriverEnum(String storageType, Class clazz){
		this.storageType = storageType;
		this.driverClazz = clazz;
	}
	
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	
	public Class getDriverClazz() {
		return driverClazz;
	}

	public void setDriverClazz(Class driverClazz) {
		this.driverClazz = driverClazz;
	}

	public static IImageCacheStoreDriver getByType(String type) {
		for (ImageCacheDriverEnum driverE : ImageCacheDriverEnum.values()) {
			if(driverE.getStorageType().equals(type)) {
				return (IImageCacheStoreDriver)SpringUtil.getBean(driverE.driverClazz);
			}
		}
		return null;
	}
}
