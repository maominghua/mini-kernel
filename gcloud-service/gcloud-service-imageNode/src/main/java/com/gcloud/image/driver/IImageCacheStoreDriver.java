package com.gcloud.image.driver;

public interface IImageCacheStoreDriver {
	void deleteImageCache(String imageId, String storeTarget);
}
