package com.gcloud.image.driver;

import java.io.File;

import org.springframework.stereotype.Component;

import com.gcloud.core.exception.GCloudException;
import com.gcloud.service.common.lvm.uitls.LvmUtil;

/**
 * 镜像的后端存储为file,镜像分发到vg
 *
 */
@Component
public class FileStoreVgDriver implements IImageStoreNodeDriver{

	@Override
	public void downloadImage(String sourceFilePath, String imageId, String target) {
		//后续考虑统一放到node节点上执行，也更好统一的feedback，并且符合长操作统一在node执行的规范
		//将镜像文件dd到对应的vg上  dd if=$1 of=/dev/$2/$3 bs=5M
		//需要创建镜像lv？  // lvcreate -L 2500 -n volumeName poolName
		File file = new File(sourceFilePath);
        if (!file.exists()) {
            throw new GCloudException("::镜像文件不存在");
        }
		LvmUtil.lvCreate(target, "img-" + imageId, (int)Math.ceil(file.length()/1024.0/1024.0/1024.0), "创建镜像：" + imageId + "lv失败");
		LvmUtil.activeLv(LvmUtil.getImagePath(target, imageId));
        LvmUtil.ddToVg(sourceFilePath, target, "img-" + imageId, "::复制镜像到vg:" + target + "失败");
	}

}
