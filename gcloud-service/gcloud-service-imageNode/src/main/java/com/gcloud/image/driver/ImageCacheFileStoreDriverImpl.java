package com.gcloud.image.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.image.prop.ImageNodeProp;
/**
 * 存放在node节点上的本地镜像缓存
 *
 */
@Component
public class ImageCacheFileStoreDriverImpl implements IImageCacheStoreDriver {
	
	@Autowired
	ImageNodeProp prop;

	@Override
	public void deleteImageCache(String imageId, String storeTarget) {
		String[] cmd = null;
        cmd = new String[]{"rm", "-f", prop.getImageCachedPath() + imageId};
        int res = SystemUtil.runAndGetCode(cmd);
        if(res != 0) {
        	throw new GCloudException("::删除镜像失败");
        }
	}

}
