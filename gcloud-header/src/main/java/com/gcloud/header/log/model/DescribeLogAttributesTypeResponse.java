package com.gcloud.header.log.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class DescribeLogAttributesTypeResponse implements Serializable{
	@ApiModel(description = "任务列表")
	private List<LogAttributesType> log;

	public List<LogAttributesType> getLog() {
		return log;
	}

	public void setLog(List<LogAttributesType> log) {
		this.log = log;
	}

}
