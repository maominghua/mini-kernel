package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

public class StandardVSwitchSetType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "交换机ID")
	private String vSwitchId;
	@ApiModel(description = "网络ID")
	private String networkId;
	@ApiModel(description = "交换机所在的专有网络ID")
	private String vpcId;
	@ApiModel(description = "交换机的地址")
	private String cidrBlock;
	@ApiModel(description = "交换机名字")
	private String vSwitchName;
	@ApiModel(description = "可用区ID")
	private String zoneId;
	@ApiModel(description = "关联的路由ID")
	private String vRouterId;
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date creationTime;
	@ApiModel(description = "dns列表")
	private String dnsServers;
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public String getvSwitchName() {
		return vSwitchName;
	}
	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getvRouterId() {
		return vRouterId;
	}
	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public String getDnsServers() {
		return dnsServers;
	}
	public void setDnsServers(String dnsServers) {
		this.dnsServers = dnsServers;
	}
}
