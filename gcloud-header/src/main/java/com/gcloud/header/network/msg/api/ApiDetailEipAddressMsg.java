package com.gcloud.header.network.msg.api;

import javax.validation.constraints.NotNull;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailEipAddressMsg extends ApiMessage{
	@ApiModel(description = "弹性 IP地址申请Id", require = true)
	@NotNull(message = "0050601::弹性 IP地址申请ID不能为空")
	private String allocationId;

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	@Override
	public Class replyClazz() {
		return ApiDetailEipAddressReplyMsg.class;
	}
	
}
