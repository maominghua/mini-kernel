package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.standard.StandardDescribeNetworkInterfacesResponse;
import com.gcloud.header.network.model.standard.StandardNetworkInterfaceSet;

import java.util.List;

public class StandardDescribeNetworkInterfacesReplyMsg extends PageReplyMessage<StandardNetworkInterfaceSet> {

	private static final long serialVersionUID = 1L;

	private StandardDescribeNetworkInterfacesResponse networkInterfaceSets;
	@Override
	public void setList(List<StandardNetworkInterfaceSet> list) {
		networkInterfaceSets = new StandardDescribeNetworkInterfacesResponse();
		networkInterfaceSets.setNetworkInterfaceSet(list);
		
	}
	public StandardDescribeNetworkInterfacesResponse getNetworkInterfaceSets() {
		return networkInterfaceSets;
	}
	public void setNetworkInterfaceSets(StandardDescribeNetworkInterfacesResponse networkInterfaceSets) {
		this.networkInterfaceSets = networkInterfaceSets;
	}
	
}
