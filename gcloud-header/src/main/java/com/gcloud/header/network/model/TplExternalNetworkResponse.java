package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class TplExternalNetworkResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "外网")
	private List<TplExternalNetworkItem> externalNetwork;

	public List<TplExternalNetworkItem> getExternalNetwork() {
		return externalNetwork;
	}

	public void setExternalNetwork(List<TplExternalNetworkItem> externalNetwork) {
		this.externalNetwork = externalNetwork;
	}

}
