package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class TplExternalNetworkItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "网络ID")
	private String networkId;
	@ApiModel(description = "网络名称")
	private String networkName;
	@ApiModel(description = "交换机信息集合")
	private TplVSwitchItems vSwitchItemItems;
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public TplVSwitchItems getvSwitchItemItems() {
		return vSwitchItemItems;
	}
	public void setvSwitchItemItems(TplVSwitchItems vSwitchItemItems) {
		this.vSwitchItemItems = vSwitchItemItems;
	}
	
	
}
