package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;

public class StandardVpcType implements Serializable{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "ID")
	private String vpcId;
	@ApiModel(description = "区域ID")
	private String regionId = ControllerProperty.REGION_ID;
	@ApiModel(description = "状态")
	private String status;
	private String vpcName;
	private String cnStatus;
	@ApiModel(description = "子网ID列表")
	private Map<String, List> vSwitchIds;

	//TODO 缺
	//cidrBlock、creationTime
	
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Map<String, List> getvSwitchIds() {
		return vSwitchIds;
	}
	public void setvSwitchIds(Map<String, List> vSwitchIds) {
		this.vSwitchIds = vSwitchIds;
	}
}
