package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDeleteSecurityGroupMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全组Id", require = true)
	@NotBlank(message = "0040401")
	private String securityGroupId;

	@Override
	public Class replyClazz() {
		return StandardApiDeleteSecurityGroupReplyMsg.class;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}
}
