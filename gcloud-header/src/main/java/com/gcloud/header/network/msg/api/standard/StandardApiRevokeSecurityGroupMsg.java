package com.gcloud.header.network.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiRevokeSecurityGroupMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全组规则Id", require = true)
    @NotBlank(message = "0040601")
    private String securityGroupRuleId;

	@Override
	public Class replyClazz() {
		return StandardApiRevokeSecurityGroupReplyMsg.class;
	}

	public String getSecurityGroupRuleId() {
		return securityGroupRuleId;
	}

	public void setSecurityGroupRuleId(String securityGroupRuleId) {
		this.securityGroupRuleId = securityGroupRuleId;
	}
}
