package com.gcloud.header.network.msg.api;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailVRouterMsg extends ApiMessage{

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDetailVRouterReplyMsg.class;
	}
	
	@ApiModel(description = "路由ID", require = true)
	@NotBlank(message = "::路由ID不能为空")
	private String vRouterId;

	public String getvRouterId() {
		return vRouterId;
	}

	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
}
