package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.standard.StandardDescribeVpcsResponse;
import com.gcloud.header.network.model.standard.StandardVpcType;

public class StandardApiDescribeVpcsReplyMsg extends PageReplyMessage<StandardVpcType>{
	private static final long serialVersionUID = 1L;

	private StandardDescribeVpcsResponse vpcs;
	
	@Override
	public void setList(List<StandardVpcType> list) {
		vpcs = new StandardDescribeVpcsResponse();
		vpcs.setVpc(list);
	}

	public StandardDescribeVpcsResponse getVpcs() {
		return vpcs;
	}

	public void setVpcs(StandardDescribeVpcsResponse vpcs) {
		this.vpcs = vpcs;
	}
}
