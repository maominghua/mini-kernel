package com.gcloud.header.network.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailSecurityGroupMsg  extends ApiMessage{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0040801")
	@ApiModel(description = "安全组ID", require = true)
	private String securityGroupId;

	@Override
	public Class replyClazz() {
		return ApiDetailSecurityGroupReplyMsg.class;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

}
