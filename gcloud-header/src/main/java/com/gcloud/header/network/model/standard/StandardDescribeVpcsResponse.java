package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeVpcsResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<StandardVpcType> vpc;

	public List<StandardVpcType> getVpc() {
		return vpc;
	}

	public void setVpc(List<StandardVpcType> vpc) {
		this.vpc = vpc;
	}
}
