package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class TplVRouterSet implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "路由器Id")
    private String vRouterId;
    @ApiModel(description = "路由器名称")
    private String vRouterName;
    @ApiModel(description = "网关网络")
    private String gatewayNetwork;
	public String getvRouterId() {
		return vRouterId;
	}
	public void setvRouterId(String vRouterId) {
		this.vRouterId = vRouterId;
	}
	public String getvRouterName() {
		return vRouterName;
	}
	public void setvRouterName(String vRouterName) {
		this.vRouterName = vRouterName;
	}
	public String getGatewayNetwork() {
		return gatewayNetwork;
	}
	public void setGatewayNetwork(String gatewayNetwork) {
		this.gatewayNetwork = gatewayNetwork;
	}
}
