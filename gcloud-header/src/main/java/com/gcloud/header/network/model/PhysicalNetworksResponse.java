package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;


public class PhysicalNetworksResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<PhysicalNetworksItemType> physicalNetworksItem;

	public List<PhysicalNetworksItemType> getPhysicalNetworksItem() {
		return physicalNetworksItem;
	}

	public void setPhysicalNetworksItem(List<PhysicalNetworksItemType> physicalNetworksItem) {
		this.physicalNetworksItem = physicalNetworksItem;
	}
	
}
