package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class TplVSwitchItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="交换机信息")
	private List<TplVSwitchItem> vSwitchItemItem;
	public List<TplVSwitchItem> getvSwitchItemItem() {
		return vSwitchItemItem;
	}
	public void setvSwitchItemItem(List<TplVSwitchItem> vSwitchItemItem) {
		this.vSwitchItemItem = vSwitchItemItem;
	}

}
