package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeSecurityGroupsResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<StandardSecurityGroupItemType> SecurityGroup;

	public List<StandardSecurityGroupItemType> getSecurityGroup() {
		return SecurityGroup;
	}

	public void setSecurityGroup(List<StandardSecurityGroupItemType> securityGroup) {
		SecurityGroup = securityGroup;
	}
}
