package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.network.model.DetailExternalNetwork;

public class ApiDetailExternalNetworkReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private DetailExternalNetwork detailExternalNetwork;

	public DetailExternalNetwork getDetailExternalNetwork() {
		return detailExternalNetwork;
	}

	public void setDetailExternalNetwork(DetailExternalNetwork detailExternalNetwork) {
		this.detailExternalNetwork = detailExternalNetwork;
	}
}
