package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.TplExternalNetworkResponse;

public class ApiTplExternalNetworksReplyMsg extends ApiReplyMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "外网列表")
	TplExternalNetworkResponse externalNetworks;
	public TplExternalNetworkResponse getExternalNetworks() {
		return externalNetworks;
	}
	public void setExternalNetworks(TplExternalNetworkResponse externalNetworks) {
		this.externalNetworks = externalNetworks;
	}
}
