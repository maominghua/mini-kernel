package com.gcloud.header.network.msg.api;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiCreateMessage;
import com.gcloud.header.api.ApiModel;

public class CreateExternalNetworkMsg extends ApiCreateMessage{

	@ApiModel(description="网络名称", require=true)
	@Length(min=2, max = 20, message = "0160101::外部网络名称不能为空")
	private String networkName;
	@ApiModel(description="网络模式", require=true)
	@NotBlank(message="::网络模式不能为空")
	private String networkType;// LOCAL,FLAT,VLAN,GRE
	@ApiModel(description="物理网络", require=true)
	@NotBlank(message="::物理网络不能为空")
	private String physicalNetwork;
	@ApiModel(description="segment ID，vlan模式时必填")
	private Integer segmentId;
	
	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getPhysicalNetwork() {
		return physicalNetwork;
	}

	public void setPhysicalNetwork(String physicalNetwork) {
		this.physicalNetwork = physicalNetwork;
	}

	public Integer getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return CreateExternalNetworkReplyMsg.class;
	}
	
	
}
