package com.gcloud.header.network.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiUnassociateEipAddressMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0050301::弹性公网实例Id不能为空")
	@ApiModel(description = "弹性公网实例Id", require = true)
	private String allocationId;
	@ApiModel(description = "实例Id")
	@NotBlank(message = "0050303::实例id不能为空")
	private String instanceId;
	@NotBlank(message = "0050304::实例类型不能为空")
	@ApiModel(description = "实例类型", require = true)
	private String instanceType;//Netcard

	@Override
	public Class replyClazz() {
		return StandardApiUnassociateEipAddressReplyMsg.class;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
}
