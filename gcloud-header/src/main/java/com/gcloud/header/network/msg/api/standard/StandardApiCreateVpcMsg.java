package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiCreateVpcMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description="专有网络ID", require=true)
	@NotBlank(message = "0100101::专有网络名称不能为空")
	private String vpcName;
	
	@Override
	public Class replyClazz() {
		return StandardApiCreateVpcReplyMsg.class;
	}

}
