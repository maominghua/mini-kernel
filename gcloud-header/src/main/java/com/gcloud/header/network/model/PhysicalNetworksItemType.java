package com.gcloud.header.network.model;

import java.io.Serializable;

public class PhysicalNetworksItemType implements Serializable{
	private String phyDev;

	public String getPhyDev() {
		return phyDev;
	}

	public void setPhyDev(String phyDev) {
		this.phyDev = phyDev;
	}
	
}
