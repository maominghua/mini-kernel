package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotNull;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiModifyEipAddressAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "弹性 IP地址申请Id", require = true)
	@NotNull(message = "0050501::弹性 IP地址申请ID不能为空")
	private String allocationId;
	@ApiModel(description = "弹性 IP地址带宽", require = true)
	@NotNull(message = "0050502::弹性 IP地址带宽不能为空")
	private Integer bandwidth;//带宽以 Mbps 计算
	//TODO 缺少 name 

	@Override
	public Class replyClazz() {
		return StandardApiModifyEipAddressAttributeReplyMsg.class;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}
}
