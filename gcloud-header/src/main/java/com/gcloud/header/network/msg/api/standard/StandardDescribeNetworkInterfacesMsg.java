package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

import java.util.List;

public class StandardDescribeNetworkInterfacesMsg extends ApiPageMessage {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "VPC 的虚拟交换机 ID")
	private String vSwitchId;
	@ApiModel(description = "网卡IP")
	private String primaryIpAddress;
	@ApiModel(description = "安全组ID")
	private String securityGroupId;
	@ApiModel(description = "网卡的名称")
	private String networkInterfaceName;
	@ApiModel(description = "实例 ID")
	private String instanceId;
	@ApiModel(description = "网卡ID列表")
	private List<String> networkInterfaceIds;
//	private String regionId;
	
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return StandardDescribeNetworkInterfacesReplyMsg.class;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

	public String getPrimaryIpAddress() {
		return primaryIpAddress;
	}

	public void setPrimaryIpAddress(String primaryIpAddress) {
		this.primaryIpAddress = primaryIpAddress;
	}

	public String getSecurityGroupId() {
		return securityGroupId;
	}

	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public List<String> getNetworkInterfaceIds() {
		return networkInterfaceIds;
	}

	public void setNetworkInterfaceIds(List<String> networkInterfaceIds) {
		this.networkInterfaceIds = networkInterfaceIds;
	}

}
