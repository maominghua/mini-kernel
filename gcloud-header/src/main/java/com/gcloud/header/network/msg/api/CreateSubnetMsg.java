package com.gcloud.header.network.msg.api;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class CreateSubnetMsg extends ApiMessage{
	@NotBlank(message = "0170101::子网网段不能为空")
	@ApiModel(description = "网段，形如1.1.1.1/20", require = true)
	private String cidrBlock;
	@NotBlank(message = "0170102::网络ID不能为空")
	@ApiModel(description = "网络ID")
	private String networkId;
	@NotBlank(message = "0170103::名称不能为空")
	@ApiModel(description = "子网名称", require = true)
	private String subnetName;
	@ApiModel(description = "网关IP")
	private String gatewayIp;
	@ApiModel(description = "dns列表")
	private List<String> dnsNameServers;
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getSubnetName() {
		return subnetName;
	}
	public void setSubnetName(String subnetName) {
		this.subnetName = subnetName;
	}
	public String getGatewayIp() {
		return gatewayIp;
	}
	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}
	public List<String> getDnsNameServers() {
		return dnsNameServers;
	}
	public void setDnsNameServers(List<String> dnsNameServers) {
		this.dnsNameServers = dnsNameServers;
	}
	@Override
	public Class replyClazz() {
		return CreateSubnetReplyMsg.class;
	}
	
	/*@ApiModel(description = "VSwitch所属区的ID")
	@NotBlank(message = "0030103::可用区不能为空")
	private String zoneId;
	@ApiModel(description = "区域ID")
	private String regionId;*/
	
}
