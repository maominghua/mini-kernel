package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.network.model.DetailNic;

public class ApiDetailNetworkInterfaceReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private DetailNic detailNetworkInterface;

	public DetailNic getDetailNetworkInterface() {
		return detailNetworkInterface;
	}

	public void setDetailNetworkInterface(DetailNic detailNetworkInterface) {
		this.detailNetworkInterface = detailNetworkInterface;
	}
}
