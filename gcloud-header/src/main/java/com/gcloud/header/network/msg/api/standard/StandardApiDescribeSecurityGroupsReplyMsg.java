package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.standard.StandardDescribeSecurityGroupsResponse;
import com.gcloud.header.network.model.standard.StandardSecurityGroupItemType;

public class StandardApiDescribeSecurityGroupsReplyMsg extends PageReplyMessage<StandardSecurityGroupItemType>{
	private static final long serialVersionUID = 1L;
	
	private StandardDescribeSecurityGroupsResponse securityGroups;

	@Override
	public void setList(List<StandardSecurityGroupItemType> list) {
		securityGroups = new StandardDescribeSecurityGroupsResponse();
		securityGroups.setSecurityGroup(list);
	}

	public StandardDescribeSecurityGroupsResponse getSecurityGroups() {
		return securityGroups;
	}

	public void setSecurityGroups(StandardDescribeSecurityGroupsResponse securityGroups) {
		this.securityGroups = securityGroups;
	}
}
