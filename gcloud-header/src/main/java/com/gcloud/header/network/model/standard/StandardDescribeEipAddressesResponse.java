package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeEipAddressesResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<StandardEipAddressSetType> eipAddress;

	public List<StandardEipAddressSetType> getEipAddress() {
		return eipAddress;
	}

	public void setEipAddress(List<StandardEipAddressSetType> eipAddress) {
		this.eipAddress = eipAddress;
	}
}
