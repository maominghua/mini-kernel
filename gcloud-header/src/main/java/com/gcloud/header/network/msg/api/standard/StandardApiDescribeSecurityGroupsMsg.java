package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.ApiPageMessage;

public class StandardApiDescribeSecurityGroupsMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeSecurityGroupsReplyMsg.class;
	}

}
