package com.gcloud.header.network.msg.api;


import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

import javax.validation.constraints.NotBlank;

public class DescribeSecurityGroupAttributeMsg extends ApiMessage {
	
	@ApiModel(description = "安全组Id", require = true)
	@NotBlank(message = "0040701")
	private String securityGroupId;
	@ApiModel(description = "安全组方向")
	private String direction;
	@ApiModel(description = "网络类型")
	private String etherType;

	
	public String getSecurityGroupId() {
		return securityGroupId;
	}


	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public String getDirection() {
		return direction;
	}


	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getEtherType() {
		return etherType;
	}

	public void setEtherType(String etherType) {
		this.etherType = etherType;
	}

	@Override
	public Class replyClazz() {
		return DescribeSecurityGroupAttributeReplyMsg.class;
	}

}
