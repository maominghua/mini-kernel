package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiTplExternalNetworksMsg extends ApiMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiTplExternalNetworksReplyMsg.class;
	}

}
