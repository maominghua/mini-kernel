package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiAssociateEipAddressMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0050201::弹性公网实例Id不能为空")
	@ApiModel(description = "弹性公网 IP的申请 Id", require = true)
	private String allocationId;
	@NotBlank(message = "0050202::实例类型不能为空")
	@ApiModel(description = "实例类型", require = true)
	private String instanceType;//Netcard
	@NotBlank(message = "0050203::实例Id不能为空")
	@ApiModel(description = "实例Id", require = true)
	private String instanceId;

	@Override
	public Class replyClazz() {
		return StandardApiAssociateEipAddressReplyMsg.class;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
