package com.gcloud.header.network.msg.api;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.msg.api.standard.StandardDescribeNetworkInterfacesMsg;

import java.util.List;

public class DescribeNetworkInterfacesMsg extends StandardDescribeNetworkInterfacesMsg {

	private static final long serialVersionUID = 1L;

	@ApiModel(description = "端口的拥有者")
	private List<String> deviceOwners;

	@ApiModel(description = "是否包含没有拥有者")
	private Boolean includeOwnerless;
	
	@Override
	public Class replyClazz() {
		// TODO Auto-generated method stub
		return DescribeNetworkInterfacesReplyMsg.class;
	}

	public List<String> getDeviceOwners() {
		return deviceOwners;
	}

	public void setDeviceOwners(List<String> deviceOwners) {
		this.deviceOwners = deviceOwners;
	}

	public Boolean getIncludeOwnerless() {
		return includeOwnerless;
	}

	public void setIncludeOwnerless(Boolean includeOwnerless) {
		this.includeOwnerless = includeOwnerless;
	}
}
