package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.ListReplyMessage;
import com.gcloud.header.network.model.PhysicalNetworksItemType;
import com.gcloud.header.network.model.PhysicalNetworksResponse;

public class DescribePhysicalNetworksReplyMsg extends ListReplyMessage<PhysicalNetworksItemType>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	PhysicalNetworksResponse physicalNetworksItems;
	@Override
	public void setList(List<PhysicalNetworksItemType> list) {
		physicalNetworksItems = new PhysicalNetworksResponse();
		physicalNetworksItems.setPhysicalNetworksItem(list);
	}
	public PhysicalNetworksResponse getPhysicalNetworksItems() {
		return physicalNetworksItems;
	}
	public void setPhysicalNetworksItems(PhysicalNetworksResponse physicalNetworksItems) {
		this.physicalNetworksItems = physicalNetworksItems;
	}

}
