package com.gcloud.header.network.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.standard.StandardNetworkInterfaceSet;

import java.io.Serializable;

public class NetworkInterfaceSet extends StandardNetworkInterfaceSet implements Serializable{

	@TableField("device_id")
	private String deviceId; // 弹性网卡当前关联的实例 ID
	@ApiModel(description = "实例类型")
	@TableField("device_owner")
	private String deviceType; // 弹性网卡当前关联的实例 ID

	@JsonIgnore
	private String securityGroupIdsStr;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getSecurityGroupIdsStr() {
		return securityGroupIdsStr;
	}

	public void setSecurityGroupIdsStr(String securityGroupIdsStr) {
		this.securityGroupIdsStr = securityGroupIdsStr;
	}
}
