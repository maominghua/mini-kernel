package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.network.model.DetailVRouter;

public class ApiDetailVRouterReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private DetailVRouter detailVRouter;

	public DetailVRouter getDetailVRouter() {
		return detailVRouter;
	}

	public void setDetailVRouter(DetailVRouter detailVRouter) {
		this.detailVRouter = detailVRouter;
	}
}
