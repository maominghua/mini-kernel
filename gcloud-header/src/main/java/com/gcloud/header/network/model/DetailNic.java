package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

public class DetailNic implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "网卡ID")
	private String networkInterfaceId;
	
	@ApiModel(description = "网卡名称")
	private String networkInterfaceName;
	
	@ApiModel(description ="中文状态")
	private String cnStatus;
	
	@ApiModel(description ="创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date creationTime;
	
	@ApiModel(description = "创建者")
	private String creator;
	
	@ApiModel(description = "")
	private String deviceId;
	
	@ApiModel(description = "")
	private String deviceName;
	
	@ApiModel(description = "")
	private String deviceType;
	
	@ApiModel(description = "Mac地址")
	private String macAddress;
	
	@ApiModel(description = "网络ID")
	private String networkId;
	
	@ApiModel(description = "网络名称")
	private String networkName;
	
	@ApiModel(description = "IP地址")
	private String privateIpAddress;
	
	@ApiModel(description = "状态")
	private String status;
	
	@ApiModel(description = "")
	private String vSwitchId;
	
	@ApiModel(description = "")
	private String vSwitchName;
	
	@ApiModel(description = "vpc的ID")
	private String vpcId;
	
	@ApiModel(description = "vpc名称")
	private String vpcName;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	
	@ApiModel(description = "可用区名称")
	private String zoneName;
//		securityGroups: [{id: "6a1e7b711fea4f4ab126e3985c3da837", name: "默认安全组"}]
	
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}
	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}
	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getPrivateIpAddress() {
		return privateIpAddress;
	}
	public void setPrivateIpAddress(String privateIpAddress) {
		this.privateIpAddress = privateIpAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	public String getvSwitchName() {
		return vSwitchName;
	}
	public void setvSwitchName(String vSwitchName) {
		this.vSwitchName = vSwitchName;
	}
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
}
