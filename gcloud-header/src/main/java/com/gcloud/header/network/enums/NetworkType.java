package com.gcloud.header.network.enums;

import java.util.Arrays;

public enum NetworkType {
	VLAN, FLAT;

    public String value(){
        return name().toLowerCase();
    }

    public static NetworkType value(String value){
        return Arrays.stream(NetworkType.values()).filter(type -> type.value().equals(value)).findFirst().orElse(null);
    }
}
