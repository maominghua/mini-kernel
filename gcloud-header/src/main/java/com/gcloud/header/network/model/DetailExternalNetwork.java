package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.controller.ControllerProperty;

public class DetailExternalNetwork implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String cidrBlock;
	private String cnStatus;
	private String networkId;
	private String networkName;
	private String regionId = ControllerProperty.REGION_ID;
	private String status;
//	vSwitchIds: {vSwitchId: ["3912e754-1210-4f97-bebf-a79eb1e87d9d"]}
	
	public String getCidrBlock() {
		return cidrBlock;
	}
	public void setCidrBlock(String cidrBlock) {
		this.cidrBlock = cidrBlock;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public String getNetworkId() {
		return networkId;
	}
	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	public String getNetworkName() {
		return networkName;
	}
	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
