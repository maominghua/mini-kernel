package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeNetworkInterfacesResponse implements Serializable{
	private List<StandardNetworkInterfaceSet> networkInterfaceSet;

	public List<StandardNetworkInterfaceSet> getNetworkInterfaceSet() {
		return networkInterfaceSet;
	}

	public void setNetworkInterfaceSet(List<StandardNetworkInterfaceSet> networkInterfaceSet) {
		this.networkInterfaceSet = networkInterfaceSet;
	}
}
