package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class TplVpcResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "VPC")
	private List<TplVpcItem> vpc;
	public List<TplVpcItem> getVpc() {
		return vpc;
	}
	public void setVpc(List<TplVpcItem> vpc) {
		this.vpc = vpc;
	}
}
