package com.gcloud.header.network.msg.api;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.DescribeSubnetModel;
import com.gcloud.header.network.model.DescribeSubnetResponse;

public class ApiDescribeSubnetReplyMsg extends PageReplyMessage<DescribeSubnetModel>{
	private static final long serialVersionUID = 1L;
	
	private DescribeSubnetResponse describeSubnets;

	@Override
	public void setList(List<DescribeSubnetModel> list) {
		describeSubnets = new DescribeSubnetResponse();
		describeSubnets.setDescribeSubnet(list);
	}

	public DescribeSubnetResponse getDescribeSubnets() {
		return describeSubnets;
	}

	public void setDescribeSubnets(DescribeSubnetResponse describeSubnets) {
		this.describeSubnets = describeSubnets;
	}
}
