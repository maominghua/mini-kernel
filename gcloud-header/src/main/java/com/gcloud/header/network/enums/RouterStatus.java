package com.gcloud.header.network.enums;

import java.util.Arrays;

import com.google.common.base.CaseFormat;

/**
 * 网络的状态
 */


public enum RouterStatus {

	ACTIVE("激活"),
	DOWN("失活"),
	BUILD("已创建"),
	ERROR("错误"),
	PENDING_CREATE("创建中"),
	PENDING_UPDATE("删除中"),
	PENDING_DELETE("删除中"),
	UNRECOGNIZED("未知");

	private String cnName;

	RouterStatus(String cnName) {
		this.cnName = cnName;
	}

	public String value() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
	}

	public String getCnName() {
		return cnName;
	}
	
	public static String getCnName(String value) {
		RouterStatus enu =  Arrays.stream(RouterStatus.values()).filter(type -> type.value().equals(value)).findFirst().orElse(null);
		return enu !=null?enu.getCnName():null;
	}
}
