package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

import javax.validation.constraints.NotBlank;

public class AllocateEipAddressMsg extends ApiMessage {
	@NotBlank(message = "0050101::弹性公网实例Id不能为空")
	@ApiModel(description = "公网网络ID", require = true)
	private String networkId;
//	//仅支持阿里的，默认值为5
	private Integer bandwidth;
	
	@Override
	public Class replyClazz() {
		return AllocateEipAddressReplyMsg.class;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}
}
