package com.gcloud.header.network.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class TplVpcItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="vpc ID")
	private String vpcId;
	@ApiModel(description="vpc 名称")
	private String vpcName;
	@ApiModel(description="路由信息")
	private TplVRouterSet vRouter;
	@ApiModel(description = "交换机信息集合")
	private TplVSwitchItems vSwitchItemItems;
	public String getVpcId() {
		return vpcId;
	}
	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}
	public String getVpcName() {
		return vpcName;
	}
	public void setVpcName(String vpcName) {
		this.vpcName = vpcName;
	}
	public TplVRouterSet getvRouter() {
		return vRouter;
	}
	public void setvRouter(TplVRouterSet vRouter) {
		this.vRouter = vRouter;
	}
	public TplVSwitchItems getvSwitchItemItems() {
		return vSwitchItemItems;
	}
	public void setvSwitchItemItems(TplVSwitchItems vSwitchItemItems) {
		this.vSwitchItemItems = vSwitchItemItems;
	}

}
