package com.gcloud.header.network.model;

import java.io.Serializable;
import java.util.List;

public class DescribeSubnetResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<DescribeSubnetModel> describeSubnet;

	public List<DescribeSubnetModel> getDescribeSubnet() {
		return describeSubnet;
	}

	public void setDescribeSubnet(List<DescribeSubnetModel> describeSubnet) {
		this.describeSubnet = describeSubnet;
	}
}
