package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.DetailEipAddressResponse;

public class ApiDetailEipAddressReplyMsg extends ApiReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="浮动IP详情")
	private DetailEipAddressResponse detailEipAddress;

	public DetailEipAddressResponse getDetailEipAddress() {
		return detailEipAddress;
	}

	public void setDetailEipAddress(DetailEipAddressResponse detailEipAddress) {
		this.detailEipAddress = detailEipAddress;
	}
	

}
