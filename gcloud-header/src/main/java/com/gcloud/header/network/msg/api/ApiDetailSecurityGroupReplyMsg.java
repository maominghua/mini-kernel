package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.DetailSecurityGroupResponse;

public class ApiDetailSecurityGroupReplyMsg extends ApiReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description="安全组详情信息")
	private DetailSecurityGroupResponse detailSecurityGroup;

	public DetailSecurityGroupResponse getDetailSecurityGroup() {
		return detailSecurityGroup;
	}

	public void setDetailSecurityGroup(DetailSecurityGroupResponse detailSecurityGroup) {
		this.detailSecurityGroup = detailSecurityGroup;
	}
	
	

}
