package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDescribeEipAddressesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description="弹性公网IP地址")
	private String eipAddress;
	@ApiModel(description="弹性公网IP地址申请Id")
	private String allocationId;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeEipAddressesReplyMsg.class;
	}

	public String getEipAddress() {
		return eipAddress;
	}

	public void setEipAddress(String eipAddress) {
		this.eipAddress = eipAddress;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
}
