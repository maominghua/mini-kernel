package com.gcloud.header.network.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDeleteVSwitchMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "0030401::交换机ID不能为空")
	@ApiModel(description = "交换机Id", require = true)
	private String vSwitchId;
	
	@Override
	public Class replyClazz() {
		return StandardApiDeleteVSwitchReplyMsg.class;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
}
