package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.TplVpcResponse;

public class ApiTplVpcsReplyMsg extends ApiReplyMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "VPC列表")
	TplVpcResponse vpcs;
	public TplVpcResponse getVpcs() {
		return vpcs;
	}
	public void setVpcs(TplVpcResponse vpcs) {
		this.vpcs = vpcs;
	}
}
