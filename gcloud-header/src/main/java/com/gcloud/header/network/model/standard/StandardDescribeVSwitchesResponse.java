package com.gcloud.header.network.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeVSwitchesResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<StandardVSwitchSetType> vSwitch;

	public List<StandardVSwitchSetType> getvSwitch() {
		return vSwitch;
	}

	public void setvSwitch(List<StandardVSwitchSetType> vSwitch) {
		this.vSwitch = vSwitch;
	}
}
