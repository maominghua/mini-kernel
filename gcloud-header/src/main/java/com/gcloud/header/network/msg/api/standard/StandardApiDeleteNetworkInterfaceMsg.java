package com.gcloud.header.network.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDeleteNetworkInterfaceMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "网卡ID", require = true)
    @NotBlank(message = "0080301::端口ID不能为空")
    private String networkInterfaceId;
	
	@Override
	public Class replyClazz() {
		return StandardApiDeleteNetworkInterfaceReplyMsg.class;
	}

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
}
