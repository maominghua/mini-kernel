package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.standard.StandardDescribeVSwitchesResponse;
import com.gcloud.header.network.model.standard.StandardVSwitchSetType;

public class StandardApiDescribeVSwitchesReplyMsg extends PageReplyMessage<StandardVSwitchSetType>{
	private static final long serialVersionUID = 1L;
	
	private StandardDescribeVSwitchesResponse vSwitches;

	@Override
	public void setList(List<StandardVSwitchSetType> list) {
		vSwitches = new StandardDescribeVSwitchesResponse();
		vSwitches.setvSwitch(list);
	}

	public StandardDescribeVSwitchesResponse getvSwitches() {
		return vSwitches;
	}

	public void setvSwitches(StandardDescribeVSwitchesResponse vSwitches) {
		this.vSwitches = vSwitches;
	}
}
