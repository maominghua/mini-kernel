package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDescribeVSwitchesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "网络Id", require=true)
	private String vpcId;
	@ApiModel(description = "交换机Id")
	private String vSwitchId;
	@ApiModel(description = "非VPC模式网络ID")
	private String networkId;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeVSwitchesReplyMsg.class;
	}

	public String getVpcId() {
		return vpcId;
	}

	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
}
