package com.gcloud.header.network.model.standard;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class StandardSecurityGroupItemType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全组Id")
	private String securityGroupId;
	@ApiModel(description = "安全组名称")
	private String securityGroupName;
	@ApiModel(description = "安全组描述")
	private String description;
	//TODO 缺vpcId、 creationTime
	
	public String getSecurityGroupId() {
		return securityGroupId;
	}
	public void setSecurityGroupId(String securityGroupId) {
		this.securityGroupId = securityGroupId;
	}
	public String getSecurityGroupName() {
		return securityGroupName;
	}
	public void setSecurityGroupName(String securityGroupName) {
		this.securityGroupName = securityGroupName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
