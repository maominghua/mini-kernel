package com.gcloud.header.network.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailVpcMsg  extends ApiMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description="专有网络ID", require=true)
	@NotBlank(message="::专有网络ID不能为空")
	private String vpcId;

	@Override
	public Class replyClazz() {
		return ApiDetailVpcReplyMsg.class;
	}

	public String getVpcId() {
		return vpcId;
	}

	public void setVpcId(String vpcId) {
		this.vpcId = vpcId;
	}

}
