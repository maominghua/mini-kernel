package com.gcloud.header.network.msg.api;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

public class DeleteSubnetMsg extends ApiMessage{
	@NotBlank(message = "0170301::子网ID不能为空")
	@ApiModel(description = "子网Id", require = true)
	private String subnetId;
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	public String getSubnetId() {
		return subnetId;
	}
	public void setSubnetId(String subnetId) {
		this.subnetId = subnetId;
	}
}
