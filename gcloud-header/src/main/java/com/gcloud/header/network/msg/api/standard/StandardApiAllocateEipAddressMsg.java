package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiAllocateEipAddressMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0050101::弹性公网实例Id不能为空")
	@ApiModel(description = "公网网络ID", require = true)
	private String networkId;
//	//仅支持阿里的，默认值为5
	private Integer bandwidth;

	@Override
	public Class replyClazz() {
		return StandardApiAllocateEipAddressReplyMsg.class;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public Integer getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(Integer bandwidth) {
		this.bandwidth = bandwidth;
	}
}
