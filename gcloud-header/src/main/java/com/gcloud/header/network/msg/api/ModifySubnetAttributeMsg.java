package com.gcloud.header.network.msg.api;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

public class ModifySubnetAttributeMsg extends ApiMessage{
	@NotBlank(message = "0170201::子网ID不能为空")
	@ApiModel(description = "子网Id", require = true)
	private String subnetId;
	@ApiModel(description = "子网名称", require = true)
	@NotBlank(message = "0170202::子网名称不能为空")
	private String subnetName;
	@ApiModel(description = "网关IP")
	private String gatewayIp;
	@ApiModel(description = "dns服务列表")
	private List<String> dnsNameServers;
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}
	public String getSubnetId() {
		return subnetId;
	}
	public void setSubnetId(String subnetId) {
		this.subnetId = subnetId;
	}
	public String getSubnetName() {
		return subnetName;
	}
	public void setSubnetName(String subnetName) {
		this.subnetName = subnetName;
	}
	public String getGatewayIp() {
		return gatewayIp;
	}
	public void setGatewayIp(String gatewayIp) {
		this.gatewayIp = gatewayIp;
	}
	public List<String> getDnsNameServers() {
		return dnsNameServers;
	}
	public void setDnsNameServers(List<String> dnsNameServers) {
		this.dnsNameServers = dnsNameServers;
	}
}
