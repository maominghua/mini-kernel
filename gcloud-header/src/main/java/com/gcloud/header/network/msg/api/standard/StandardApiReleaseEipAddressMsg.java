package com.gcloud.header.network.msg.api.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiReleaseEipAddressMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "弹性公网实例Id", require = true)
	@NotBlank(message = "0050401::弹性公网实例Id不能为空")
	private String allocationId;

	@Override
	public Class replyClazz() {
		return StandardApiReleaseEipAddressReplyMsg.class;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}
}
