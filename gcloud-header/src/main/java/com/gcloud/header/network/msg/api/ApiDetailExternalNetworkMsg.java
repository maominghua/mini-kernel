package com.gcloud.header.network.msg.api;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailExternalNetworkMsg extends ApiMessage{

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDetailExternalNetworkReplyMsg.class;
	}
	
	@ApiModel(description = "网络ID", require = true)
	@NotBlank(message = "::网络ID不能为空")
	private String networkId;

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
}
