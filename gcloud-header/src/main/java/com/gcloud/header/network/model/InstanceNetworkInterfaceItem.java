package com.gcloud.header.network.model;

public class InstanceNetworkInterfaceItem {
	private String instanceId;
	private String networkInterfaceItem;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getNetworkInterfaceItem() {
		return networkInterfaceItem;
	}
	public void setNetworkInterfaceItem(String networkInterfaceItem) {
		this.networkInterfaceItem = networkInterfaceItem;
	}
	
}
