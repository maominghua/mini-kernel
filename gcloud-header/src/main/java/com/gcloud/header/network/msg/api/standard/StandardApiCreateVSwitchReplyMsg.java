package com.gcloud.header.network.msg.api.standard;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiCreateVSwitchReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "交换机Id")
	private String vSwitchId;

	public String getvSwitchId() {
		return vSwitchId;
	}

	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
}
