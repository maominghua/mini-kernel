package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.network.model.standard.StandardDescribeEipAddressesResponse;
import com.gcloud.header.network.model.standard.StandardEipAddressSetType;

public class StandardApiDescribeEipAddressesReplyMsg extends PageReplyMessage<StandardEipAddressSetType>{
	private static final long serialVersionUID = 1L;
	
	private StandardDescribeEipAddressesResponse eipAddresses;

	@Override
	public void setList(List<StandardEipAddressSetType> list) {
		eipAddresses = new StandardDescribeEipAddressesResponse();
		eipAddresses.setEipAddress(list);
	}

	public StandardDescribeEipAddressesResponse getEipAddresses() {
		return eipAddresses;
	}

	public void setEipAddresses(StandardDescribeEipAddressesResponse eipAddresses) {
		this.eipAddresses = eipAddresses;
	}
}
