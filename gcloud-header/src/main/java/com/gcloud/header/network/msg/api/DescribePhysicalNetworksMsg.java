package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiPageMessage;

public class DescribePhysicalNetworksMsg extends ApiPageMessage{
	
	private String networkType;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return DescribePhysicalNetworksReplyMsg.class;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

}
