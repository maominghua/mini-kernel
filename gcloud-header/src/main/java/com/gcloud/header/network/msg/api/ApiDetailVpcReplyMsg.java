package com.gcloud.header.network.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.DetailVpcResponse;

public class ApiDetailVpcReplyMsg  extends ApiReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="vpc详情")
	private DetailVpcResponse detailVpc;

	public DetailVpcResponse getDetailVpc() {
		return detailVpc;
	}

	public void setDetailVpc(DetailVpcResponse detailVpc) {
		this.detailVpc = detailVpc;
	}
	

}
