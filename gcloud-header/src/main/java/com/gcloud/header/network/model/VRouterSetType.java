package com.gcloud.header.network.model;

import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;

import java.io.Serializable;

public class VRouterSetType implements Serializable {
    @ApiModel(description = "路由器Id")
    @TableField("id")
    private String vRouterId;
    @ApiModel(description = "路由器名称")
    @TableField("name")
    private String vRouterName;
    @ApiModel(description = "区域Id")
    private String regionId = ControllerProperty.REGION_ID;
    @ApiModel(description = "状态")
    private String status;
    @ApiModel(description = "中文状态")
    private String cnStatus;
    private String subnets;
    
    @ApiModel(description = "外网网络ID")
    @TableField("external_gateway_network_id")
    private String networkId;
    @ApiModel(description = "外网网络名称")
    private String networkName;

    public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getvRouterId() {
        return vRouterId;
    }

    public void setvRouterId(String vRouterId) {
        this.vRouterId = vRouterId;
    }

    public String getvRouterName() {
        return vRouterName;
    }

    public void setvRouterName(String vRouterName) {
        this.vRouterName = vRouterName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubnets() {
        return subnets;
    }

    public void setSubnets(String subnets) {
        this.subnets = subnets;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

	public String getNetworkName() {
		return networkName;
	}

	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}
}
