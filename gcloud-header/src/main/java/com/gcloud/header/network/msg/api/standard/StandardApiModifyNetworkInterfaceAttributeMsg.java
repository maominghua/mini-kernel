package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiModifyNetworkInterfaceAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "网卡ID不能为空", require = true)
    @NotBlank(message = "0080201::端口ID不能为空")
    private String networkInterfaceId;
    @ApiModel(description = "安全组ID列表")
    private List<String> securityGroupIds;
    @ApiModel(description = "网卡名称")
    @Length(max = 255, message = "0080203::名称长度不能大于255")
    private String networkInterfaceName;
    //TODO 缺description

	@Override
	public Class replyClazz() {
		return StandardApiModifyNetworkInterfaceAttributeReplyMsg.class;
	}

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}

	public List<String> getSecurityGroupIds() {
		return securityGroupIds;
	}

	public void setSecurityGroupIds(List<String> securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}

	public String getNetworkInterfaceName() {
		return networkInterfaceName;
	}

	public void setNetworkInterfaceName(String networkInterfaceName) {
		this.networkInterfaceName = networkInterfaceName;
	}
}
