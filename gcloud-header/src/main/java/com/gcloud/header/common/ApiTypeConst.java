package com.gcloud.header.common;

public class ApiTypeConst {
	public static final String CREATE = "create"; //创建类api接口
	public static final String QUERY = "query"; //查询类api接口
	public static final String OPERATE = "operate"; //操作类api接口
}
