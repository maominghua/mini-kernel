package com.gcloud.header.image.msg.node;

import com.gcloud.header.NodeMessage;

public class DownloadImageMsg  extends NodeMessage {
	private String imageId;
	private String provider;//glance、gcloud
	private String imageStroageType;//file\rbd
	private String target;//节点名、rbd池名、vg名
	private String targetType;//node\vg\rbd
	private String imagePath;//image 源路径\rbd池名

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getImageStroageType() {
		return imageStroageType;
	}

	public void setImageStroageType(String imageStroageType) {
		this.imageStroageType = imageStroageType;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

}
