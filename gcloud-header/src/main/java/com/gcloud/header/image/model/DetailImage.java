package com.gcloud.header.image.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

public class DetailImage implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "镜像ID")
	private String id;
	
	@ApiModel(description = "镜像名称")
	private String name;
	
	@ApiModel(description = "架构")
	private String architeture;
	
	@ApiModel(description = "中文状态")
	private String cnStatus;
	
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	
	@ApiModel(description = "描述")
	private String description;
	
	@ApiModel(description = "")
	private Boolean isTask;
	
	@ApiModel(description = "系统类型")
	private String osType;
	
	@ApiModel(description = "镜像大小")
	private Long size;
	
	@ApiModel(description = "状态")
	private String status;
	
	public String getArchiteture() {
		return architeture;
	}
	public void setArchiteture(String architeture) {
		this.architeture = architeture;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Boolean getIsTask() {
		return isTask;
	}
	public void setIsTask(Boolean isTask) {
		this.isTask = isTask;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOsType() {
		return osType;
	}
	public void setOsType(String osType) {
		this.osType = osType;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
