package com.gcloud.header.image.msg.api;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailImageMsg extends ApiMessage{

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDetailImageReplyMsg.class;
	}
	
	@ApiModel(description = "镜像ID", require = true)
	@NotBlank(message = "::镜像ID不能为空")
	private String imageId;

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
}
