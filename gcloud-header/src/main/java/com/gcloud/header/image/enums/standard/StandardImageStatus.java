package com.gcloud.header.image.enums.standard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.gcloud.header.image.enums.ImageStatus;

public enum StandardImageStatus {
	//TODO 响应返回状态的可能有问题
	AVALIABLE("Available", "可使用", Arrays.asList(ImageStatus.ACTIVE)),
	UNAVAILABLE("UnAvailable", "不可用", Arrays.asList(ImageStatus.DEACTIVATED)),
//	CREATING("Creating", "创建中", Arrays.asList(ImageStatus.QUEUED)),
//	CREATEFAILED("CreateFailed", "创建失败"),
	DELETING("Deleting", "删除中", Arrays.asList(ImageStatus.PENDING_DELETE));
//	USING("Using", "使用中"),
//	SYNCING("Syncing", "同步中"),
//	IMPORTING("Importing", "导入中", Arrays.asList(ImageStatus.QUEUED));
	
//	UNRECOGNIZED("未知"),
//    QUEUED("队列中"),
//    SAVING("保存中"),
//    ACTIVE("可用"),
//    DEACTIVATED("不可用"),
//    KILLED("终止"),
//    DELETED("已删除"),
//    PENDING_DELETE("删除中");
	
	private String value;
    private String cnName;
    private List<ImageStatus> gcStatus;
    
    private StandardImageStatus(String value, String cnName, List<ImageStatus> gcStatus) {
    	this.value = value;
    	this.cnName = cnName;
    	this.gcStatus = gcStatus;
    }
    
    public List<String> getGcStatusValues(){
        if(gcStatus == null){
            return null;
        }

        List<String> resultStatus = new ArrayList<>();
        gcStatus.forEach(s -> resultStatus.add(s.value()));
        return resultStatus;
    }
    
    public static String standardStatus(String gcStatusStr){
        if(StringUtils.isBlank(gcStatusStr)){
            return gcStatusStr;
        }

        for(StandardImageStatus status : StandardImageStatus.values()){
            if(status.gcStatus == null || status.getGcStatus().size() == 0){
                continue;
            }
            for(ImageStatus gcStatus : status.getGcStatus()){
                if(gcStatus.value().equals(gcStatusStr)){
                    return status.getValue();
                }
            }
        }
        return null;
    }
    
    public static StandardImageStatus value(String value){
        return Arrays.stream(StandardImageStatus.values()).filter(s -> s.getValue().equals(value)).findFirst().orElse(null);
    }

	public String getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}

	public List<ImageStatus> getGcStatus() {
		return gcStatus;
	}
}
