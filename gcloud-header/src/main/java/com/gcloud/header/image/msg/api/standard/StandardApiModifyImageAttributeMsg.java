package com.gcloud.header.image.msg.api.standard;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiModifyImageAttributeMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "0090201::镜像ID不能为空")
    @ApiModel(description = "镜像ID")
    private String imageId;
    @Length(max = 255, message = "0090203::名称长度不能大于255")
    @ApiModel(description = "镜像名称")
    private String imageName;
	
	@Override
	public Class replyClazz() {
		return StandardApiModifyImageAttributeReplyMsg.class;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
}
