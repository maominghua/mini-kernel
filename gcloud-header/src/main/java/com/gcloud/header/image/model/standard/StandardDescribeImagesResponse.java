package com.gcloud.header.image.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeImagesResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<StandardImageType> image;

	public List<StandardImageType> getImage() {
		return image;
	}

	public void setImage(List<StandardImageType> image) {
		this.image = image;
	}
}
