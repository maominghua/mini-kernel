package com.gcloud.header.image.msg.api.standard;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDescribeImagesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "镜像 ID", require = false)
    private String imageId;
    
    @ApiModel(description = "镜像名称", require = false)
    private String imageName;
    
    @ApiModel(description = "镜像状态", require = false)
    private String status;
    
    @ApiModel(description = "是否禁用", require = false)
    private Boolean disable;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeImagesReplyMsg.class;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getDisable() {
		return disable;
	}

	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
}
