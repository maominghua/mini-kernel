package com.gcloud.header.image.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDeleteImageMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "0090301::镜像ID不能为空")
    @ApiModel(description = "镜像ID")
    private String imageId;
	
	@Override
	public Class replyClazz() {
		return StandardApiDeleteImageReplyMsg.class;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
}
