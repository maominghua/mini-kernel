package com.gcloud.header.image.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.image.model.DetailImage;

public class ApiDetailImageReplyMsg extends ApiReplyMessage{

	private static final long serialVersionUID = 1L;
	
	private DetailImage detailImage;

	public DetailImage getDetailImage() {
		return detailImage;
	}

	public void setDetailImage(DetailImage detailImage) {
		this.detailImage = detailImage;
	}
}
