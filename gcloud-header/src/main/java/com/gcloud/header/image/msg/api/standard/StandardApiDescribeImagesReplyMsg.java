package com.gcloud.header.image.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.image.model.standard.StandardDescribeImagesResponse;
import com.gcloud.header.image.model.standard.StandardImageType;

public class StandardApiDescribeImagesReplyMsg extends PageReplyMessage<StandardImageType>{
	private static final long serialVersionUID = 1L;
	
	private StandardDescribeImagesResponse images;

	@Override
	public void setList(List<StandardImageType> list) {
		images = new StandardDescribeImagesResponse();
		images.setImage(list);
	}

	public StandardDescribeImagesResponse getImages() {
		return images;
	}

	public void setImages(StandardDescribeImagesResponse images) {
		this.images = images;
	}
}