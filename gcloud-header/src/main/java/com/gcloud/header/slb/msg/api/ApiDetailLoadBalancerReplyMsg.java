package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.slb.model.DetailLoadBalancerResponse;

public class ApiDetailLoadBalancerReplyMsg  extends ApiReplyMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="vpc详情")
	private DetailLoadBalancerResponse detailLoadBalancer;
	public DetailLoadBalancerResponse getDetailLoadBalancer() {
		return detailLoadBalancer;
	}
	public void setDetailLoadBalancer(DetailLoadBalancerResponse detailLoadBalancer) {
		this.detailLoadBalancer = detailLoadBalancer;
	}
}
