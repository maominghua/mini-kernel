package com.gcloud.header.slb.enums;

import java.util.Arrays;

import com.gcloud.header.network.enums.NetworkStatus;
import com.google.common.base.CaseFormat;

public enum LbProvisioningStatus {

    ACTIVE("激活"),
    DOWN("失活"),
    CREATED("已创建"),
    PENDING_CREATE("创建中"),
    PENDING_UPDATE("升级中"),
    PENDING_DELETE("删除中"),
    INACTIVE("失活"),
    ERROR("错误");


    private String cnName;

    LbProvisioningStatus(String cnName) {
        this.cnName = cnName;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public String getCnName() {
        return cnName;
    }
    
    public static String getCnName(String enName) {
    	LbProvisioningStatus status = Arrays.stream(LbProvisioningStatus.values()).filter(state -> state.value().equals(enName)).findFirst().orElse(null);
		return status != null ? status.getCnName() : null;
	}

}
