package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

public class ApiLoadBalancersStatisticsReplyMsg extends ApiReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "总数量")
	private int allNum;
	public int getAllNum() {
		return allNum;
	}
	public void setAllNum(int allNum) {
		this.allNum = allNum;
	}

}
