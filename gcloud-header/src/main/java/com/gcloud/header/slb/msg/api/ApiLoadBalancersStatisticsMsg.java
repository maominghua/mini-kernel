package com.gcloud.header.slb.msg.api;

import com.gcloud.header.ApiMessage;

public class ApiLoadBalancersStatisticsMsg extends ApiMessage{

	@Override
	public Class replyClazz() {
		return ApiLoadBalancersStatisticsReplyMsg.class;
	}

}
