package com.gcloud.header.storage.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeSnapshotsResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<StandardSnapshotType> snapshot;

	public List<StandardSnapshotType> getSnapshot() {
		return snapshot;
	}

	public void setSnapshot(List<StandardSnapshotType> snapshot) {
		this.snapshot = snapshot;
	}
}
