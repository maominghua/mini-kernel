package com.gcloud.header.storage.model.standard;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StandardSnapshotType implements Serializable{
	private static final long serialVersionUID = 1L;
	
    private String snapshotId;
    private String snapshotName;
    private String description;
    private String sourceDiskId;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date creationTime;
    private String status;
    private String sourceDiskSize;
	public String getSnapshotId() {
		return snapshotId;
	}
	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
	public String getSnapshotName() {
		return snapshotName;
	}
	public void setSnapshotName(String snapshotName) {
		this.snapshotName = snapshotName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSourceDiskId() {
		return sourceDiskId;
	}
	public void setSourceDiskId(String sourceDiskId) {
		this.sourceDiskId = sourceDiskId;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSourceDiskSize() {
		return sourceDiskSize;
	}
	public void setSourceDiskSize(String sourceDiskSize) {
		this.sourceDiskSize = sourceDiskSize;
	}
}
