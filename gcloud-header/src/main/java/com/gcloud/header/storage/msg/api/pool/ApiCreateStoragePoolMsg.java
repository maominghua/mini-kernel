
package com.gcloud.header.storage.msg.api.pool;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;

public class ApiCreateStoragePoolMsg extends ApiMessage {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiCreateStoragePoolReplyMsg.class;
    }

    @NotBlank(message = StorageErrorCodes.INPUT_POOL_NAME_ERROR)
    @ApiModel(description="存储池显示名称")
    private String displayName;
    @NotNull(message = StorageErrorCodes.INPUT_PROVIDER_ERROR)
    @ApiModel(description="供应商")
    private Integer provider;
    @NotBlank(message = StorageErrorCodes.INPUT_STORAGE_TYPE_ERROR)
    @ApiModel(description="存储类型")
    private String storageType;
    @NotBlank(message = StorageErrorCodes.INPUT_POOL_NAME_ERROR)
    @ApiModel(description="存储池名称")
    private String poolName;
    @NotBlank(message = StorageErrorCodes.INPUT_ZONE_ID_ERROR)
    @ApiModel(description="可用区ID")
    private String zoneId;
    @NotBlank(message = StorageErrorCodes.INPUT_DISK_CATEGORY_ERROR)
    @ApiModel(description="存储类型ID")
    private String categoryId;
    @ApiModel(description="所在节点名称")
    private String hostname;
    @ApiModel(description="驱动类型")
    private String driver;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getProvider() {
        return provider;
    }

    public void setProvider(Integer provider) {
        this.provider = provider;
    }

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

}
