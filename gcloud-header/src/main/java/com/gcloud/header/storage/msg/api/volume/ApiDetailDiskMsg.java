package com.gcloud.header.storage.msg.api.volume;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailDiskMsg extends ApiMessage{

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDetailDiskReplyMsg.class;
	}
	
	@ApiModel(description = "磁盘ID", require = true)
	@NotBlank(message = "::磁盘ID不能为空")
	private String diskId;

	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}
}
