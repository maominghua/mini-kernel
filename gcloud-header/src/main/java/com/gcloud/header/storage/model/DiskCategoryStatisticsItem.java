package com.gcloud.header.storage.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class DiskCategoryStatisticsItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "磁盘类型ID")
	private String id;
	@ApiModel(description = "磁盘类型名称")
	private String name;
	@ApiModel(description = "数量")
	private int countNum;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCountNum() {
		return countNum;
	}
	public void setCountNum(int countNum) {
		this.countNum = countNum;
	}
}
