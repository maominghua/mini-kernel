package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.storage.msg.api.volume.standard.StandardApiDescribeDisksMsg;

/**
 * Created by yaowj on 2018/9/29.
 */
public class ApiDescribeDisksMsg extends StandardApiDescribeDisksMsg {

    private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiDescribeDisksReplyMsg.class;
    }

	
}
