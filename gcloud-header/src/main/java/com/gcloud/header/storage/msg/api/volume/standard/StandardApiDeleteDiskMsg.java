package com.gcloud.header.storage.msg.api.volume.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;

public class StandardApiDeleteDiskMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "磁盘ID", require = true)
    @NotBlank(message = StorageErrorCodes.INPUT_DISK_ID_ERROR)
    private String diskId;
	
	@Override
	public Class replyClazz() {
		return StandardApiDeleteDiskReplyMsg.class;
	}

	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}
}
