package com.gcloud.header.storage.msg.api.volume.standard;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.storage.model.standard.StandardDescribeDisksResponse;
import com.gcloud.header.storage.model.standard.StandardDiskItemType;

import java.util.List;

/**
 * Created by yaowj on 2018/9/29.
 */
public class StandardApiDescribeDisksReplyMsg extends PageReplyMessage<StandardDiskItemType> {

    private static final long serialVersionUID = 1L;

    private StandardDescribeDisksResponse disks;

    @Override
    public void setList(List<StandardDiskItemType> list) {
        disks = new StandardDescribeDisksResponse();
        disks.setDisk(list);
    }

    public StandardDescribeDisksResponse getDisks() {
        return disks;
    }

    public void setDisks(StandardDescribeDisksResponse disks) {
        this.disks = disks;
    }
}
