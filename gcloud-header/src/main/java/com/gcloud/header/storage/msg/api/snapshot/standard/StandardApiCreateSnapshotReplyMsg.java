package com.gcloud.header.storage.msg.api.snapshot.standard;

import com.gcloud.header.ApiReplyMessage;

public class StandardApiCreateSnapshotReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private String snapshotId;

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
}
