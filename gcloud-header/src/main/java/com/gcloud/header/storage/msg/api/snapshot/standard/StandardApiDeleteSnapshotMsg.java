package com.gcloud.header.storage.msg.api.snapshot.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.storage.StorageErrorCodes;

public class StandardApiDeleteSnapshotMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@NotBlank(message = StorageErrorCodes.INPUT_SNAPSHOT_ID_ERROR)
    private String snapshotId;
	
	@Override
	public Class replyClazz() {
		return StandardApiDeleteSnapshotReplyMsg.class;
	}

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}
}
