
package com.gcloud.header.storage.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class DiskCategoryModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "磁盘类型ID")
    private String id;
    
    @ApiModel(description = "磁盘类型名称")
    private String name;
    
    @ApiModel(description = "磁盘类型大小最小值")
    private Integer minSize;
    
    @ApiModel(description = "磁盘类型大小最大值")
    private Integer maxSize;
    
    @ApiModel(description = "磁盘类型可用区，存储池相关信息")
    private List<DiskCategoryZonePool> associateInfo;
    
    @ApiModel(description = "磁盘类型是否可用")
    private boolean enabled;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMinSize() {
        return minSize;
    }

    public void setMinSize(Integer minSize) {
        this.minSize = minSize;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

	public List<DiskCategoryZonePool> getAssociateInfo() {
		return associateInfo;
	}

	public void setAssociateInfo(List<DiskCategoryZonePool> associateInfo) {
		this.associateInfo = associateInfo;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
