package com.gcloud.header.storage.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;

public class DetailDisk implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "磁盘ID")
	private String id;
	
	@ApiModel(description = "磁盘类型ID")
	private String category;
	
	@ApiModel(description = "磁盘名称")
	private String name;
	
	@ApiModel(description ="磁盘大小")
	private Integer size;
	
	@ApiModel(description = "磁盘类型名称")
	private String cnCategory;
	
	@ApiModel(description = "中文状态")
	private String cnStatus;
	
	@ApiModel(description = "磁盘类型中文")
	private String cnType;
	
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	
	@ApiModel(description = "描述")
	private String description;
	
	@ApiModel(description = "")
	private String device;
	
	@ApiModel(description = "挂载实例ID")
	private String instanceId;
	
	@ApiModel(description = "挂载实例名称")
	private String instanceName;
	
	@ApiModel(description = "")
	private Boolean isTask;
	
	@ApiModel(description = "磁盘状态")
	private String status;
	
	@ApiModel(description = "磁盘类型")
	private String type;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	
	@ApiModel(description = "可用区名称")
	private String zoneName;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCnCategory() {
		return cnCategory;
	}
	public void setCnCategory(String cnCategory) {
		this.cnCategory = cnCategory;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public String getCnType() {
		return cnType;
	}
	public void setCnType(String cnType) {
		this.cnType = cnType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public Boolean getIsTask() {
		return isTask;
	}
	public void setIsTask(Boolean isTask) {
		this.isTask = isTask;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
}
