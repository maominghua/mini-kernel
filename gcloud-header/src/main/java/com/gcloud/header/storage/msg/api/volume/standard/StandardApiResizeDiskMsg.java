package com.gcloud.header.storage.msg.api.volume.standard;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;

public class StandardApiResizeDiskMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "磁盘ID")
    @NotBlank(message = StorageErrorCodes.INPUT_DISK_ID_ERROR)
    private String diskId;
    @ApiModel(description = "磁盘大小。单位:GB")
    @NotNull(message = StorageErrorCodes.INPUT_DISK_SIZE_ERROR)
    private Integer newSize;
    
	@Override
	public Class replyClazz() {
		return StandardApiResizeDiskReplyMsg.class;
	}

	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}

	public Integer getNewSize() {
		return newSize;
	}

	public void setNewSize(Integer newSize) {
		this.newSize = newSize;
	}
}
