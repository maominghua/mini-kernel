package com.gcloud.header.storage.msg.api.pool;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiAssociatePoolZoneMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiAssociatePoolZoneReplyMsg.class;
	}
	
	@ApiModel(description = "存储池ID", require = true)
	@NotBlank(message = "::存储池ID不能为空")
	private String poolId;
	
	@ApiModel(description = "可用区ID", require = true)
	@NotBlank(message = "::可用区ID不能为空")
	private String zoneId;

	public String getPoolId() {
		return poolId;
	}

	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
}
