package com.gcloud.header.storage.enums;

import java.util.Arrays;

/**
 * Created by yaowj on 2018/10/8.
 */
public enum  DiskType {

    SYSTEM("system", "系统盘"), DATA("data", "数据盘");

    private String value;
    private String cnName;


    DiskType(String value, String cnName) {
        this.value = value;
        this.cnName = cnName;
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }
    
    public static String getCnName(String value) {
    	DiskType enu =  Arrays.stream(DiskType.values()).filter(type -> type.getValue().equals(value)).findFirst().orElse(null);
		return enu != null ? enu.getCnName() : null;
	}

}
