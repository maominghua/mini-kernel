package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.storage.model.DetailDisk;

public class ApiDetailDiskReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private DetailDisk detailDisk;

	public DetailDisk getDetailDisk() {
		return detailDisk;
	}

	public void setDetailDisk(DetailDisk detailDisk) {
		this.detailDisk = detailDisk;
	}
}
