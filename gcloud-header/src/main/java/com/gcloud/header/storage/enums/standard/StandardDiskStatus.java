package com.gcloud.header.storage.enums.standard;

import com.gcloud.header.storage.enums.VolumeStatus;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum StandardDiskStatus {

    AVAILABLE("Available", "可使用", Arrays.asList(VolumeStatus.AVAILABLE)),
    ATTACHING("Attaching", "挂载中", Arrays.asList(VolumeStatus.ATTACHING)),
    TASKING("Tasking", "任务中", Arrays.asList(VolumeStatus.CREATING_BACKUP, VolumeStatus.RESIZING, VolumeStatus.RESTORING_BACKUP, VolumeStatus.UPLOADING, VolumeStatus.DOWNLOADING)),
    ERROR("Error", "错误", Arrays.asList(VolumeStatus.ERROR, VolumeStatus.ERROR_DELETING, VolumeStatus.ERROR_RESTORING, VolumeStatus.UNRECOGNIZED)),
    REINITING("Reiniting", "初始化中", null),
    DELETING("Deleting", "删除中", Arrays.asList(VolumeStatus.DELETING)),
    INUSED("In_use", "已挂载", Arrays.asList(VolumeStatus.IN_USE)),
    DETACHING("Detaching", "卸载中", Arrays.asList(VolumeStatus.DETACHING)),
    CREATING("Creating", "创建中", Arrays.asList(VolumeStatus.CREATING)),
    DELETED("Deleted", "已删除", null);

    private String value;
    private String cnName;
    private List<VolumeStatus> gcStatus;

    StandardDiskStatus(String value, String cnName, List<VolumeStatus> gcStatus) {
        this.value = value;
        this.cnName = cnName;
        this.gcStatus = gcStatus;
    }

    public List<String> getGcStatusValues(){
        if(gcStatus == null){
            return null;
        }

        List<String> resultStatus = new ArrayList<>();
        gcStatus.forEach(s -> resultStatus.add(s.value()));
        return resultStatus;
    }

    public static String standardStatus(String gcStatusStr){

        if(StringUtils.isBlank(gcStatusStr)){
            return gcStatusStr;
        }

        for(StandardDiskStatus status : StandardDiskStatus.values()){
            if(status.gcStatus == null || status.getGcStatus().size() == 0){
                continue;
            }
            for(VolumeStatus gcStatus : status.getGcStatus()){
                if(gcStatus.value().equals(gcStatusStr)){
                    return status.getValue();
                }
            }
        }

        return null;
    }

    public static StandardDiskStatus value(String value){
        return Arrays.stream(StandardDiskStatus.values()).filter(s -> s.getValue().equals(value)).findFirst().orElse(null);
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }

    public List<VolumeStatus> getGcStatus() {
        return gcStatus;
    }
}
