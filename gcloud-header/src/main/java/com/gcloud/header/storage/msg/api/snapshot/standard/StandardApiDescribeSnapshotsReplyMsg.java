package com.gcloud.header.storage.msg.api.snapshot.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.storage.model.standard.StandardDescribeSnapshotsResponse;
import com.gcloud.header.storage.model.standard.StandardSnapshotType;

public class StandardApiDescribeSnapshotsReplyMsg extends PageReplyMessage<StandardSnapshotType>{
	private static final long serialVersionUID = 1L;
	
	private StandardDescribeSnapshotsResponse snapshots;

	@Override
	public void setList(List<StandardSnapshotType> list) {
		snapshots = new StandardDescribeSnapshotsResponse();
		snapshots.setSnapshot(list);
	}

	public StandardDescribeSnapshotsResponse getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(StandardDescribeSnapshotsResponse snapshots) {
		this.snapshots = snapshots;
	}
}
