package com.gcloud.header.storage.msg.api.pool;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiEnableDiskCategoryMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "磁盘类型ID", require = true)
	@NotBlank(message = "::磁盘类型ID不能为空")
	private String diskCategoryId;
	
	@ApiModel(description = "是否启用磁盘类型，true:启用，false:禁用， 默认为true")
	private boolean enable = true;

	@Override
	public Class replyClazz() {
		return ApiEnableDiskCategoryReplyMsg.class;
	}

	public String getDiskCategoryId() {
		return diskCategoryId;
	}
	
	public void setDiskCategoryId(String diskCategoryId) {
		this.diskCategoryId = diskCategoryId;
	}
	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
}
