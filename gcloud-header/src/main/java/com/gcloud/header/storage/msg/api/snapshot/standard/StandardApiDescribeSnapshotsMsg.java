package com.gcloud.header.storage.msg.api.snapshot.standard;

import com.gcloud.header.ApiPageMessage;

public class StandardApiDescribeSnapshotsMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;
	
	private String diskId;
    private String snapshotIds;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeSnapshotsReplyMsg.class;
	}

	public String getDiskId() {
		return diskId;
	}

	public void setDiskId(String diskId) {
		this.diskId = diskId;
	}

	public String getSnapshotIds() {
		return snapshotIds;
	}

	public void setSnapshotIds(String snapshotIds) {
		this.snapshotIds = snapshotIds;
	}
}
