package com.gcloud.header.storage.model.standard;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yaowj on 2018/9/29.
 */
public class StandardDescribeDisksResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<StandardDiskItemType> disk;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<StandardDiskItemType> getDisk() {
        return disk;
    }

    public void setDisk(List<StandardDiskItemType> disk) {
        this.disk = disk;
    }
}
