package com.gcloud.header.storage.model;

import com.gcloud.header.storage.model.standard.StandardDiskItemType;

import java.io.Serializable;

/**
 * Created by yaowj on 2018/9/29.
 */
public class DiskItemType extends StandardDiskItemType implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String cnStatus;
    
    private String zoneName;
    
    private String instanceName;

	public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
}
