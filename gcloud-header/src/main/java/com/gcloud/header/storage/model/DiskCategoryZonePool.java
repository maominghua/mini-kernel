package com.gcloud.header.storage.model;

import java.io.Serializable;

public class DiskCategoryZonePool implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String zoneId;
	private String zoneName;
	private String poolId;
	private String poolName;
	
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
	public String getPoolId() {
		return poolId;
	}
	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}
	public String getPoolName() {
		return poolName;
	}
	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}
}
