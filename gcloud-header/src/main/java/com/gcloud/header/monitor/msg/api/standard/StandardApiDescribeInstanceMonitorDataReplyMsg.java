package com.gcloud.header.monitor.msg.api.standard;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.monitor.model.standard.StandardDescribeInstanceMonitorDataResponse;

public class StandardApiDescribeInstanceMonitorDataReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private StandardDescribeInstanceMonitorDataResponse monitorData;

	public StandardDescribeInstanceMonitorDataResponse getMonitorData() {
		return monitorData;
	}

	public void setMonitorData(StandardDescribeInstanceMonitorDataResponse monitorData) {
		this.monitorData = monitorData;
	}
}
