package com.gcloud.header.monitor.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeInstanceMonitorDataResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<StandardInstanceMonitorDataType> instanceMonitorData;

	public List<StandardInstanceMonitorDataType> getInstanceMonitorData() {
		return instanceMonitorData;
	}

	public void setInstanceMonitorData(List<StandardInstanceMonitorDataType> instanceMonitorData) {
		this.instanceMonitorData = instanceMonitorData;
	}
}
