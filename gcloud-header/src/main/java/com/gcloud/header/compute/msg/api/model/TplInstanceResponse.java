package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class TplInstanceResponse implements Serializable{
	@ApiModel(description = "云服务器")
	private List<TplInstanceItem> instance;

	public List<TplInstanceItem> getInstance() {
		return instance;
	}

	public void setInstance(List<TplInstanceItem> instance) {
		this.instance = instance;
	}

}
