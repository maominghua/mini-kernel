package com.gcloud.header.compute.msg.api.model.standard;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.vm.zone.AvailableResources;

public class StandardZoneType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	@ApiModel(description = "可用区名字")
    private String localName;
	@ApiModel(description = "状态，是否可用")
    private Boolean status;
	@ApiModel(description = "中文状态")
    private String cnStatus;
	@ApiModel(description = "可用资源的合集")
    private AvailableResources availableResources;
    
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getLocalName() {
		return localName;
	}
	public void setLocalName(String localName) {
		this.localName = localName;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public AvailableResources getAvailableResources() {
		return availableResources;
	}
	public void setAvailableResources(AvailableResources availableResources) {
		this.availableResources = availableResources;
	}
}
