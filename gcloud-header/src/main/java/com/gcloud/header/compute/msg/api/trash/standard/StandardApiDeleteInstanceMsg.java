package com.gcloud.header.compute.msg.api.trash.standard;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDeleteInstanceMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

    @ApiModel(description = "云服务器ID", require = true)
    @NotBlank(message = "0010801::云服务器ID不能为空")
    private String instanceId;
    
	@Override
	public Class replyClazz() {
		return StandardApiDeleteInstanceReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
