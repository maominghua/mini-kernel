package com.gcloud.header.compute.msg.api.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeInstanceTypesResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<StandardInstanceTypeItemType> instanceType;

	public List<StandardInstanceTypeItemType> getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(List<StandardInstanceTypeItemType> instanceType) {
		this.instanceType = instanceType;
	}
}
