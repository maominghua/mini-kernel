package com.gcloud.header.compute.msg.api.vm.base.standard;

import com.gcloud.header.ApiPageMessage;

public class StandardApiDescribeInstanceTypesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeInstanceTypesReplyMsg.class;
	}

}
