package com.gcloud.header.compute.enums;

import java.util.Arrays;

public enum  NetProtocol {

    TCP, UDP, ICMP;

    public String value(){
        return name().toUpperCase();
    }

    public static NetProtocol value(String value){
        return Arrays.stream(NetProtocol.values()).filter(p -> p.value().equals(value)).findFirst().orElse(null);
    }

}
