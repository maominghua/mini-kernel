package com.gcloud.header.compute.msg.api.vm.base.standard;

import java.util.List;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDescribeInstancesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeInstancesReplyMsg.class;
	}
	
	@ApiModel(description="实例名称")
	private String instanceName;
	@ApiModel(description="实例状态")
	private String status;
	@ApiModel(description="可用区ID")
	private String zoneId;
	@ApiModel(description="实例ID列表")
	private List<String> instanceIds;
	//云盘挂载时，搜索虚拟机列表时用
	@ApiModel(description="挂载云盘ID")
	private String attachDiskId;

	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public List<String> getInstanceIds() {
		return instanceIds;
	}
	public void setInstanceIds(List<String> instanceIds) {
		this.instanceIds = instanceIds;
	}
	public String getAttachDiskId() {
		return attachDiskId;
	}
	public void setAttachDiskId(String attachDiskId) {
		this.attachDiskId = attachDiskId;
	}
}
