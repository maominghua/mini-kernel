package com.gcloud.header.compute.msg.api.vm.base.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiModifyInstanceSpecMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "云服务器ID", require = true)
	@NotBlank(message = "0011301::云服务器ID不能为空")
	private String instanceId;
	@ApiModel(description = "实例类型ID", require = true)
	@NotBlank(message = "0011302::实例类型ID不能为空")
	private String instanceType;
	
	@Override
	public Class replyClazz() {
		return StandardApiModifyInstanceSpecReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
}
