package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class TplInstanceItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "别名")
	private String alias;
	@ApiModel(description = "云服务器ID")
	private String instanceId;
	@ApiModel(description = "云服务器网卡信息集合")
	private TplNetworkInterfaceItems networkInterfaceItems;
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public TplNetworkInterfaceItems getNetworkInterfaceItems() {
		return networkInterfaceItems;
	}
	public void setNetworkInterfaceItems(TplNetworkInterfaceItems networkInterfaceItems) {
		this.networkInterfaceItems = networkInterfaceItems;
	}

}
