package com.gcloud.header.compute.enums;

import java.util.Arrays;

public enum ZoneState {
	ENABLE(true, "可用"),
	DISABLE(false, "不可用");
	
	private boolean value;
	private String cnName;
	
	private ZoneState(boolean value, String cnName) {
		this.value = value;
		this.cnName = cnName;
	}

	public boolean getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}
	
	public static String getCnName(boolean value) {
		ZoneState zoneState = Arrays.stream(ZoneState.values()).filter(state -> state.getValue() == value).findFirst().orElse(null);
		return zoneState != null ? zoneState.getCnName() : null;
	}
}
