package com.gcloud.header.compute.enums;

import java.util.Arrays;

public enum DeviceType {
	NETCARD;
	
	public String value(){
        return name().toLowerCase();
    }

    public static DeviceType value(String value){
        return Arrays.stream(DeviceType.values()).filter(p -> p.value().equals(value)).findFirst().orElse(null);
    }
}
