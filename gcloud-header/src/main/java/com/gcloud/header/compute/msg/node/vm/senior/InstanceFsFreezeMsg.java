package com.gcloud.header.compute.msg.node.vm.senior;

import com.gcloud.header.NodeMessage;

public class InstanceFsFreezeMsg extends NodeMessage{
	private String instanceId;
	private String fsfreezeType;
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getFsfreezeType() {
		return fsfreezeType;
	}
	public void setFsfreezeType(String fsfreezeType) {
		this.fsfreezeType = fsfreezeType;
	}
	
}
