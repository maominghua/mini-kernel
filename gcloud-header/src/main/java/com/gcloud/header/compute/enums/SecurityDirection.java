package com.gcloud.header.compute.enums;

import java.util.Arrays;

public enum SecurityDirection {
    EGRESS,
    INGRESS;

    public static SecurityDirection value(String value){
        return value == null || "".equals(value) ? null : Arrays.stream(SecurityDirection.values()).filter(d -> d.value().equals(value)).findFirst().orElse(null);
    }

    public String value(){
        return name().toLowerCase();
    }
}
