package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.header.GcloudConstants;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;

public class DetailInstance implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "虚拟机ID")
	private String id;
	
	@ApiModel(description = "实例名称")
	private String name;
	
	@ApiModel(description = "CPU核数")
	private Integer cpu;
	
	@ApiModel(description = "中文状态")
	private String cnStatus;
	
	@ApiModel(description = "创建时间")
	@JsonFormat(timezone = GcloudConstants.DEFAULT_TIMEZONE, pattern = GcloudConstants.DEFAULT_DATEFORMAT)
	private Date createTime;
	
	@ApiModel(description = "镜像ID")
	private String imageId;
	
	@ApiModel(description ="镜像名称")
	private String imageName;
	
	@ApiModel(description = "实例类型ID")
	private String instanceTypeId;
	
	@ApiModel(description = "实例类型名称")
	private String instanceTypeName;
	
	@ApiModel(description = "实例UUID")
	private String instanceUuid;
	
	@ApiModel(description = "")
	private String internetChargeType;
	
	@ApiModel(description = "")
	private String internetChargeTypeName;
	
	@ApiModel(description = "")
	private String internetMaxBandwidthOut;
	
	@ApiModel(description = "")
	private Boolean isTask;
	
	@ApiModel(description = "内存大小")
	private Integer memory;
	
	@ApiModel(description = "区域ID")
	private String regionId = ControllerProperty.REGION_ID;
	
	@ApiModel(description = "状态")
	private String status;
	
	@ApiModel(description = "磁盘类型ID")
	private String systemCategoryId;
	
	@ApiModel(description = "磁盘类型名称")
	private String systemCategoryName;
	
	@ApiModel(description = "磁盘大小")
	private Integer systemDisk;
	
	@ApiModel(description = "可用区ID")
	private String zoneId;
	
	@ApiModel(description = "可用区名称")
	private String zoneName;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getCpu() {
		return cpu;
	}
	public void setCpu(Integer cpu) {
		this.cpu = cpu;
	}
	public String getCnStatus() {
		return cnStatus;
	}
	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getInstanceTypeId() {
		return instanceTypeId;
	}
	public void setInstanceTypeId(String instanceTypeId) {
		this.instanceTypeId = instanceTypeId;
	}
	public String getInstanceTypeName() {
		return instanceTypeName;
	}
	public void setInstanceTypeName(String instanceTypeName) {
		this.instanceTypeName = instanceTypeName;
	}
	public String getInstanceUuid() {
		return instanceUuid;
	}
	public void setInstanceUuid(String instanceUuid) {
		this.instanceUuid = instanceUuid;
	}
	public String getInternetChargeType() {
		return internetChargeType;
	}
	public void setInternetChargeType(String internetChargeType) {
		this.internetChargeType = internetChargeType;
	}
	public String getInternetChargeTypeName() {
		return internetChargeTypeName;
	}
	public void setInternetChargeTypeName(String internetChargeTypeName) {
		this.internetChargeTypeName = internetChargeTypeName;
	}
	public String getInternetMaxBandwidthOut() {
		return internetMaxBandwidthOut;
	}
	public void setInternetMaxBandwidthOut(String internetMaxBandwidthOut) {
		this.internetMaxBandwidthOut = internetMaxBandwidthOut;
	}
	public Boolean getIsTask() {
		return isTask;
	}
	public void setIsTask(Boolean isTask) {
		this.isTask = isTask;
	}
	public Integer getMemory() {
		return memory;
	}
	public void setMemory(Integer memory) {
		this.memory = memory;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegionId() {
		return regionId;
	}
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSystemCategoryId() {
		return systemCategoryId;
	}
	public void setSystemCategoryId(String systemCategoryId) {
		this.systemCategoryId = systemCategoryId;
	}
	public String getSystemCategoryName() {
		return systemCategoryName;
	}
	public void setSystemCategoryName(String systemCategoryName) {
		this.systemCategoryName = systemCategoryName;
	}
	public Integer getSystemDisk() {
		return systemDisk;
	}
	public void setSystemDisk(Integer systemDisk) {
		this.systemDisk = systemDisk;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
}
