package com.gcloud.header.compute.msg.api.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeZonesResponse implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<StandardZoneType> zone;

	public List<StandardZoneType> getZone() {
		return zone;
	}

	public void setZone(List<StandardZoneType> zone) {
		this.zone = zone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
