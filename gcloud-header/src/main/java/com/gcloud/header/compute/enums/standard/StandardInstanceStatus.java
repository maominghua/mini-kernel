package com.gcloud.header.compute.enums.standard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.gcloud.header.compute.enums.VmState;

public enum StandardInstanceStatus {
	//TODO 查询状态有点问题，标准接口只有running和stopped对得上，只能查这2个
	RUNNING("Running", "运行中", Arrays.asList(VmState.RUNNING)),
	PENDING("Pending", "创建中", Arrays.asList(VmState.PENDING)),
//	STARTING("Starting", "正在开机", Arrays.asList(VmState.RUNNING)),
//	STOPPING("Stopping", "正在关机", Arrays.asList(VmState.RUNNING)),
	ERROR("Error", "不可用", Arrays.asList(VmState.DISABLED, VmState.CRASHED)),
	STOPPED("Stopped", "关机", Arrays.asList(VmState.STOPPED));
	
	private String value;
    private String cnName;
    private List<VmState> gcStatus;
    
    private StandardInstanceStatus(String value, String cnName, List<VmState> gcStatus) {
    	this.value = value;
    	this.cnName = cnName;
    	this.gcStatus = gcStatus;
    }
    
    public List<String> getGcStatusValues(){
        if(gcStatus == null){
            return null;
        }

        List<String> resultStatus = new ArrayList<>();
        gcStatus.forEach(s -> resultStatus.add(s.value()));
        return resultStatus;
    }
    
    public static String standardStatus(String gcStatusStr){
        if(StringUtils.isBlank(gcStatusStr)){
            return gcStatusStr;
        }

        for(StandardInstanceStatus status : StandardInstanceStatus.values()){
            if(status.gcStatus == null || status.getGcStatus().size() == 0){
                continue;
            }
            for(VmState gcStatus : status.getGcStatus()){
                if(gcStatus.value().equals(gcStatusStr)){
                    return status.getValue();
                }
            }
        }
        return null;
    }
    
    public static StandardInstanceStatus value(String value){
        return Arrays.stream(StandardInstanceStatus.values()).filter(s -> s.getValue().equals(value)).findFirst().orElse(null);
    }
    

	public String getValue() {
		return value;
	}

	public String getCnName() {
		return cnName;
	}

	public List<VmState> getGcStatus() {
		return gcStatus;
	}
}
