package com.gcloud.header.compute.msg.api.vm.base;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailInstanceMsg extends ApiMessage{

	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDetailInstanceReplyMsg.class;
	}
	
	@ApiModel(description = "实例ID", require = true)
	@NotBlank(message = "::实例ID不能为空")
	private String instanceId;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
