package com.gcloud.header.compute.msg.api.vm.base.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiRebootInstanceMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description="云服务器ID", require=true)
    @NotBlank(message = "0010401::云服务器ID不能为空")
    private String instanceId;
	@ApiModel(description="是否强强制执行",type="boolean")
    private Boolean forceStop;

	@Override
	public Class replyClazz() {
		return StandardApiRebootInstanceReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Boolean getForceStop() {
		return forceStop;
	}

	public void setForceStop(Boolean forceStop) {
		this.forceStop = forceStop;
	}
}
