package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class TplNetworkInterfaceItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "网卡ID")
	private String networkInterfaceId;
	@ApiModel(description = "内网IP")
	private String privateIp;
	@ApiModel(description = "交换机ID")
	private String vSwitchId;
	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}
	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
	public String getPrivateIp() {
		return privateIp;
	}
	public void setPrivateIp(String privateIp) {
		this.privateIp = privateIp;
	}
	public String getvSwitchId() {
		return vSwitchId;
	}
	public void setvSwitchId(String vSwitchId) {
		this.vSwitchId = vSwitchId;
	}
	
}
