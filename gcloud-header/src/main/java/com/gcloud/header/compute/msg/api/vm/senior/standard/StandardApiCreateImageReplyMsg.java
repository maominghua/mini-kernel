package com.gcloud.header.compute.msg.api.vm.senior.standard;

import com.gcloud.header.ApiReplyMessage;

public class StandardApiCreateImageReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private String imageId;
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
}
