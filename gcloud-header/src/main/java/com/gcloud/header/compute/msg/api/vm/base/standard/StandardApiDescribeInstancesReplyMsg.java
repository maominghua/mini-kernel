package com.gcloud.header.compute.msg.api.vm.base.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.compute.msg.api.model.standard.StandardDescribeInstancesResponse;
import com.gcloud.header.compute.msg.api.model.standard.StandardInstanceAttributesType;

public class StandardApiDescribeInstancesReplyMsg extends PageReplyMessage<StandardInstanceAttributesType>{

	private static final long serialVersionUID = 1L;

	private StandardDescribeInstancesResponse instances;
	
	@Override
	public void setList(List<StandardInstanceAttributesType> list) {
		instances = new StandardDescribeInstancesResponse();
		instances.setInstance(list);
	}

	public StandardDescribeInstancesResponse getInstances() {
		return instances;
	}

	public void setInstances(StandardDescribeInstancesResponse instances) {
		this.instances = instances;
	}
}
