package com.gcloud.header.compute.msg.api.model.standard;

import java.io.Serializable;
import java.util.List;

public class StandardDescribeInstancesResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<StandardInstanceAttributesType> instance;

	public List<StandardInstanceAttributesType> getInstance() {
		return instance;
	}

	public void setInstance(List<StandardInstanceAttributesType> instance) {
		this.instance = instance;
	}
}
