package com.gcloud.header.compute.msg.api.vm.senior.standard;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiCreateImageMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "0011201::云服务器ID不能为空")
	@ApiModel(description = "云服务器ID")
	private String instanceId;
	
	@Length(min=4, max=255, message="0011204::镜像名称长度为4-255")
	@ApiModel(description = "镜像名称")
	private String imageName;
	
	@Override
	public Class replyClazz() {
		return StandardApiCreateImageReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
}
