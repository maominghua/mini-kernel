package com.gcloud.header.compute.msg.api.vm.zone.standard;

import com.gcloud.header.ApiPageMessage;

public class StandardApiDescribeZonesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeZonesReplyMsg.class;
	}

}
