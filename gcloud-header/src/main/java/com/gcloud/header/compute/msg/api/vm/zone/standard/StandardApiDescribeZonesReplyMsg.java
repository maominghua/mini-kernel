package com.gcloud.header.compute.msg.api.vm.zone.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.compute.msg.api.model.standard.StandardDescribeZonesResponse;
import com.gcloud.header.compute.msg.api.model.standard.StandardZoneType;

public class StandardApiDescribeZonesReplyMsg extends PageReplyMessage<StandardZoneType>{
	private static final long serialVersionUID = 1L;
	
	private StandardDescribeZonesResponse zones;

	@Override
	public void setList(List<StandardZoneType> list) {
		zones = new StandardDescribeZonesResponse();
		zones.setZone(list);
	}

	public StandardDescribeZonesResponse getZones() {
		return zones;
	}

	public void setZones(StandardDescribeZonesResponse zones) {
		this.zones = zones;
	}
}
