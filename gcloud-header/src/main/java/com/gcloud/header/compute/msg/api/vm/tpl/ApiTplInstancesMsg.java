package com.gcloud.header.compute.msg.api.vm.tpl;

import com.gcloud.header.ApiMessage;

public class ApiTplInstancesMsg extends ApiMessage{

	@Override
	public Class replyClazz() {
		return ApiTplInstancesReplyMsg.class;
	}

}
