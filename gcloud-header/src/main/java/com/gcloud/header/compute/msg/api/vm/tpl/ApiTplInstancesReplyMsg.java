package com.gcloud.header.compute.msg.api.vm.tpl;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.TplInstanceResponse;

public class ApiTplInstancesReplyMsg extends ApiReplyMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "云服务器列表")
	TplInstanceResponse instances;
	public TplInstanceResponse getInstances() {
		return instances;
	}
	public void setInstances(TplInstanceResponse instances) {
		this.instances = instances;
	}
	
}
