package com.gcloud.header.compute.enums;

public enum QosDirection {

    EGRESS,
    INGRESS;

    public String value(){
        return name().toLowerCase();
    }

}
