package com.gcloud.header.compute.msg.api.vm.base.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.compute.msg.api.model.standard.StandardDescribeInstanceTypesResponse;
import com.gcloud.header.compute.msg.api.model.standard.StandardInstanceTypeItemType;

public class StandardApiDescribeInstanceTypesReplyMsg extends PageReplyMessage<StandardInstanceTypeItemType>{

	private static final long serialVersionUID = 1L;
	
	private StandardDescribeInstanceTypesResponse instanceTypes;

	@Override
	public void setList(List<StandardInstanceTypeItemType> list) {
		instanceTypes = new StandardDescribeInstanceTypesResponse();
		instanceTypes.setInstanceType(list);
	}

	public StandardDescribeInstanceTypesResponse getInstanceTypes() {
		return instanceTypes;
	}

	public void setInstanceTypes(StandardDescribeInstanceTypesResponse instanceTypes) {
		this.instanceTypes = instanceTypes;
	}
}
