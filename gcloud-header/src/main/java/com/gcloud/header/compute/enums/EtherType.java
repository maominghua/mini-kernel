package com.gcloud.header.compute.enums;

import java.util.Arrays;

public enum EtherType {

    IPv4("IPv4"), IPv6("IPv6");
    private String value;

    EtherType(String name){
        this.value = name;
    }

    public static EtherType getByValue(String value){
        return value == null || "".equals(value) ? null : Arrays.stream(EtherType.values()).filter(t -> t.getValue().equals(value)).findFirst().orElse(null);
    }

    public String getValue() {
        return value;
    }

}
