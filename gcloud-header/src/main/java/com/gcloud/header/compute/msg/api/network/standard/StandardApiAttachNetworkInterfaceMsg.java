package com.gcloud.header.compute.msg.api.network.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiAttachNetworkInterfaceMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "云服务器ID", require = true)
    @NotBlank(message = "0010601::云服务器ID不能为空")
    private String instanceId;
    @ApiModel(description = "网卡ID", require = true)
    @NotBlank(message = "0010602::网卡ID不能为空")
    private String networkInterfaceId;
    
	@Override
	public Class replyClazz() {
		return StandardApiAttachNetworkInterfaceReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getNetworkInterfaceId() {
		return networkInterfaceId;
	}

	public void setNetworkInterfaceId(String networkInterfaceId) {
		this.networkInterfaceId = networkInterfaceId;
	}
}
