package com.gcloud.header.compute.msg.api.vm.base.standard;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class StandardApiDescribeInstanceVncUrlMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description="实例ID")
    private String instanceId;
	
	@Override
	public Class replyClazz() {
		return StandardApiDescribeInstanceVncUrlReplyMsg.class;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
