package com.gcloud.header.compute.msg.api.vm.base.standard;

import com.gcloud.header.ApiReplyMessage;

public class StandardApiDescribeInstanceVncUrlReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private String vncUrl;

	public String getVncUrl() {
		return vncUrl;
	}

	public void setVncUrl(String vncUrl) {
		this.vncUrl = vncUrl;
	}
}
