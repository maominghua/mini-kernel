package com.gcloud.header.compute.msg.api.vm.create.standard;

import com.gcloud.header.ApiReplyMessage;

public class StandardApiCreateInstanceReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;

	private String instanceId;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
}
