package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;
import java.util.List;

public class TplNetworkInterfaceItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<TplNetworkInterfaceItem> networkInterfaceItem;

	public List<TplNetworkInterfaceItem> getNetworkInterfaceItem() {
		return networkInterfaceItem;
	}

	public void setNetworkInterfaceItem(List<TplNetworkInterfaceItem> networkInterfaceItem) {
		this.networkInterfaceItem = networkInterfaceItem;
	}


}
