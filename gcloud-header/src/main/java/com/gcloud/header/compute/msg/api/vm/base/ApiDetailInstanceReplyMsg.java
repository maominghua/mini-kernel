package com.gcloud.header.compute.msg.api.vm.base;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.compute.msg.api.model.DetailInstance;

public class ApiDetailInstanceReplyMsg extends ApiReplyMessage{

	private static final long serialVersionUID = 1L;

	private DetailInstance detailInstance;

	public DetailInstance getDetailInstance() {
		return detailInstance;
	}

	public void setDetailInstance(DetailInstance detailInstance) {
		this.detailInstance = detailInstance;
	}
}
