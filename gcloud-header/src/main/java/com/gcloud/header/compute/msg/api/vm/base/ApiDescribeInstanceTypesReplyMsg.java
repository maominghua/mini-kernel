package com.gcloud.header.compute.msg.api.vm.base;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.compute.msg.api.model.DescribeInstanceTypesResponse;
import com.gcloud.header.compute.msg.api.model.InstanceTypeItemType;

public class ApiDescribeInstanceTypesReplyMsg extends PageReplyMessage<InstanceTypeItemType> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DescribeInstanceTypesResponse instanceTypes;
	
	@Override
	public void setList(List<InstanceTypeItemType> list) {
		instanceTypes = new DescribeInstanceTypesResponse();
		instanceTypes.setInstanceType(list);
	}

	public DescribeInstanceTypesResponse getInstanceTypes() {
		return instanceTypes;
	}

	public void setInstanceTypes(DescribeInstanceTypesResponse instanceTypes) {
		this.instanceTypes = instanceTypes;
	}

}