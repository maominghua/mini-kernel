package com.gcloud.header.identity.tenant;

import javax.validation.constraints.NotNull;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;

public class ApiDetailTenantMsg  extends ApiMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "租户Id", require = true)
	@NotNull(message = "2020601::租户ID不能为空")
	private String tenantId;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public Class replyClazz() {
		return ApiDetailTenantReplyMsg.class;
	}
}
