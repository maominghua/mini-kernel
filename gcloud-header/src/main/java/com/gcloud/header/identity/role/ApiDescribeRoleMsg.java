package com.gcloud.header.identity.role;

import com.gcloud.header.ApiPageMessage;

public class ApiDescribeRoleMsg extends ApiPageMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return ApiDescribeRoleReplyMsg.class;
	}

}
