package com.gcloud.header.identity.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class TenantUserItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="用户ID")
	private String userId;
	@ApiModel(description="登陆名")
	private String loginName;
	@ApiModel(description="真实姓名")
	private String realName;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
}
