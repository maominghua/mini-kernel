package com.gcloud.header.identity.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.common.RegExp;

public class ApiUpdateSelInfoMsg extends ApiMessage{
	@ApiModel(description = "性别")
	//@NotNull(message = "2010601")
	private Boolean gender;//false男true女
	
	@ApiModel(description = "邮箱", require = true)
	@NotBlank(message = "2010602")
	@Email(message="2010603::请输入正确的邮箱地址")
	private String email;
	
	@ApiModel(description = "手机号码", require = true)
	@NotBlank(message = "2010604")
	@Pattern(regexp=RegExp.REGEX_MOBILE_PHONE, message="2010207::请输入正确的手机号码")
	private String mobile;
	
	@ApiModel(description = "真实姓名", require = true)
	@NotBlank(message = "2010605")
	@Length(min=2, max=20, message="2010608")
	private String realName;
	
	/*@ApiModel(description = "原密码")
	private String oldPassword;//登录用密码，需外部传输，加密不可逆
	
	@ApiModel(description = "新密码")
	@Pattern(regexp=RegExp.REGEX_PASSWORD_STRONG, message="2010606::请输入8-20位的密码，必含字母数字及特殊字符，且以字母开头")
	private String password;//登录用密码，需外部传输，加密不可逆
*/
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	/*public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}*/

}
