package com.gcloud.header.identity.model;

import java.io.Serializable;
import java.util.Date;

import com.gcloud.header.api.ApiModel;

public class DetailTenantResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "租户ID")
	private String id;
	@ApiModel(description = "租户名称")
	private String name;
	@ApiModel(description = "描述")
	private String description;
	@ApiModel(description = "创建者")
	private String creator;
	@ApiModel(description = "创建时间")
	private Date createTime;
	@ApiModel(description = "关联用户集合")
	private TenantUserItems tenantUserItems;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public TenantUserItems getTenantUserItems() {
		return tenantUserItems;
	}
	public void setTenantUserItems(TenantUserItems tenantUserItems) {
		this.tenantUserItems = tenantUserItems;
	}
	
}
