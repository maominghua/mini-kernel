package com.gcloud.header.identity.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class TenantUserItems implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@ApiModel(description="租户用户信息")
	private List<TenantUserItem> tenantUserItem;
	public List<TenantUserItem> getTenantUserItem() {
		return tenantUserItem;
	}
	public void setTenantUserItem(List<TenantUserItem> tenantUserItem) {
		this.tenantUserItem = tenantUserItem;
	}
}
