package com.gcloud.header.identity.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.user.UserModel;

public class DescribeUserResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="用户")
	private List<UserModel> user;
	public List<UserModel> getUser() {
		return user;
	}
	public void setUser(List<UserModel> user) {
		this.user = user;
	}
}
