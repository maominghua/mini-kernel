package com.gcloud.header.identity.tenant;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DetailTenantResponse;

public class ApiDetailTenantReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;
	@ApiModel(description="租户详情")
	private DetailTenantResponse tenant;
	public DetailTenantResponse getTenant() {
		return tenant;
	}
	public void setTenant(DetailTenantResponse tenant) {
		this.tenant = tenant;
	}
	
}
