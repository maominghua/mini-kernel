package com.gcloud.header.identity.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;

public class UserTenantItems implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="用户租户信息")
	private List<UserTenantItem> userTenantItem;
	public List<UserTenantItem> getUserTenantItem() {
		return userTenantItem;
	}
	public void setUserTenantItem(List<UserTenantItem> userTenantItem) {
		this.userTenantItem = userTenantItem;
	}
}
