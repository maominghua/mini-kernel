package com.gcloud.header.identity.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.tenant.TenantModel;

public class DescribeTenantResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="租户")
	private List<TenantModel> tenant;
	public List<TenantModel> getTenant() {
		return tenant;
	}
	public void setTenant(List<TenantModel> tenant) {
		this.tenant = tenant;
	}

}
