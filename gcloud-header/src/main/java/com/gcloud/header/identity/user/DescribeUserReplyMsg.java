package com.gcloud.header.identity.user;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DescribeUserResponse;

public class DescribeUserReplyMsg extends PageReplyMessage<UserModel>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="用户列表")
	private DescribeUserResponse users;
	
	@Override
	public void setList(List<UserModel> list) {
		users = new DescribeUserResponse();
		users.setUser(list);
	}

	public DescribeUserResponse getUsers() {
		return users;
	}

	public void setUsers(DescribeUserResponse users) {
		this.users = users;
	}
	
}
