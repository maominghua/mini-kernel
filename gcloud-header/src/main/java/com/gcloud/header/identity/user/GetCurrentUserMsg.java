package com.gcloud.header.identity.user;

import com.gcloud.header.ApiMessage;

public class GetCurrentUserMsg extends ApiMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return GetCurrentUserReplyMsg.class;
	}

}
