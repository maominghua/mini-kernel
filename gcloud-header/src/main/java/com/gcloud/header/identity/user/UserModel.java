package com.gcloud.header.identity.user;

import java.io.Serializable;
import java.util.Date;

import com.gcloud.header.api.ApiModel;

public class UserModel implements Serializable{
	@ApiModel(description="用户ID")
	private String id;
	@ApiModel(description="登录名")
	private String loginName;
	@ApiModel(description="性别")
	private Boolean gender;//false男true女
	@ApiModel(description="邮箱")
	private String email;
	@ApiModel(description="手机号")
	private String mobile;
	@ApiModel(description="角色ID")
	private String roleId;//超级管理员admin、租户管理员、普通用户
	@ApiModel(description="是否禁用")
	private Boolean disable;//true禁用false可用
	@ApiModel(description="真实姓名")
	private String realName;
	@ApiModel(description="创建时间")
	private String createTime;
	@ApiModel(description="创建者")
	private String creater;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public Boolean getDisable() {
		return disable;
	}
	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
}
