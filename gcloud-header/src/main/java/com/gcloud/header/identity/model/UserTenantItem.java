package com.gcloud.header.identity.model;

import java.io.Serializable;

import com.gcloud.header.api.ApiModel;

public class UserTenantItem implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="租户ID")
	private String tenantId;
	@ApiModel(description="租户名称")
	private String tenantName;
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
}
