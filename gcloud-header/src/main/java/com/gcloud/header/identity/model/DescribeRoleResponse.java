package com.gcloud.header.identity.model;

import java.io.Serializable;
import java.util.List;

import com.gcloud.header.api.ApiModel;


public class DescribeRoleResponse implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="角色")
	private List<RoleModel> role;

	public List<RoleModel> getRole() {
		return role;
	}

	public void setRole(List<RoleModel> role) {
		this.role = role;
	}
}
