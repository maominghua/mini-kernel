package com.gcloud.header.identity.role;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DescribeRoleResponse;
import com.gcloud.header.identity.model.RoleModel;

public class ApiDescribeRoleReplyMsg extends PageReplyMessage<RoleModel>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="角色列表")
	private DescribeRoleResponse roles;
	
	@Override
	public void setList(List<RoleModel> list) {
		roles = new DescribeRoleResponse();
		roles.setRole(list);
	}

	public DescribeRoleResponse getRoles() {
		return roles;
	}

	public void setRoles(DescribeRoleResponse roles) {
		this.roles = roles;
	}

}
