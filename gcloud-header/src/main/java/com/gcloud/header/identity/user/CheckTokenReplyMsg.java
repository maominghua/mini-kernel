package com.gcloud.header.identity.user;

import com.gcloud.header.ReplyMessage;

public class CheckTokenReplyMsg extends ReplyMessage{
	private String userId;
	private String loginName;
	private Long expressTime;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getExpressTime() {
		return expressTime;
	}
	public void setExpressTime(Long expressTime) {
		this.expressTime = expressTime;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
}
