package com.gcloud.header.identity.user;

import com.gcloud.header.ApiMessage;

public class ApiGetTokenMsg extends ApiMessage{

	@Override
	public Class replyClazz() {
		return ApiGetTokenReplyMsg.class;
	}
	
	
}
