package com.gcloud.header.identity.user;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;

public class ApiGetTokenReplyMsg extends ApiReplyMessage{
	@ApiModel(description="用户ID")
	private String userId;
	@ApiModel(description="登陆名称")
	private String loginName;
	@ApiModel(description="过期时间")
	private Long expireTime;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Long getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(Long expireTime) {
		this.expireTime = expireTime;
	}
	
}
