package com.gcloud.header.identity.user;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.common.RegExp;

public class ApiUpdateSelfPasswordMsg extends ApiMessage{
	@ApiModel(description = "原密码", require=true)
	@NotBlank(message="2010701")
	//@Pattern(regexp=RegExp.REGEX_PASSWORD_STRONG, message="2010202::请输入8-20位的密码，必含字母数字及特殊字符，且以字母开头")
	private String oldPassword;//登录用密码，需外部传输，加密不可逆
	
	@ApiModel(description = "新密码", require=true)
	@NotBlank(message="2010702")
	@Pattern(regexp=RegExp.REGEX_PASSWORD_STRONG, message="2010703::请输入8-20位的密码，必含字母数字及特殊字符，且以字母开头")
	private String password;//登录用密码，需外部传输，加密不可逆
	
	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
