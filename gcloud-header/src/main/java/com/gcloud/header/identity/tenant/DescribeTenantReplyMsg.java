package com.gcloud.header.identity.tenant;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.DescribeTenantResponse;

public class DescribeTenantReplyMsg extends PageReplyMessage<TenantModel>{
	@ApiModel(description="租户列表")
	private DescribeTenantResponse tanants;
	
	@Override
	public void setList(List<TenantModel> list) {
		tanants = new DescribeTenantResponse();
		tanants.setTenant(list);
	}

	public DescribeTenantResponse getTanants() {
		return tanants;
	}

	public void setTanants(DescribeTenantResponse tanants) {
		this.tanants = tanants;
	}
}
