package com.gcloud.header.identity.user;


import java.util.List;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.model.UserTenantItems;

public class GetUserReplyMsg extends ApiReplyMessage {
	@ApiModel(description="用户ID")
	private String id;
	@ApiModel(description="登录名")
	private String loginName;
	@ApiModel(description="性别")
	private Boolean gender;//false男true女
	@ApiModel(description="邮箱")
	private String email;
	@ApiModel(description="手机号码")
	private String mobile;
	@ApiModel(description="角色ID")
	private String roleId;//超级管理员admin、租户管理员、普通用户
	@ApiModel(description="是否禁用")
	private Boolean disable;//true禁用false可用
	@ApiModel(description="真实姓名")
	private String realName;
	@ApiModel(description="域")
	private String domain;
	@ApiModel(description="租户列表")
	private UserTenantItems tenants;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public Boolean getDisable() {
		return disable;
	}
	public void setDisable(Boolean disable) {
		this.disable = disable;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public UserTenantItems getTenants() {
		return tenants;
	}
	public void setTenants(UserTenantItems tenants) {
		this.tenants = tenants;
	}
	
}
