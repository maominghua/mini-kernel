package com.gcloud.common.model;

public class PortRangeInfo {

    private Integer min;
    private Integer max;

    public PortRangeInfo(Integer min, Integer max) {
        this.min = min;
        this.max = max;
    }

    public PortRangeInfo(Integer port) {
        this.min = port;
        this.max = port;
    }

    public PortRangeInfo() {
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }
}
